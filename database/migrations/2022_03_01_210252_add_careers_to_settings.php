<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCareersToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->string('career_title1');
            $table->string('career_title2');
            $table->string('career_title3');
            $table->text('career_description1');
            $table->text('career_description2');
            $table->text('career_description3');
            $table->string('career_image')->nullable();
        });
        DB::table('settings')->insert(
            array(
                'career_title1' => 'CUSTOM WORKING TIME',
                'career_title2' => 'FAMILY INSURANCE',
                'career_title3' => 'AWARD WINNING TEAM',
                'career_description1' => 'All Projects have an amazing and bright design, with a beautiful facade and a cozy location.',
                'career_description2' => 'All Projects have an amazing and bright design, with a beautiful facade and a cozy location.',
                'career_description3' => 'All Projects have an amazing and bright design, with a beautiful facade and a cozy location.',
                'career_image' => 'logo.png'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            //
        });
    }
}
