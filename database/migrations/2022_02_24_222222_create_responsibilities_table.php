<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responsibilities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('icon')->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->string('icon1')->nullable();
            $table->string('title1')->nullable();
            $table->text('description1')->nullable();
            $table->string('image1')->nullable();
            $table->string('icon2')->nullable();
            $table->string('title2')->nullable();
            $table->text('description2')->nullable();
            $table->string('image2')->nullable();
            $table->timestamps();
        });
        DB::table('responsibilities')->insert(
            array(
                'icon' => '21312938712.jpg',
                'title' => 'PEOPLE',
                'image' => '21312938712.jpg',
                'description' => 'We work closely with all of our stakeholders as we believe in the power of collaboration in nurturing sustainable, supportive partnerships with our clients. We aim to provide the best possible service to our customers, and to ensure ongoing advancement within our industry sector and beyond. Our approach of transparency and openness about both our goals and how we will achieve them, helps us attract, develop, support and retain a talented team of employees, which will ultimately secure our long-term success.',
                'icon1' => '21312938712.jpg',
                'title1' => 'COMMUNITY',
                'image1' => '21312938712.jpg',
                'description1' => 'We aim to contribute positively to our surrounding and to leave a positive legacy in the communities in which we work and have often been present. We are fully aware of our responsibility in raising the industry standards in Egypt and in enhancing public image of contractors by providing beneficial service and building trust relationships with our customers and the wider population. We are also focused on creating social value nurturing local employment and use of local trades, helping grow local smaller businesses, setting up social enterprises, providing training opportunities, participating in and encouraging community activities. We also have short-term plans of engaging in local education programmes.',
                'icon2' => '21312938712.jpg',
                'title2' => 'PLANET',
                'image2' => '21312938712.jpg',
                'description2' => 'We are dedicated to developing increased care and attention to the environment by minimising the environmental impact of our work, by promoting environmentally positive solutions, and increasing awareness of environmental issues throughout the industry. We ensure that all our site operations follow and exceed best practice guidelines, encouraging better practice throughout the sector. As a responsible business, we believe that construction industry has a vital role to play in reducing the carbon in its operations and that we need to take actions to tackle climate change, so we plan to take a whole lifecycle approach to every project and to make environmental concerns central to all our processes.'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responsibilities');
    }
}
