<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sitename')->nullable();
            $table->string('phone')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('addresslink')->nullable();
            $table->string('logo')->nullable();
            $table->text('slogan')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('linkedin')->nullable();
            $table->timestamps();
        });

        DB::table('settings')->insert(
            array(
                'sitename' => 'BUILDIN | APASSION TO BUILD',
                'phone' => '+20 128 888 6153',
                'whatsapp' => '+20 128 888 6153',
                'email' => 'contact@buildineg.com',
                'address' => '11B Abaza Square, Heliopolis, Cairo',
                'addresslink' => 'https://www.google.com/maps?q=483F+P63+Buildin+contracting,+El-Bostan,+Heliopolis,+Cairo+Governorate&ftid=0x14581591d3e45bad:0x1fa47ac3e883a584&hl=en-EG&gl=eg&entry=gps&lucs=swa&shorturl=1',
                'logo' => 'logo.png',
                'slogan' => 'is an egyption based general and specialized construction contractor .',
                'facebook' => '#',
                'instagram' => '#',
                'linkedin' => '#'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
