<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public static $imagePath="upload/setting/";
    public static $defaultImage="default.jpg";
    protected $fillable = [
        'sitename','logo', 'address','addresslink','email','phone','whatsapp','facebook','linkedin','instagram','slogan','title','description','mission','vision','image','mapembed','career_title1','career_title2','career_title3','career_description1','career_description2','career_description3','career_image','since',
    ];
}
