<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'name','email','phone','message','service_id',
    ];
    function mainService() {
        return $this->belongsTo('App\Service', 'service_id', 'id');
    }
}
