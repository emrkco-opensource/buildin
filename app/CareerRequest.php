<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerRequest extends Model
{
    public static $imagePath="upload/careers_files/";
    public static $defaultImage="default.jpg";

    protected $fillable = [
        'career_id',
        'name',
        'email',
        'phone',
        'file',
    ];

    public function getImageAttribute($value)
    {
        return url($this::$imagePath).'/'.$value;

    }
}
