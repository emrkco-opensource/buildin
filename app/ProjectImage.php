<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectImage extends Model
{
    public static $imagePath="upload/project/";
    public static $defaultImage="default.jpg";

    protected $fillable = [
        'image', 'project_id','role',
    ];

    public function getImageAttribute($value)
    {
        return url($this::$imagePath).'/'.$value;

    }
}
