<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsibilityslider extends Model
{
    public static $imagePath="upload/responslider/";
    public static $defaultImage="default.jpg";
    protected $fillable = [
        'image',
    ];
}
