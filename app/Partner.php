<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    public static $imagePath="upload/partner/";
    public static $defaultImage="default.jpg";
    protected $fillable = [
        'logo',
    ];
}
