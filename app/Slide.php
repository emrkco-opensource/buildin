<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    public static $imagePath="upload/slider/";
    public static $defaultImage="default.jpg";
    protected $fillable = [
        'logo','description','link',
    ];
}
