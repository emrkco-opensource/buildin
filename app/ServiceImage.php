<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceImage extends Model
{
    public static $imagePath="upload/service/";
    public static $defaultImage="default.jpg";

    protected $fillable = [
        'image', 'service_id','role',
    ];

    public function getImageAttribute($value)
    {
        return url($this::$imagePath).'/'.$value;

    }
}
