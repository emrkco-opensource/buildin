<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ourvalue extends Model
{
    protected $fillable = [
        'title','description',
    ];
}
