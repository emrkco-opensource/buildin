<?php
function check_language(){
    if(Session::has('lang')) {
        $s=session()->get('lang');
        if($s=='ar') {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function rate($rating)
{
    $html = "";
    for($i=0;$i<5;$i++){
            if($rating >0){

                if($rating >0.5)
                    $html  = $html . '<i class="icon-star"></i>';
                else
                    $html  = $html . '<i class="icon-star-half"></i>';
            }else{
                    $html  = $html . '<i class="icon-star-empty"></i>';
            }
             $rating--;
    }
    return $html;
}