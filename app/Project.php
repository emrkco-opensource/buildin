<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $with = [
        'main_image',
        'image',

    ];
    protected $fillable = [
        'service_id',
        'title',
        'short_description',
        'full_description',
        'link',
        'mapmark',
        'duration',
        'stakeholders',
        'sector',
        'location',
        'completion',
        'sort',
        'video',
    ];

    function main_image() {
        return $this->hasMany('App\ProjectImage', 'project_id', 'id')->where('role', 0);
    }

    function slider_image() {
        return $this->hasMany('App\ProjectImage', 'project_id', 'id')->where('role', 2);
    }

    function image() {
        return $this->hasMany('App\ProjectImage', 'project_id', 'id');
    }

    function mainService() {
        return $this->belongsTo('App\Service', 'service_id', 'id');
    }
    function service_image() {
        return $this->hasMany('App\ServiceImage', 'service_id', 'service_id')->where('role', 0);
    }
}
