<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Service extends Model
{

    protected $with = [
        'main_image',
        'image',

    ];

    function main_image() {
        return $this->hasMany('App\ServiceImage', 'service_id', 'id')->where('role', 0);
    }
    function image() {
        return $this->hasMany('App\ServiceImage', 'service_id', 'id');
    }

    function serviceCategory() {
        return $this->belongsTo('App\Servicecategory', 'service_category_id', 'id');
    }
}
