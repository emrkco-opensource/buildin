<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{

    protected $fillable = [
        'title',
        'description',
    ];

    function all_career_requests() {
        return $this->hasMany('App\CareerRequest', 'career_id', 'id');
    }
}
