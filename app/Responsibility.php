<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsibility extends Model
{
    public static $imagePath="upload/responsibility/";
    public static $defaultImage="default.jpg";
    protected $fillable = [
        'icon',
        'title',
        'description',
        'image',
        'icon1',
        'title1',
        'description1',
        'image1',
        'icon2',
        'title2',
        'description2',
        'image2',
    ];
}
