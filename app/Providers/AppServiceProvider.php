<?php

namespace App\Providers;

use View;
use App\Service;
use App\Setting;
use App\Servicecategory;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view::share('allSettings', Setting::first());
        view::share('allServices', Service::all());
        view::share('serviceCategories', Servicecategory::all());
    }
}
