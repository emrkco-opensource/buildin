<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    public static $imagePath="upload/certificate/";
    public static $defaultImage="default.jpg";
    protected $fillable = [
        'image',
    ];
}
