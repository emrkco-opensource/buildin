<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicecategory extends Model
{
    public static $imagePath="upload/serviceCategory/";
    public static $defaultImage="default.jpg";
    protected $fillable = [
        'logo', 'title',
    ];

    function services() {
        return $this->hasMany('App\Service', 'service_category_id', 'id');
    }
}
