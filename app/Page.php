<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public static $imagePath="upload/page/";
    public static $defaultImage="default.jpg";
    protected $fillable = [
        'title','description','image',
    ];
}
