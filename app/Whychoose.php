<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Whychoose extends Model
{
    public static $imagePath="upload/feature/";
    public static $defaultImage="default.jpg";
    protected $fillable = [
        'logo', 'title','description',
    ];
}
