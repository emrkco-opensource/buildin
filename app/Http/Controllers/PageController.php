<?php

namespace App\Http\Controllers;

use App\Page;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['data'] = Page::all();
        return view('admin.page.index',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.page.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required',
            'description' => 'required',
        ]);
        $data = $request->all();

        $data['image'] = ImageHelper::save_webp($data['image'],Page::$imagePath);

        Page::create($data);
        return redirect('pages')->with('success', 'Page Added Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['single'] = Page::find($id);
        return view('admin.page.add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        $data = $request->all();

        if(isset($data['image']) && $data['image']){
            if($page->getOriginal('image') !=  Page::$defaultImage){
                ImageHelper::delete($page->getOriginal('image'),Page::$imagePath);
            }
            $data['image'] = ImageHelper::save_webp($data['image'],Page::$imagePath);
        }
// dd($data);
        $page->update($data);
        return redirect('pages')->with('success', 'Page Edited Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }
}
