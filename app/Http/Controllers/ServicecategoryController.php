<?php

namespace App\Http\Controllers;

use App\Service;
use App\Servicecategory;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;

class ServicecategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['data'] = Servicecategory::all();
        return view('admin.servicescategory.index',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.servicescategory.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'logo' => 'required',
            'title' => 'required',
        ]);
        $data = $request->all();

        $data['logo'] = ImageHelper::save_webp($data['logo'],Servicecategory::$imagePath);

        Servicecategory::create($data);
        return redirect('services_category')->with('success', 'Category Added Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Servicecategory  $servicecategory
     * @return \Illuminate\Http\Response
     */
    public function show(Servicecategory $servicecategory)
    {
        //
    }

    public function oneCategory($id)
    {
        $query['category'] = Servicecategory::with('services')->find($id);

        // dd($query['category']['services']);
        return view('service_category',$query);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Servicecategory  $servicecategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['single'] = Servicecategory::find($id);
        return view('admin.servicescategory.add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Servicecategory  $servicecategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Servicecategory $servicecategory,$id)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $data = $request->all();

        if(isset($data['logo']) && $data['logo']){
            if($servicecategory->getOriginal('logo') !=  Servicecategory::$defaultImage){
                ImageHelper::delete($servicecategory->getOriginal('logo'),Servicecategory::$imagePath);
            }
            $data['logo'] = ImageHelper::save_webp($data['logo'],Servicecategory::$imagePath);
        }

        $servicecategory = Servicecategory::find($id)->update($data);
        return redirect('services_category')->with('success', 'Category Edited Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Servicecategory  $servicecategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Servicecategory $servicecategory , $id)
    {
        $servicecategory = Servicecategory::find($id)->delete();
        return redirect('services_category')->with('success', 'Category Deleted Succesfully');
    }
}
