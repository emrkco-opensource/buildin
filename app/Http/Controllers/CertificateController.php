<?php

namespace App\Http\Controllers;

use App\Page;
use App\Service;
use App\Setting;
use App\Certificate;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['data'] = Certificate::all();

        return view('admin.certificates.index',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.certificates.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required',
        ]);

        $data = $request->all();

        $data['image'] = ImageHelper::save_webp($data['image'],Certificate::$imagePath);

        Certificate::create($data);
        return redirect('certificates')->with('success', 'Certificate Added Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function show(Certificate $certificate)
    {
        $query['settings'] = Setting::first();
        $query['services'] = Service::all();
        $query['data'] = Certificate::all();
        $query['page'] = Page::find(3);
        return view('certificates',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function edit(Certificate $certificate)
    {
        $query['single'] = $certificate;
        return view('admin.certificates.add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Certificate $certificate)
    {
                $data = $request->all();

        if(isset($data['image']) && $data['image']){
            if($certificate->getOriginal('image') !=  Certificate::$defaultImage){
                ImageHelper::delete($certificate->getOriginal('image'),Certificate::$imagePath);
            }
            $data['image'] = ImageHelper::save_webp($data['image'],Certificate::$imagePath);
        }

        $certificate->update($data);
        return redirect('certificates')->with('success', 'Certificate Updated Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Certificate  $certificate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Certificate $certificate)
    {
        $certificate->delete();
        ImageHelper::delete($certificate->getOriginal('image'), Certificate::$imagePath);
        return redirect('certificates')->with('success', 'Certificate Deleted Succesfully');
    }
}
