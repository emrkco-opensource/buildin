<?php

namespace App\Http\Controllers;

use App\Whychoose;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;

class WhychooseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['data'] = Whychoose::all();
        return view('admin.whychoose.index',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.whychoose.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'logo' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);

        $data['logo'] = ImageHelper::save_webp($data['logo'],Whychoose::$imagePath);

        Whychoose::create($data);
        return redirect('whychoose')->with('success', 'Why choose Us Added Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Whychoose  $whychoose
     * @return \Illuminate\Http\Response
     */
    public function show(Whychoose $whychoose)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Whychoose  $whychoose
     * @return \Illuminate\Http\Response
     */
    public function edit(Whychoose $whychoose)
    {
        $query['single'] = $whychoose;
        return view('admin.whychoose.add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Whychoose  $whychoose
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Whychoose $whychoose)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $data = $request->all();

        if(isset($data['logo']) && $data['logo']){
            if($whychoose->getOriginal('logo') !=  Whychoose::$defaultImage){
                ImageHelper::delete($whychoose->getOriginal('logo'),Whychoose::$imagePath);
            }
            $data['logo'] = ImageHelper::save_webp($data['logo'],Whychoose::$imagePath);
        }

        $whychoose->update($data);
        return redirect('whychoose')->with('success', 'Why Choose Us Edited Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Whychoose  $whychoose
     * @return \Illuminate\Http\Response
     */
    public function destroy(Whychoose $whychoose)
    {
        $whychoose->delete();
        ImageHelper::delete($whychoose->getOriginal('logo'), Whychoose::$imagePath);
        return redirect('whychoose')->with('success', 'Why Choose Us Deleted Succesfully');
    }
}
