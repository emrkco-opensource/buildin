<?php

namespace App\Http\Controllers;

use App\Page;
use App\Project;
use App\Service;
use App\Setting;
use App\ProjectImage;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['data'] = Project::with('mainService')->orderByRaw('CONVERT(sort, SIGNED) ASC')->get();
        return view('admin.projects.index',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $query['services'] = Service::all();
        return view('admin.projects.add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'service_id' => 'required',
            'title' => 'required',
            'short_description' => 'required',
            'full_description' => 'required',
            'mapmark' => 'required',
            'duration' => 'required',
            'stakeholders' => 'required',
            'sector' => 'required',
            'location' => 'required',
            'completion' => 'required',
        ]);

        $project = new Project;
        $project->service_id = $request->service_id;
        $project->title = $request->title;
        $project->mapmark = $request->mapmark;
        $project->short_description = $request->short_description;
        $project->full_description = $request->full_description;
        $project->link = $request->link;
        $project->duration = $request->duration;
        $project->stakeholders = $request->stakeholders;
        $project->sector = $request->sector;
        $project->location = $request->location;
        $project->completion = $request->completion;
        $project->sort = $request->sort;
        $project->video = $request->video;
        $project->save();

        if ($request->hasFile('images')) {
            $files = $request->file('images');
            foreach ($files as $key => $file) {
                $extension = $file->getClientOriginalExtension();
                $file_name = 'master' . rand(11111, 99999) . time() . '.' . $extension;
                $destinationPath = public_path() . '/upload/project';
                $file->move($destinationPath, $file_name);
                $img = new ProjectImage;
                $img->project_id = $project->id;
                if($key==0)
                    $img->role = 0;
                else
                    $img->role = 1;
                $img->image = $file_name;
                $img->save();
            }
        }
        if ($request->hasFile('sliderimages')) {
            $files = $request->file('sliderimages');
            foreach ($files as $key => $file) {
                $extension = $file->getClientOriginalExtension();
                $file_name = 'master' . rand(11111, 99999) . time() . '.' . $extension;
                $destinationPath = public_path() . '/upload/project';
                $file->move($destinationPath, $file_name);
                $img1 = new ProjectImage;
                $img1->project_id = $project->id;
                $img1->role = 2;
                $img1->image = $file_name;
                $img1->save();
            }
        }
        return redirect('projects')->with('success','Project Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $query['settings'] = Setting::first();
        $query['services'] = Service::all();
        $query['projects'] = Project::orderByRaw('CONVERT(sort, SIGNED) ASC')->get();
        $query['page'] = Page::find(2);

        return view('our_projects', $query);

    }

    public function oneProject($id)
    {
        $query['settings'] = Setting::first();
        $query['project'] = Project::where('projects.id',$id)->first();
        $query['projects'] = Project::where('projects.service_id',$query['project']->service_id)->get();
        $query['imgs'] = ProjectImage::where('project_id',$id)->where('role',1)->get();
        $query['sliderimgs'] = ProjectImage::where('project_id',$id)->where('role',2)->get();
        $query['services'] = Service::all();

        // dd($query['images']);
        return view('one_project',$query);

    }
//---------- Images Functions -------------//
public function project_images($id)
{
    $query['project'] = Project::where('projects.id',$id)->first();
    $query['images'] = ProjectImage::where('project_id',$id)->get();
    return view('admin.projects.images',$query);
}

public function project_images_delete($id)
{
    $image = ProjectImage::where('id',$id)->first();
    if($image){
        $image->delete();
    }
    return redirect()->back()->with('success','Image Deleted Successfully');

}

public function project_images_default($id)
{
    $image = ProjectImage::where('id',$id)->first();
    if($image){
        $images = ProjectImage::where('project_id',$image->project_id)->where('role',0)->update(['role' => 1]);
        $image->role = 0;
        $image->save();
    }
    return redirect()->back()->with('success','Image Edited Successfully');

}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $query['single'] = Project::with('mainService')->findOrFail($project->id);
        $query['services'] = Service::all();

        return view('admin.projects.add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'service_id' => 'required',
            'title' => 'required',
            'short_description' => 'required',
            'full_description' => 'required',
            'mapmark' => 'required',
            'duration' => 'required',
            'stakeholders' => 'required',
            'sector' => 'required',
            'location' => 'required',
            'completion' => 'required',
        ]);

        $project = Project::find($id);
        $project->service_id = $request->service_id;
        $project->title = $request->title;
        $project->mapmark = $request->mapmark;
        $project->short_description = $request->short_description;
        $project->full_description = $request->full_description;
        $project->link = $request->link;
        $project->duration = $request->duration;
        $project->stakeholders = $request->stakeholders;
        $project->sector = $request->sector;
        $project->location = $request->location;
        $project->completion = $request->completion;
        $project->sort = $request->sort;
        $project->video = $request->video;
        $project->save();

        if ($request->hasFile('images')) {
            $files = $request->file('images');
            foreach ($files as $key => $file) {
                $extension = $file->getClientOriginalExtension();
                $file_name = 'master' . rand(11111, 99999) . time() . '.' . $extension;
                $destinationPath = public_path() . '/upload/project';
                $file->move($destinationPath, $file_name);
                $img = new ProjectImage;
                $img->project_id = $project->id;
                $img->role = 1;
                $img->image = $file_name;
                $img->save();
            }
        }
        if ($request->hasFile('sliderimages')) {
            $files = $request->file('sliderimages');
            foreach ($files as $key => $file) {
                $extension = $file->getClientOriginalExtension();
                $file_name = 'master' . rand(11111, 99999) . time() . '.' . $extension;
                $destinationPath = public_path() . '/upload/project';
                $file->move($destinationPath, $file_name);
                $img1 = new ProjectImage;
                $img1->project_id = $project->id;
                $img1->role = 2;
                $img1->image = $file_name;
                $img1->save();
            }
        }
        return redirect('projects')->with('success','Project Edited Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id)->delete();
        return redirect('projects')->with('success', 'Project Deleted Succesfully');
    }
}
