<?php

namespace App\Http\Controllers;

use App\Page;
use App\Service;
use App\Setting;
use App\Ourvalue;
use App\Whychoose;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['setting'] = Setting::first();
        // dd($query);
        return view('admin.dashboard',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        $query['setting'] = Setting::first();
        // dd($query['setting']);
        return view('admin.dashboard',$query);
    }

    public function aboutUs(Setting $setting)
    {
        $query['settings'] = Setting::first();
        $query['page'] = Page::find(6);
        $query['services'] = Service::all();
        $query['why'] = Whychoose::all();
        $query['valuefirst'] = Ourvalue::first();
        $query['ourvalue'] = Ourvalue::where('id', '!=' ,$query['valuefirst']->id)->get();

        // dd($query['setting']);
        return view('about',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        $this->validate($request, [
            'sitename' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'addresslink' => 'required',
            'whatsapp' => 'required',
            'facebook' => 'required',
            'linkedin' => 'required',
            'instagram' => 'required',
            'slogan' => 'required',
            'title' => 'required',
            'description' => 'required',
            'mission' => 'required',
            'vision' => 'required',
            'mapembed' => 'required',
            'career_title1' => 'required',
            'career_title2' => 'required',
            'career_title3' => 'required',
            'career_description1' => 'required',
            'career_description2' => 'required',
            'career_description3' => 'required',
            'since' => 'required',
        ]);
        $data = $request->all();
        $setting = Setting::first();

        if(isset($data['logo']) && $data['logo']){
            if($setting->getOriginal('logo') !=  Setting::$defaultImage){
                ImageHelper::delete($setting->getOriginal('logo'),Setting::$imagePath);
            }
            $data['logo'] = ImageHelper::save_webp($data['logo'],Setting::$imagePath);

        }
        if(isset($data['image']) && $data['image']){
            if($setting->getOriginal('image') !=  Setting::$defaultImage){
                ImageHelper::delete($setting->getOriginal('image'),Setting::$imagePath);
            }
            $data['image'] = ImageHelper::save_webp($data['image'],Setting::$imagePath);
        }
        if(isset($data['career_image']) && $data['career_image']){
            if($setting->getOriginal('career_image') !=  Setting::$defaultImage){
                ImageHelper::delete($setting->getOriginal('career_image'),Setting::$imagePath);
            }
            $data['career_image'] = ImageHelper::save_webp($data['career_image'],Setting::$imagePath);
        }
        $setting->update($data);
        return redirect()->back()->with('success','Edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
