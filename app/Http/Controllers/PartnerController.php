<?php

namespace App\Http\Controllers;

use App\Page;
use App\Partner;
use App\Service;
use App\Setting;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['data'] = Partner::all();

        return view('admin.partners.index',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partners.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'logo' => 'required',
        ]);

        $data = $request->all();

        $data['logo'] = ImageHelper::save_webp($data['logo'],Partner::$imagePath);

        Partner::create($data);
        return redirect('partners')->with('success', 'Parnter Added Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {
        $query['settings'] = Setting::first();
        $query['services'] = Service::all();
        $query['data'] = Partner::all();
        $query['page'] = Page::find(4);
        return view('clients',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        $query['single'] = $partner;
        // dd($partner);
        return view('admin.partners.add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partner $partner)
    {
        $data = $request->all();

        if(isset($data['logo']) && $data['logo']){
            if($partner->getOriginal('logo') !=  Partner::$defaultImage){
                ImageHelper::delete($partner->getOriginal('logo'),Partner::$imagePath);
            }
            $data['logo'] = ImageHelper::save_webp($data['logo'],Partner::$imagePath);
        }

        $partner->update($data);
        return redirect('partners')->with('success', 'Parnter Updated Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partner $partner)
    {
        $partner->delete();
        ImageHelper::delete($partner->getOriginal('logo'), Partner::$imagePath);
        return redirect('partners')->with('success', 'Parnter Deleted Succesfully');
    }
}
