<?php

namespace App\Http\Controllers;

use App\Page;
use App\Contact;
use App\Service;
use App\Setting;
use App\Newsletter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['data'] = Contact::with('mainService')->get();

        return view('admin.contacts.index',$query);
    }

    public function contactPage()
    {
        $query['settings'] = Setting::first();
        $query['services'] = Service::all();
        // $query['contacts'] = Contact::all();
        $query['page'] = Page::find(5);
        return view('contacts',$query);
    }

    public function contactWithUs(Request $request)
    {
        $query['settings'] = Setting::first();
        $query['services'] = Service::all();
        $this->validate($request, [
            'service_id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required',
        ]);

        $contact = new Contact;
        $contact->service_id = $request->service_id;
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->message = $request->message;
        $contact->save();

        return redirect('contactUSpage')->with('success','Message Sent Successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        $query['single'] = $contact;
        return view('admin.contacts.show',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();
        return redirect('contact')->with('success', 'Message Deleted Succesfully');
    }

    public function subscribe(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
        ]);

        $contact = new Newsletter;
        $contact->email = $request->email;
        $contact->save();

        return redirect()->back()->with('success','Subscribed Successfully');
    }

    public function subscribe_admin()
    {
        $query['data'] = Newsletter::all();

        return view('admin.newsletter.index',$query);
    }

    public function subscribe_delete($id)
    {
        // $newsletter->delete();
        $newsletter = Newsletter::find($id)->delete();

        return redirect('newsletter_sub')->with('success', 'Subscribe Deleted Succesfully');
    }
}
