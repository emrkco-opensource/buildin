<?php

namespace App\Http\Controllers;

use App\Page;
use App\Service;
use App\Responsibility;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Responsibilityslider;
use App\Http\Controllers\Controller;

class ResponsibilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['respo'] = Responsibility::first();
        // dd($query);
        return view('admin.responsibilty.index',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Responsibility  $responsibility
     * @return \Illuminate\Http\Response
     */
    public function show(Responsibility $responsibility)
    {
        $query['respo'] = Responsibility::first();
        $query['services'] = Service::all();
        $query['reslider'] = Responsibilityslider::all();
        // $query['projects'] = Project::all();
        $query['page'] = Page::find(1);
        return view('responsibilities',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Responsibility  $responsibility
     * @return \Illuminate\Http\Response
     */
    public function edit(Responsibility $responsibility)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Responsibility  $responsibility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Responsibility $responsibility)
    {
        $this->validate($request, [
            'title' => 'required',
            'title1' => 'required',
            'title2' => 'required',
            'description' => 'required',
            'description1' => 'required',
            'description2' => 'required',
        ]);
        $data = $request->all();
        $responsibility = Responsibility::first();

        if(isset($data['icon']) && $data['icon']){
            if($responsibility->getOriginal('icon') !=  Responsibility::$defaultImage){
                ImageHelper::delete($responsibility->getOriginal('icon'),Responsibility::$imagePath);
            }
            $data['icon'] = ImageHelper::save_webp($data['icon'],Responsibility::$imagePath);

        }
        if(isset($data['image']) && $data['image']){
            if($responsibility->getOriginal('image') !=  Responsibility::$defaultImage){
                ImageHelper::delete($responsibility->getOriginal('image'),Responsibility::$imagePath);
            }
            $data['image'] = ImageHelper::save_webp($data['image'],Responsibility::$imagePath);
        }
        if(isset($data['icon1']) && $data['icon1']){
            if($responsibility->getOriginal('icon1') !=  Responsibility::$defaultImage){
                ImageHelper::delete($responsibility->getOriginal('icon1'),Responsibility::$imagePath);
            }
            $data['icon1'] = ImageHelper::save_webp($data['icon1'],Responsibility::$imagePath);

        }
        if(isset($data['image1']) && $data['image1']){
            if($responsibility->getOriginal('image1') !=  Responsibility::$defaultImage){
                ImageHelper::delete($responsibility->getOriginal('image1'),Responsibility::$imagePath);
            }
            $data['image1'] = ImageHelper::save_webp($data['image1'],Responsibility::$imagePath);
        }
        if(isset($data['icon2']) && $data['icon2']){
            if($responsibility->getOriginal('icon2') !=  Responsibility::$defaultImage){
                ImageHelper::delete($responsibility->getOriginal('icon2'),Responsibility::$imagePath);
            }
            $data['icon2'] = ImageHelper::save_webp($data['icon2'],Responsibility::$imagePath);

        }
        if(isset($data['image2']) && $data['image2']){
            if($responsibility->getOriginal('image2') !=  Responsibility::$defaultImage){
                ImageHelper::delete($responsibility->getOriginal('image2'),Responsibility::$imagePath);
            }
            $data['image2'] = ImageHelper::save_webp($data['image2'],Responsibility::$imagePath);
        }

        $responsibility->update($data);
        return redirect()->back()->with('success','Edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Responsibility  $responsibility
     * @return \Illuminate\Http\Response
     */
    public function destroy(Responsibility $responsibility)
    {
        //
    }
}
