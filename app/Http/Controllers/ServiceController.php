<?php

namespace App\Http\Controllers;

use App\Service;
use App\ServiceImage;
use App\Servicecategory;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['data'] = Service::with('serviceCategory')->get();
        return view('admin.services.index',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $query['serviceCategory'] = Servicecategory::all();
        return view('admin.services.add',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'service_category_id' => 'required',
            'title' => 'required',
            'short_description' => 'required',
            'first_part_description' => 'required',
        ]);

        $service = new Service;
        $service->service_category_id = $request->service_category_id;
        $service->title = $request->title;
        $service->short_description = $request->short_description;
        $service->first_part_description = $request->first_part_description;
        $service->second_part_description = $request->second_part_description;
        $service->video = $request->video;
        $service->save();

        if ($request->hasFile('images')) {
            $files = $request->file('images');
            foreach ($files as $key => $file) {
                $extension = $file->getClientOriginalExtension();
                $file_name = 'master' . rand(11111, 99999) . time() . '.' . $extension;
                $destinationPath = public_path() . '/upload/service';
                $file->move($destinationPath, $file_name);
                $img = new ServiceImage;
                $img->service_id = $service->id;
                if($key==0)
                    $img->role = 0;
                else
                    $img->role = 1;
                $img->image = $file_name;
                $img->save();
            }
        }
        return redirect('services')->with('success','Service Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    public function oneService($id)
    {
        $query['service'] = Service::find($id);
        $query['links'] = Service::where('service_category_id',$query['service']->serviceCategory->id)->get();
// dd($query['links']);
        return view('one_service',$query);
    }

                //---------- Images Functions -------------//
    public function service_images($id)
    {
        $query['service'] = Service::where('services.id',$id)->first();
        $query['images'] = ServiceImage::where('service_id',$id)->get();
        return view('admin.services.images',$query);
    }

    public function service_images_delete($id)
    {
        $image = ServiceImage::where('id',$id)->first();
        if($image){
            $image->delete();
        }
        return redirect()->back()->with('success','Image Deleted Successfully');

    }

    public function service_images_default($id)
    {
        $image = ServiceImage::where('id',$id)->first();
        if($image){
            $images = ServiceImage::where('service_id',$image->service_id)
            ->where('role',0)->update(['role' => 1]);
            $image->role = 0;
            $image->save();
        }
        return redirect()->back()->with('success','Image Edited Successfully');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $query['single'] = Service::with('serviceCategory')->findOrFail($service->id);
        $query['serviceCategory'] = Servicecategory::all();

        return view('admin.services.add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $this->validate($request, [
            'service_category_id' => 'required',
            'title' => 'required',
            'short_description' => 'required',
            'first_part_description' => 'required',
            // 'second_part_description' => 'required',
        ]);

        $service = $service;
        $service->service_category_id = $request->service_category_id;
        $service->title = $request->title;
        $service->short_description = $request->short_description;
        $service->first_part_description = $request->first_part_description;
        $service->second_part_description = $request->second_part_description;
        $service->video = $request->video;
        $service->save();

        if ($request->hasFile('images')) {
            $files = $request->file('images');
            foreach ($files as $key => $file) {
                $extension = $file->getClientOriginalExtension();
                $file_name = 'master' . rand(11111, 99999) . time() . '.' . $extension;
                $destinationPath = public_path() . '/upload/service';
                $file->move($destinationPath, $file_name);
                $img = new ServiceImage;
                $img->service_id = $service->id;
                if($key==0)
                    $img->role = 0;
                else
                    $img->role = 1;
                $img->image = $file_name;
                $img->save();
            }
        }
        return redirect('services')->with('success','Service Edited Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return redirect('services')->with('success', 'Service Deleted Succesfully');
    }
}
