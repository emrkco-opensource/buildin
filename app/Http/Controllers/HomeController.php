<?php

namespace App\Http\Controllers;
use App\Slide;
use App\Service;
use App\Partner;
use App\Project;
use App\Setting;
use App\Whychoose;
use App\Servicecategory;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $query['settings'] = Setting::first();
        $query['partners'] = Partner::all();
        $query['why'] = Whychoose::all();
        $query['sliders'] = Slide::all();
        $query['servicescategory'] = Servicecategory::all();
        // $query['slider1'] = Slide::first();
        $query['services'] = Service::all();
        $query['projects'] = Project::orderByRaw('CONVERT(sort, SIGNED) ASC')->get();
        return view('home', $query);
    }
}
