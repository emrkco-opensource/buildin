<?php

namespace App\Http\Controllers;

use App\Ourvalue;
use Illuminate\Http\Request;

class OurvalueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['data'] = Ourvalue::all();
        return view('admin.ourvalue.index',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ourvalue.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        Ourvalue::create($data);
        return redirect('ourvalue')->with('success', 'Value Added Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ourvalue  $ourvalue
     * @return \Illuminate\Http\Response
     */
    public function show(Ourvalue $ourvalue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ourvalue  $ourvalue
     * @return \Illuminate\Http\Response
     */
    public function edit(Ourvalue $ourvalue)
    {
        $query['single'] = $ourvalue;
        return view('admin.ourvalue.add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ourvalue  $ourvalue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ourvalue $ourvalue)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        $data = $request->all();

        $ourvalue->update($data);
        return redirect('ourvalue')->with('success', 'Value Edited Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ourvalue  $ourvalue
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ourvalue $ourvalue)
    {
        $ourvalue->delete();
        return redirect('ourvalue')->with('success', 'Value Deleted Succesfully');
    }
}
