<?php

namespace App\Http\Controllers;

use App\Page;
use App\Career;
use App\CareerRequest;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['data'] = Career::all();
        return view('admin.careers.index',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.careers.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        $data = $request->all();

        Career::create($data);

        return redirect('careers')->with('success','Career Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query['career'] = Career :: find($id);
        return view('one_career',$query);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function edit(Career $career)
    {
        $query['single'] = $career;

        return view('admin.careers.add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Career $career)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        $data = $request->all();
        $career->update($data);
        return redirect('careers')->with('success', 'Career Edited Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function destroy(Career $career)
    {
        $career->delete();
        return redirect('careers')->with('success', 'Career Deleted Succesfully');
    }

    public function one_career_requests($id)
    {
        $query['career'] = Career::where('careers.id',$id)->first();
        $query['requests'] = CareerRequest::where('career_id',$id)->get();
        return view('admin.careers.requests',$query);
    }

    public function career_request(Request $request)
    {

        $data = $request->all();
        $this->validate($request, [
            'career_id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'file' => 'required',
        ]);

        $data['file'] = ImageHelper::save($data['file'],CareerRequest::$imagePath);
        // $data['career_id'] = $career->id;

        CareerRequest::create($data);
        return redirect()->back()->with('success', 'Career Request Sent Succesfully');
    }

    public function allcareers()
    {
        $query['careers'] = Career::all();
        $query['page'] = Page::find(7);
        return view('our_careers',$query);
    }

    public function career_request_delete ($id)
    {
        $image = CareerRequest::where('id',$id)->first();
        if($image){
            $image->delete();
        }
        // $careerRequest->delete();
        ImageHelper::delete($image->getOriginal('file'), CareerRequest::$imagePath);
        return redirect()->back()->with('success', 'Request Deleted Succesfully');
    }

}
