<?php

namespace App\Http\Controllers;

use App\Responsibilityslider;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;

class ResponsibilitysliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['data'] = Responsibilityslider::all();
        return view('admin.responsibiltyslider.index',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.responsibiltyslider.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required',
        ]);
        $data = $request->all();

        $data['image'] = ImageHelper::save_webp($data['image'],Responsibilityslider::$imagePath);

        Responsibilityslider::create($data);
        return redirect('responslider')->with('success', 'Slider Added Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Responsibilityslider  $responsibilityslider
     * @return \Illuminate\Http\Response
     */
    public function show(Responsibilityslider $responsibilityslider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Responsibilityslider  $responsibilityslider
     * @return \Illuminate\Http\Response
     */
    public function edit(Responsibilityslider $responsibilityslider,$id)
    {
        $query['single'] = Responsibilityslider::find($id);
        return view('admin.responsibiltyslider.add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Responsibilityslider  $responsibilityslider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Responsibilityslider $responsibilityslider, $id)
    {
        $data = $request->all();

        if(isset($data['image']) && $data['image']){
            if($responsibilityslider->getOriginal('image') !=  Responsibilityslider::$defaultImage){
                ImageHelper::delete($responsibilityslider->getOriginal('image'),Responsibilityslider::$imagePath);
            }
            $data['image'] = ImageHelper::save_webp($data['image'],Responsibilityslider::$imagePath);
        }
// dd($data);
        $responsibilityslider = Responsibilityslider::find($id)->update($data);
        return redirect('responslider')->with('success', 'Slider Edited Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Responsibilityslider  $responsibilityslider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Responsibilityslider $responsibilityslider,$id)
    {
        $responsibilityslider = Responsibilityslider::find($id)->delete();
        return redirect('responslider')->with('success', 'Slider Deleted Succesfully');

    }
}
