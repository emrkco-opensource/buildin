<?php

namespace App\Http\Controllers;

use App\Slide;
use App\Helpers\ImageHelper;
use Illuminate\Http\Request;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query['data'] = Slide::all();
        return view('admin.slider.index',$query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'logo' => 'required',
            'link' => 'required',
            'description' => 'required',
        ]);
        $data = $request->all();

        $data['logo'] = ImageHelper::save_webp($data['logo'],Slide::$imagePath);

        Slide::create($data);
        return redirect('slider')->with('success', 'Slide Added Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function show(Slide $slide)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['single'] = Slide::find($id);
        // dd($query['single']);
        return view('admin.slider.add',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slide $slide,$id)
    {
        $this->validate($request, [
            'link' => 'required',
            'description' => 'required',
        ]);

        $data = $request->all();

        if(isset($data['logo']) && $data['logo']){
            if($slide->getOriginal('logo') !=  Slide::$defaultImage){
                ImageHelper::delete($slide->getOriginal('logo'),Slide::$imagePath);
            }
            $data['logo'] = ImageHelper::save_webp($data['logo'],Slide::$imagePath);
        }
// dd($data);
        $slide = Slide::find($id)->update($data);
        return redirect('slider')->with('success', 'Slider Edited Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slide $slide, $id)
    {
        $slide = Slide::find($id)->delete();
        // ImageHelper::delete($slide->getOriginal('image'), Slide::$imagePath);
        return redirect('slider')->with('success', 'Slider Deleted Succesfully');
    }
}
