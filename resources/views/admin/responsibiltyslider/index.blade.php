@extends('layouts.admin')
@extends('layouts.side-menu')
@section('content')
<!--begin::Toolbar-->
<div class="toolbar" id="kt_toolbar">
    <!--begin::Container-->
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <!--begin::Page title-->
        <div data-kt-place="true" data-kt-place-mode="prepend"
            data-kt-place-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
            class="page-title d-flex align-items-center me-3 flex-wrap mb-5 mb-lg-0 lh-1">
            <!--begin::Title-->
            <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Sliders</h1>
            <!--end::Title-->
            <!--begin::Separator-->
            <span class="h-20px border-gray-200 border-start mx-4"></span>
            <!--end::Separator-->

        </div>
        <!--end::Page title-->
    </div>
    <!--end::Container-->
</div>
<!--end::Toolbar-->
<div class="table-responsive manage-table">
    <table class="table">
        <thead>
            <tr>
                <th colspan="7" class="sm-pd"></th>
                <th colspan="7" class="sm-pd">Image</th>
                {{-- <th colspan="7" class="sm-pd">Title</th> --}}
                {{-- <th colspan="7" class="sm-pd">Secound Word</th> --}}
                <th colspan="7" class="sm-pd">Acctions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $key=>$value)

            <tr row="{{$value->id}}" data-table="responslider" class="advance-table-row active">
                <td colspan="7" class="sm-pd"></td>
                <td colspan="7" class="sm-pd">
                    <img src="{{asset('upload/responslider/'.$value->image)}}" class="img-circle" width="175" height="90" alt="slider img" />
                </td>
                {{-- <td colspan="7" class="sm-pd">{{$value->title}}</td> --}}
                {{-- <td colspan="7" class="sm-pd">{{$value->title2}}</td> --}}

                <td>
                    <a href="{{url('responslider').'/'.$value->id.'/edit'}}" class="btn btn-info btn-outline btn-circle btn-lg m-r-5 ">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit
                    </a>
                    <a onclick=" event.preventDefault(); var r = confirm('are you sure?'); if (r==true){document.getElementById('responslider-form{{$value->id}}').submit();}"
                        class="btn btn-danger btn-outline btn-circle btn-lg m-r-5 "> <i class="fa fa-trash"
                            aria-hidden="true"></i> Delete</a>

                    <form method="post" id="responslider-form{{$value->id}}" action="{{ url('responslider/'.$value->id) }}"
                        style="display: none;">
                        <input name="_method" type="hidden" value="DELETE">
                        {{ csrf_field() }}
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
