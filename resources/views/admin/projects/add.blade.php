@extends('layouts.admin')
@extends('layouts.side-menu')
@section('content')
<!--begin::Post-->
<div class="post d-flex flex-column-fluid" id="kt_post">
    <!--begin::Container-->
    <div id="kt_content_container" class="container">
        <!--begin::Basic info-->
        <div class="card mb-5 mb-xl-10">
            <!--begin::Card header-->
            <div class="card-header border-0">
                <!--begin::Card title-->
                <div class="card-title m-0">
                    <h3 class="fw-bolder m-0">
                        @if(isset($single))
                        Edit Service
                        @else
                        Add Service
                        @endif
                    </h3>
                </div>
                <!--end::Card title-->
            </div>
            <!--begin::Card header-->
            <!--begin::Content-->
            <div id="kt_account_profile_details" class="collapse show">
                <!--begin::Form-->
                <form class="form" @if(isset($single)) action="{{url('projects/'.$single->id)}}" @else
                    action="{{url('projects')}}" @endif" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    @if(isset($single))
                    <input type="hidden" name="_method" value="patch">
                    @endif
                    <!--begin::Card body-->
                    <div class="card-body border-top p-9">
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Title</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <input type="text" name="title" class="form-control form-control-lg form-control-solid"
                                    placeholder="Title"
                                    value="@if(isset($single) && ($single->title)){{$single->title}}@endif"/>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Map Mark</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <input type="text" name="mapmark" class="form-control form-control-lg form-control-solid"
                                    placeholder="Cairo, Egypt"
                                    value="@if(isset($single) && ($single->mapmark)){{$single->mapmark}}@endif"/>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Main Service</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <select class="form-control select2" id="serviceSelect" name="service_id">
                                    <option selected="" disabled="">* Choose One </option>
                                    @foreach($services as $service)
                                    <option value="{{$service->id}}"@if (isset($single) && $service->id == $single->service_id)) selected="" @endif>{{$service->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Short Description</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <textarea type="text" name="short_description"
                                    class="form-control form-control-lg form-control-solid ckeditor"
                                    placeholder="Short Description">
                                    @if(isset($single) && ($single->short_description)){!! $single->short_description !!}@endif
                                </textarea>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Full Description</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <textarea type="text" name="full_description"
                                    class="form-control form-control-lg form-control-solid ckeditor"
                                    placeholder="Description">
                                    @if(isset($single) && ($single->full_description)){!! $single->full_description !!}@endif
                                </textarea>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Map Embed Code</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <input type="text" name="link" class="form-control form-control-lg form-control-solid ckeditor"
                                    placeholder="Link"
                                    value="@if(isset($single) && ($single->link)){{$single->link}}@endif"/>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-2 col-form-label fw-bold fs-6">Project Images</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-10">
                                <!--begin::Image input-->
                                <div>
                                    <input type="file" accept="image/*" name="images[]" class="dropify"
                                    data-default-file= "{{asset('upload/default.jpg')}}" multiple />
                                </div>
                                <!--end::Image input-->
                                <!--begin::Hint-->
                                <div class="form-text">Multiple Image Upload - Allowed file types: png, jpg, jpeg.</div>
                                <!--end::Hint-->
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-2 col-form-label fw-bold fs-6">Slider Images</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-10">
                                <!--begin::Image input-->
                                <div>
                                    <input type="file" accept="image/*" name="sliderimages[]" class="dropify"
                                    data-default-file= "{{asset('upload/default.jpg')}}" multiple />
                                </div>
                                <!--end::Image input-->
                                <!--begin::Hint-->
                                <div class="form-text">Multiple Image Upload - Allowed file types: png, jpg, jpeg.</div>
                                <!--end::Hint-->
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">KEY STAKEHOLDERS</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <textarea type="text" name="stakeholders"
                                    class="form-control form-control-lg form-control-solid ckeditor"
                                    placeholder="stakeholders">
                                    @if(isset($single) && ($single->stakeholders)){!! $single->stakeholders !!}@endif
                                </textarea>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">CLIENT SECTOR</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <textarea type="text" name="sector"
                                    class="form-control form-control-lg form-control-solid ckeditor"
                                    placeholder="sector">
                                    @if(isset($single) && ($single->sector)){!! $single->sector !!}@endif
                                </textarea>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">PROJECT LOCATION</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <textarea type="text" name="location"
                                    class="form-control form-control-lg form-control-solid ckeditor"
                                    placeholder="location">
                                    @if(isset($single) && ($single->location)){!! $single->location !!}@endif
                                </textarea>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">PROJECT COMPLETION</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <textarea type="text" name="completion"
                                    class="form-control form-control-lg form-control-solid ckeditor"
                                    placeholder="completion">
                                    @if(isset($single) && ($single->completion)){!! $single->completion !!}@endif
                                </textarea>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">PROJECT DURATION</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <input type="text" name="duration" class="form-control form-control-lg form-control-solid"
                                    placeholder="PROJECT DURATION"
                                    value="@if(isset($single) && ($single->duration)){{$single->duration}}@endif"/>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">PROJECT SORT</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <input type="text" name="sort" class="form-control form-control-lg form-control-solid"
                                    placeholder="PROJECT SORT"
                                    value="@if(isset($single) && ($single->sort)){{$single->sort}}@endif"/>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">PROJECT Video</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <input type="text" name="video" class="form-control form-control-lg form-control-solid"
                                    placeholder="Video ID example: (76979871)"
                                    value="@if(isset($single) && ($single->video)){{$single->video}}@endif"/>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                    </div>
                    <!--end::Card body-->
                    <!--begin::Actions-->
                    <div class="card-footer d-flex justify-content-end py-6 px-9">
                        {{-- <button type="reset"
                            class="btn btn-white btn-active-light-primary me-2">Discard</button> --}}
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                    <!--end::Actions-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Basic info-->
    </div>
    <!--end::Container-->
</div>
<!--end::Post-->
@endsection
