@extends('layouts.admin')
@extends('layouts.side-menu')
@section('content')
<!--begin::Post-->
<div class="post d-flex flex-column-fluid" id="kt_post">
    <!--begin::Container-->
    <div id="kt_content_container" class="container">
        <!--begin::Basic info-->
        <div class="card mb-5 mb-xl-10">
            <!--begin::Card header-->
            <div class="card-header border-0">
                <!--begin::Card title-->
                <div class="card-title m-0">
                    <h3 class="fw-bolder m-0">
                        @if(isset($single))
                        Edit Service
                        @else
                        Add Service
                        @endif
                    </h3>
                </div>
                <!--end::Card title-->
            </div>
            <!--begin::Card header-->
            <!--begin::Content-->
            <div id="kt_account_profile_details" class="collapse show">
                <!--begin::Form-->
                <form class="form" @if(isset($single)) action="{{url('services/'.$single->id)}}" @else
                    action="{{url('services')}}" @endif" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    @if(isset($single))
                    <input type="hidden" name="_method" value="patch">
                    @endif
                    <!--begin::Card body-->
                    <div class="card-body border-top p-9">
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Title</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <input type="text" name="title" class="form-control form-control-lg form-control-solid"
                                    placeholder="Title"
                                    value="@if(isset($single) && ($single->title)){{$single->title}}@endif"/>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Category</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <select class="form-control select2" id="categorySelect" name="service_category_id">
                                    <option selected="" disabled="">* Choose One </option>
                                    @foreach($serviceCategory as $category)
                                    <option value="{{$category->id}}"@if(isset($single) && $category->id == $single->service_category_id)) selected="" @endif>{{$category->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Short Description</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <textarea type="text" name="short_description"
                                    class="form-control form-control-lg form-control-solid ckeditor"
                                    placeholder="Short Description">
                                    @if(isset($single) && ($single->short_description)){!! $single->short_description !!}@endif
                                </textarea>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">First Part Description</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <textarea type="text" name="first_part_description"
                                    class="form-control form-control-lg form-control-solid ckeditor"
                                    placeholder="Description">
                                    @if(isset($single) && ($single->first_part_description)){!! $single->first_part_description !!}@endif
                                </textarea>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Second Part Description</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <textarea type="text" name="second_part_description"
                                    class="form-control form-control-lg form-control-solid ckeditor"
                                    placeholder="Description">
                                    @if(isset($single) && ($single->second_part_description)){!! $single->second_part_description !!}@endif
                                </textarea>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-2 col-form-label fw-bold fs-6">Service Images</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-10">
                                <!--begin::Image input-->
                                <div>
                                    <input type="file" accept="image/*" name="images[]" class="dropify"
                                    data-default-file= "{{asset('upload/default.jpg')}}" multiple />
                                </div>
                                <!--end::Image input-->
                                <!--begin::Hint-->
                                <div class="form-text">Multiple Image Upload - Allowed file types: png, jpg, jpeg.</div>
                                <!--end::Hint-->
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Service Video</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8 fv-row">
                                <input type="text" name="video" class="form-control form-control-lg form-control-solid"
                                    placeholder="Video ID example: (76979871)"
                                    value="@if(isset($single) && ($single->video)){{$single->video}}@endif"/>
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                    </div>
                    <!--end::Card body-->
                    <!--begin::Actions-->
                    <div class="card-footer d-flex justify-content-end py-6 px-9">
                        {{-- <button type="reset"
                            class="btn btn-white btn-active-light-primary me-2">Discard</button> --}}
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                    <!--end::Actions-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Content-->
        </div>
        <!--end::Basic info-->
    </div>
    <!--end::Container-->
</div>
<!--end::Post-->
@endsection
