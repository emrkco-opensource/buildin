@extends('layouts.admin')
@extends('layouts.side-menu')
@section('content')
<!--begin::Toolbar-->
<div class="toolbar" id="kt_toolbar">
    <!--begin::Container-->
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <!--begin::Page title-->

        <div data-kt-place="true" data-kt-place-mode="prepend"
            data-kt-place-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
            class="page-title d-flex align-items-center me-3 flex-wrap mb-5 mb-lg-0 lh-1">

            <!--begin::Title-->
            <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Service Images</h1>
            <!--end::Title-->
            <!--begin::Separator-->
            <span class="h-20px border-gray-200 border-start mx-4"></span>
            <!--end::Separator-->

        </div>
        <!--end::Page title-->
    </div>
    <!--end::Container-->
</div>

<!--end::Toolbar-->
<div class="table-responsive manage-table">
    <table class="table">
        <thead>
            <tr>
                <th colspan="7" class="sm-pd"></th>
                <th colspan="7" class="sm-pd">IMAGE</th>
                <th colspan="7" class="sm-pd">DEFAULT</th>
                <th colspan="7" class="sm-pd">ACTIONS</th>
            </tr>
        </thead>
        <tbody>
            @foreach($images as $key=> $value)
            <tr row="{{$value->id}}" data-table="Services" class="advance-table-row active">
                <td colspan="7" class="sm-pd"></td>
                <td colspan="7" class="sm-pd">
                    <img src="{{$value->image}}" class="img-circle" width="175" height="90" alt="Service Image" />
                </td>
                <td colspan="7" class="sm-pd">
                    @if($value->role == 0)
                    Main Image
                    @else
                    Sub Image
                    @endif
                </td>
                <td>
                    @if($value->role == 1)
                        <a href="{{url('services_images/').'/'.$value->id.'/delete'}}" class="btn btn-danger">Delete</a>
                        <br>
                        <a href="{{url('services_images/').'/'.$value->id.'/default'}}" class="btn btn-primary">MAKE IT MAIN IMAGE</a>
                        <br>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
