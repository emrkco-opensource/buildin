@extends('layouts.master')

@section('content')
<div class="page_content">
    <!-- Slider
    ==========================================-->
    <section class="main_section">
        <div id="main_section" class="carousel slide" data-ride="carousel" data-pause="false" data-interval="7000">
            <ol class="carousel-indicators">
                @foreach ($sliders as $key => $slider)
                <li data-target="#main_section" data-slide-to="{{$slider->id}}" class="@if($key == 0)active @endif"></li>
                @endforeach
            </ol>
            <a class="carousel-control-prev icon_link" href="#main_section" role="button" data-slide="prev">
                <i class="fa fa-angle-left"></i>
            </a>
            <a class="carousel-control-next icon_link" href="#main_section" role="button" data-slide="next">
                <i class="fa fa-angle-right"></i>
            </a>
            <div class="carousel-inner">
            @foreach ($sliders as $key => $slider)
                <div class="carousel-item @if($key == 0)active @endif">
                    <img src="{{asset('upload/slider/'.$slider->logo)}}" class="animated zoomIn" style="animation-duration: 7s">
                    <div class="look_cont">
                        <h3 class="animated fadeInDown" style="animation-delay: 0.5s">
                            {!! strip_tags( $slider->description )!!}
                        </h3>
                        <a href="{{$slider->link}}" class="link animated fadeInDown" style="animation-delay: 1s">
                            <span> More Details <i class="fa fa-angle-right"></i> </span>
                        </a>
                    </div>
                </div>
            @endforeach
            </div>
    </div>
</section>
<!--End Section-->
<!-- Section About
    ==========================================-->
<section class="about">
    <div class="container">
        <div class="row">
            <div class="overlap">
                <h1>{{$settings->sitename}}</h1>
                <h3>
                    {{$settings->slogan}}
                </h3>
            </div>

            <div class="col-lg-6">
                <div class="about_img" data-aos="fade-up" data-aos-delay="25">
                    <img src="{{asset('upload/setting/'.$settings->image)}}" />
                    <p class="hint" data-aos="fade-up" data-aos-delay="50">
                        <span> our vision : </span>{!! strip_tags( $settings->vision )!!}
                    </p>
                </div>
                <!--End About Img-->
            </div>
            <!--End Col -->
            <div class="col-lg-6">
                <div class="about_content">
                    <div class="exp" data-aos="fade-up" data-aos-delay="25">
                        <b> {{$settings->since}} </b>
                        <p>
                            YEARS OF EXPERIENCE
                            <span> since 1999 </span>
                        </p>
                    </div>
                    <h3 data-aos="fade-up" data-aos-delay="50">
                        {{$settings->title}}
                    </h3>
                    <p data-aos="fade-up" data-aos-delay="75">
                        {!! strip_tags( $settings->description ) !!}
                    </p>
                    <a href="{{url('about')}}" class="link" data-aos="fade-up" data-aos-delay="125">
                        <span> More Details <i class="fa fa-angle-right"></i> </span>
                    </a>

                    <a href="{{url('ourProjects')}}" class="link black" data-aos="fade-up" data-aos-delay="150">
                        <span> Discover Projects <i class="fa fa-angle-right"></i> </span>
                    </a>
                </div>
            </div>
            <!--End Col-12-->
        </div>
        <!--End Row-->
    </div>
    <!--End Container-->
</section>
<!--End Section -->
<!-- Section Services
    ==========================================-->
<section class="section_color">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_title" data-aos="fade-up" data-aos-delay="30">
                    <h3>Our services</h3>
                    <p>
                        BUILDIN CONTRACTING is an Egyption based general and
                        specialized construction contractor .
                    </p>
                </div>
            </div>
            <!--End Col-12-->
            <div class="col-12" data-aos="fade-up" data-aos-delay="60">
                <div class="owl-carousel owl-theme services_slider">
                    @foreach ($servicescategory as $item)

                    <div class="item">
                        <div class="service_item">
                            <img src="{{asset('upload/serviceCategory/'.$item->logo)}}" />
                            <a href="{{url('service_main_category'.'/'.$item->id)}}"> {{$item->title}}</a>
                        </div>
                        <!--End Service Item-->
                    </div>
                    @endforeach

                    <!--End Item-->
                </div>
            </div>
        </div>
        <!--End Row-->
    </div>
    <!--End Container-->
</section>
<!--End Section -->
<!-- Section Features
    ==========================================-->
<section class="features">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="section_title" data-aos="fade-up" data-aos-delay="30">
                    <h3>Why Choose Us</h3>
                    <p>
                        BUILDIN CONTRACTING is an Egyption based general and
                        specialized construction contractor .
                    </p>
                </div>
                <!--End Section Title-->
                <div class="row">
                    @foreach ($why as $item)

                    <div class="col-lg-6 col-md-4 col-sm-6 aos-init" data-aos="fade-up" data-aos-delay="60">
                        <div class="feature_item">
                            <img src="{{asset('upload/feature/'.$item->logo)}}" tabindex="0" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-title="{{$item->title}}" data-content="{!! strip_tags( $item->description ) !!}" data-original-title="" title=""/>
                            <h3>{{$item->title}}</h3>
                        </div>
                        <!--End Feature Item-->
                    </div>
                    @endforeach

                    <!--End Feature Item-->
                </div>
            </div>
            <!--End Col-->
            <div class="col-lg-5" data-aos="fade-up" data-aos-delay="120">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ( $errors->all() as $error )
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if(\session('success'))
                <div class="alert alert-success">
                    {{\session('success')}}
                </div>
                @endif
                @if(\session('error'))
                <div class="alert alert-danger">
                    {{\session('error')}}
                </div>
                @endif
                <form class="contact_form" action="{{url('contactUSform')}}" method="post"
                    enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form_title">Speak with our Experts</div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Full Name" name="name" />
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="  Email Address" name="email" />
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" placeholder="Phone Number" name="phone" />
                    </div>
                    <div class="form-group">
                        <select class="form-control select2" id="serviceSelect" name="service_id">
                            <option selected="" disabled="">* Choose One </option>
                            @foreach($services as $service)
                            <option value="{{$service->id}}">{{$service->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message"></textarea>
                    </div>
                    <button class="link">
                        <span> Send Message <i class="fa fa-angle-right"></i> </span>
                    </button>
                </form>
            </div>
            <!--End Col-->
        </div>
        <!--End Row-->
    </div>
    <!--End Container-->
</section>
<!--End Section -->
<!-- Section Projects
    ==========================================-->
<section class="section_color">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_title" data-aos="fade-up" data-aos-delay="30">
                    <h3>Our Projects</h3>
                    <p>
                        BUILDIN CONTRACTING is an Egyption based general and
                        specialized construction contractor .
                    </p>
                </div>
            </div>
            <!--End Col-12-->
            <div class="col-12" data-aos="fade-up" data-aos-delay="60">
                <div class="owl-carousel owl-theme projects_slider">
                    @foreach ($projects as $project)

                    <div class="item">
                        <a href="{{url('singleProject/'.$project->id)}}" class="project_item">
                            <div class="cover">
                                <img src="{{$project->main_image[0]['image']}}" />
                                <div class="desc">
                                    <span>
                                        <i class="fa fa-map-marker-alt"></i>
                                        {!! strip_tags( $project->mapmark ) !!}
                                    </span>
                                    {!! strip_tags( $project->short_description ) !!}
                                </div>
                            </div>
                            <!--End Cover-->
                            <div class="cont">
                                <h3>{{$project->title}}</h3>
                                <p>
                                    <i class="fa fa-link"> </i>
                                    {{$project->mainService->title}}
                                </p>
                            </div>
                            <!--end Cont-->
                        </a>
                        <!--End Project Item-->
                    </div>
                    @endforeach
                    <!--End Item-->
                </div>
            </div>
            <!--Ed Col-->
            <div class="col-12" data-aos="fade-up" data-aos-delay="90">
                <a href="{{url('ourProjects')}}" class="link more">
                    <span> Load More <i class="fa fa-angle-right"></i></span>
                </a>
            </div>
        </div>
        <!--End Row-->
    </div>
    <!--End Container-->
</section>
<!--End Section -->
<!-- Section Clients
    ==========================================-->
<section class="clients">
    <div class="container">
        <div class="row">
            <div class="col-12" data-aos="fade-up" data-aos-delay="30">
                <div class="section_title text-center">
                    <h3>Partners & Clients</h3>
                    <p>
                        BUILDIN CONTRACTING is an Egyption based general and
                        specialized construction contractor .
                    </p>
                </div>
            </div>
            <!--End Col-12-->
            <div class="col-12" data-aos="fade-up" data-aos-delay="60">
                <div class="owl-carousel owl-theme clients_slider">
                    @foreach ($partners as $partner)
                    <div class="item">
                        <div class="client_item">
                            <img src="{{asset('upload/partner/'.$partner->logo)}}" />
                        </div>
                    </div>
                    @endforeach
                    <!--End Item-->
                </div>
                <!--End Owl-->
            </div>
            <!--End Col-->
        </div>
        <!--End Row-->
    </div>
    <!--End Container-->
</section>
<!--End Section -->
</div>
@endsection
