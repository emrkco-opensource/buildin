@extends('layouts.master')

@section('content')
<div class="page_content">
    <div class="page_head" style="background-image:url('{{url('upload/page/'.$page->image)}}'); background-position-x: center;
        background-position-y: center;
        background-size: cover;
        background-repeat-x: no-repeat;
        background-repeat-y: no-repeat;
        background-attachment: fixed;">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3>{{$page->title}}</h3>
                    {!!$page->description!!}
                </div>
                <!--End Col-->
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
    </div>
    <!--End Page Head-->
    <!-- Section Clients
    ==========================================-->
    <section class="clients">
      <div class="container">
        <div class="row">
            @foreach ($data as $item)
            <div class="col-md-4 col-sm-6 aos-init" data-aos="fade-up" data-aos-delay="20">
                <div class="client_item">
                    <img src="{{asset('upload/partner/'.$item->logo)}}">
                </div>
            </div>
            @endforeach
          <!--End col-->
        </div>
        <!--End Row-->
      </div>
      <!--End Container-->
    </section>
    <!--End Section -->
  </div>
@endsection
