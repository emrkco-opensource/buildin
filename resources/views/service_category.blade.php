@extends('layouts.master')

@section('content')
<div class="page_content">
    <div class="page_head" style="background-image:url('{{url('upload/serviceCategory/'.$category->logo)}}'); background-position-x: center;
        background-position-y: center;
        background-size: cover;
        background-repeat-x: no-repeat;
        background-repeat-y: no-repeat;
        background-attachment: fixed;">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3>{{$category->title}}</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="service">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p data-aos="fade-up" data-aos-delay="30" class="aos-init">
                    buildin have executed numerous large and complex projects
                    through which we developed a wide- range of niche core
                    competences that enhance our market positioning for our clients.
                </p>
            </div>
            <!--End Col-->
            <div class="col-12 aos-init" data-aos="fade-up" data-aos-delay="60">
                <div class="owl-carousel owl-theme services_slider owl-loaded owl-drag">

                    <!--End Item-->
                    <div class="owl-stage-outer">
                        <div class="owl-stage"
                            style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1140px;">
                            @foreach ($category['services'] as $item)
                            <div class="owl-item active" style="width: 255px; margin-right: 30px;">
                                <div class="item">
                                    <div class="service_item">
                                        <img src="{{$item->main_image[0]['image']}}">
                                        <a href="{{url('singleService'.'/'.$item->id)}}">
                                            {{$item->title}}
                                        </a>
                                    </div>
                                    <!--End Service Item-->
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                    <div class="owl-nav disabled"><button type="button" role="presentation"
                            class="owl-prev icon_link disabled"><span aria-label="Previous">‹</span></button><button
                            type="button" role="presentation" class="owl-next icon_link disabled"><span
                                aria-label="Next">›</span></button></div>
                    <div class="owl-dots disabled"></div>
                </div>
            </div>
        </div>
        <!--End Row-->
    </div>
    <!--End Container-->
</section>
@endsection
