<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <!-- Meta Tags
        ==============================-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords" content="" />
    <meta name="copyright" content="" />
    <title>{{$allSettings->sitename}}</title>

    <!-- Fave Icons
    ================================-->
    <link rel="shortcut icon" href="{{asset('assets')}}/images/favicon.png" />

    <!-- CSS Files
    ================================-->
    <link rel="stylesheet" href="{{asset('assets')}}/vendor/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{asset('assets')}}/vendor/aos/animate.css" />
    <link rel="stylesheet" href="{{asset('assets')}}/vendor/aos/aos.css" />
    <link rel="stylesheet" href="{{asset('assets')}}/vendor/owl/owl.carousel.css" />

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="{{asset('assets')}}/css/style.css" />
</head>

<body>
    <div class="preloader">
        <div class="load_cont">
            <img src="{{asset('upload/setting/'.$allSettings->logo)}}" />
            <div class="icon"></div>
        </div>
    </div>
    <!-- Header
    ==========================================-->
    <div class="top_header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ul class="top_content">
                        <li>
                            <a target="_blank" href="{{$allSettings->addresslink}}">
                                <i class="fa fa-map-marker-alt"></i>
                                <span class="tit"> Find Us</span>
                                <span> {{$allSettings->address}} </span>
                            </a>
                        </li>
                        <li>
                            <a href="mailto:{{$allSettings->email}}">
                                <i class="fa fa-envelope"></i>
                                <span class="tit"> Mail Us </span>
                                <span> {{$allSettings->email}} </span>
                            </a>
                        </li>
                        <li class="call">
                            <a href="https://wa.me/{{$allSettings->whatsapp}}" target="_blank">
                                <i class="fab fa-whatsapp"></i> Chat With us
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <header>
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="{{url('/')}}" class="logo">
                        <img src="{{asset('upload/setting/'.$allSettings->logo)}}" />
                    </a>
                    <div class="header_widget">
                        {{-- <form>
                            <div class="form-group">
                                <input type="text" placeholder="Find" class="form-control" />
                                <button class="icon_link fa fa-search"></button>
                            </div>
                        </form>
                        <button class="icon_link fa fa-search search_btn"></button> --}}
                        <button class="menu-btn icon_link" type="button" data-toggle="collapse" data-target="#main-nav">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                    <!--End Header Widget-->
                </div>
                <!--End Col-->
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="collapse navbar-collapse" id="main-nav">
                            <ul class="navbar-nav">
                                <li class="@if(Route::is('home'))active @endif">
                                    <a href="{{url('/')}}"> Home </a>
                                </li>
                                <li class="@if(Route::is('about'))active @endif"><a href="{{url('about')}}">About us
                                    </a></li>

                                <li
                                    class="dropdown @if(Route::is('service_main_category') || Route::is('singleService'))active @endif">
                                    <a href="#" class="extra" data-toggle="dropdown">
                                        Services
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <div class="row">
                                            @foreach ($serviceCategories as $item)
                                            <div class="col-lg-3 col-md-6 col-sm-6">
                                                <a href="{{url('service_main_category'.'/'.$item->id)}}"
                                                    class="main_serv">
                                                    {{$item->title}}</a>
                                                    @foreach($item->services as $serv)
                                                    <a href="{{url('singleService'.'/'.$serv->id)}}" class="dropdown-item">
                                                        {{$serv->title}}
                                                    </a>
                                                    @endforeach
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </li>
                                <li class="@if(Route::is('ourProjects') || Route::is('singleProject'))active @endif"><a
                                        href="{{url('ourProjects')}}"> Projects </a></li>
                                <li class="@if(Route::is('clients'))active @endif"><a href="{{url('clients')}}"> Clients
                                    </a></li>
                                <li class="@if(Route::is('ourCertificates'))active @endif"><a
                                        href="{{url('ourCertificates')}}"> Certificates </a></li>
                                <li class="@if(Route::is('responsibilities'))active @endif"><a
                                        href="{{url('responsibilities')}}"> Responsibilities </a></li>
                                <li class="@if(Route::is('ourCareers'))active @endif"><a href="{{url('ourCareers')}}">
                                        Careers </a></li>
                                <li class="@if(Route::is('contactUSpage'))active @endif"><a
                                        href="{{url('contactUSpage')}}"> Contact us </a></li>
                            </ul>
                        </div>
                    </div>
                    <!--End Col-->
                </div>
                <!--End Row-->
            </div>
            <!--End Container-->
        </nav>
        <!--End Nav-->
    </header>
    <!--End Header-->
    @yield('content')
    <!-- Footer
    ==========================================-->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="item white">
                        <a href="{{url('/')}}" class="logo">
                            <img src="{{asset('upload/setting/'.$allSettings->logo)}}" />
                        </a>
                        <div class="row">
                            <div class="col-lg-12 col-md-8">
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ( $errors->all() as $error )
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if(\session('success'))
                                <div class="alert alert-success">
                                    {{\session('success')}}
                                </div>
                                @endif
                                @if(\session('error'))
                                <div class="alert alert-danger">
                                    {{\session('error')}}
                                </div>
                                @endif
                                <form class="contact_form" action="{{url('newsletter')}}" method="post"
                                    enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="form-group">
                                        <label>LATEST NEWS Letters</label>
                                        <input type="email" name="email" placeholder="Email Address"
                                            class="form-control" />
                                        <button class="link">
                                            <span> <i class="fa fa-envelope"></i> Subscribe </span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-12 col-md-4">
                                <ul class="social">
                                    <li>
                                        <a href="{{$allSettings->facebook}}" class="icon_link icon-facebook"
                                            target="_blank">
                                            <i class="fab fa-facebook"></i>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{$allSettings->instagram}}" class="icon_link icon-instagram"
                                            target="_blank"><i class="fab fa-instagram"></i></a>
                                    </li>
                                    <li>
                                        <a href="{{$allSettings->linkedin}}" class="icon_link icon-linkedin"
                                            target="_blank"><i class="fab fa-linkedin"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End col-md-4-->
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="item">
                                <h3>Quick Links</h3>
                                <ul class="quick_links row">
                                    <li class="col-md-6 col-sm-4 col-6">
                                        <a href="{{url('/')}}"> Home </a>
                                    </li>
                                    <li class="col-md-6 col-sm-4 col-6">
                                        <a href="{{url('about')}}"> About us </a>
                                    </li>
                                    <li class="col-md-6 col-sm-4 col-6">
                                        <a href="{{url('ourProjects')}}"> Projects </a>
                                    </li>
                                    <li class="col-md-6 col-sm-4 col-6">
                                        <a href="{{url('clients')}}"> Clients </a>
                                    </li>
                                    <li class="col-md-6 col-sm-4 col-6">
                                        <a href="{{url('ourCertificates')}}"> Certificates </a>
                                    </li>
                                    <li class="col-md-6 col-sm-4 col-6">
                                        <a href="{{url('responsibilities')}}"> Responsibilities </a>
                                    </li>
                                    <li class="col-md-6 col-sm-4 col-6">
                                        <a href="{{url('ourCareers')}}"> Careers </a>
                                    </li>
                                    <li class="col-md-6 col-sm-4 col-6">
                                        <a href="{{url('contactUSpage')}}">Contact us</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="item">
                                <h3>Contact Info</h3>
                                <ul class="contact_info">
                                    <li>
                                        <i class="fa fa-map-marker-alt"></i>
                                        <a target="_blank" href="{{$allSettings->addresslink}}">
                                            <span class="tit"> Find Us</span>
                                            <span> {{$allSettings->address}} </span>
                                        </a>
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope"></i>
                                        <a href="mailto:{{$allSettings->email}}">
                                            <span class="tit"> Mail Us </span>
                                            <span> {{$allSettings->email}} </span>
                                        </a>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker-alt"></i>
                                        <a href="tel:{{$allSettings->phone}}">
                                            <span class="tit"> Call Us </span>
                                            <span>{{$allSettings->phone}}</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 copyright">
                            Copyright © 2022 {{$allSettings->sitename}}
                        </div>
                    </div>
                    <!--End Row-->
                </div>
                <!--End Col-md-8-->
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
    </footer>
    <!--End Footer-->
    <button class="up_btn icon_link">
        <i class="fa fa-angle-up"></i>
    </button>

    <!-- Mouse Cursor
      ==========================================-->
    <div class="cursor"></div>
    <!-- JS & Vendor Files
      ==========================================-->
    <script src="{{asset('assets')}}/vendor/jquery.js"></script>
    <script src="{{asset('assets')}}/vendor/bootstrap/popper.min.js"></script>
    <script src="{{asset('assets')}}/vendor/bootstrap/bootstrap.min.js"></script>
    <script src="{{asset('assets')}}/vendor/owl/owl.carousel.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script src="{{asset('assets')}}/vendor/aos/aos.js"></script>
    <script src="{{asset('assets')}}/js/main.js"></script>
    <script>
        $('[data-toggle="popover"]').popover({
            trigger: "focus",
        });

    </script>
</body>

</html>
