@extends('layouts.master')
@section('content')
<div class="page_content">
    <div class="page_head" style="background-image:url('{{$service->main_image[0]->image}}'); background-position-x: center;
        background-position-y: center;
        background-size: cover;
        background-repeat-x: no-repeat;
        background-repeat-y: no-repeat;
        background-attachment: fixed;">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3 class="animated fadeInRight" style="animation-duration: 1s; animation-delay: 1s;">{{$service->title}}</h3>
                    <p class="animated fadeInRight" style="animation-duration: 1s; animation-delay: 1.5s;">
                        {!!$service->short_description!!}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="service">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 aos-init list" data-aos="fade-up" data-aos-delay="30">
                <h3>{{$service->title}}</h3>
                {!! $service->first_part_description !!}

                <div class="row">
                    @foreach ($service->image as $item)

                    <div class="col-md-4 col-sm-6">
                        <a href="{{$item->image}}" data-fancybox="gallery" class="inner_gall">
                            <img src="{{$item->image}}">
                        </a>
                    </div>
                    @endforeach
                    <!--End Col-->
                </div>
                <!--End Row-->
                {!! $service->second_part_description !!}
                @if(isset($service->video))
                    <h3>Video</h3>
                    <iframe src="https://player.vimeo.com/video/{{$service->video}}" width="100%" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                @endif
            </div>

            <div class="col-lg-3 aos-init" data-aos="fade-up" data-aos-delay="30">
                <div class="services_list">
                    <h3>{{$service->serviceCategory->title}}</h3>
                    @foreach ($links as $item)
                    <a href="{{url('singleService'.'/'.$item->id)}}" @if($service->id == $item->id) class="active" @endif>{{$item->title}}</a>
                    @endforeach
                    <a href="{{url('contactUSpage')}}" class="link">
                        <span> Request Service </span>
                    </a>
                </div>
            </div>
            <!--End Col-->
        </div>
        <!--End Row-->
    </div>
    <!--End Container-->
</section>
@endsection
