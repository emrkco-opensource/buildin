@extends('layouts.master')
@section('content')
<div class="page_content">
    <div class="page_head" style="background-image:url('{{url($project->main_image[0]->image)}}'); background-position-x: center;
        background-position-y: center;
        background-size: cover;
        background-repeat-x: no-repeat;
        background-repeat-y: no-repeat;
        background-attachment: fixed;">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3>{{$project->title}}</h3>
                    <p>
                        {!! strip_tags( $project->short_description ) !!}
                    </p>
                </div>
                <!--End Col-->
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
    </div>
    <!--End Page Head-->

    <!-- Section Project
    ==========================================-->
    <section class="project">
        <div class="container">
            <div class="row">
                <div class="col-12 aos-init" data-aos="fade-up" data-aos-delay="30">
                    <div id="main_section" class="carousel slide main_section project_slider" data-ride="carousel"
                        data-pause="false" data-interval="7000">
                        <a class="carousel-control-prev icon_link" href="#main_section" role="button" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="carousel-control-next icon_link" href="#main_section" role="button" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                        <div class="carousel-inner">
                            {{-- <div class="carousel-item">
                                <a href="{{$project->main_image[0]['image']}}" data-fancybox="gallery">
                                    <i class="fas fa-expand"></i>
                                    <img src="{{$project->main_image[0]['image']}}">
                                </a>
                            </div> --}}
                            <!--End Item-->
                            @foreach ($sliderimgs as $key => $item)
                            <div class="carousel-item @if($key == 0)active @endif">
                                <a href="{{$item->image}}" data-fancybox="gallery">
                                    <i class="fas fa-expand"></i>
                                    <img src="{{$item->image}}">
                                </a>
                            </div>
                            @endforeach
                        </div>
                        <!--End Div-->
                    </div>
                </div>
                <!--End Col-->
            </div>
            <div class="row">
                <div class="col-lg-8 aos-init list" data-aos="fade-up" data-aos-delay="60">
                    <ul class="project_info row">
                        <li class="col-12">
                          <i class="fa fa-user"></i>
                          <div class="cont">
                            <span> key stakeholders  </span>
                            {!! strip_tags( $project->stakeholders ) !!}
                          </div>
                        </li>
                        <li class="col-md-6">
                          <i class="fa fa-info"></i>
                          <div class="cont">
                            <span> Client Sector </span>
                            {!! strip_tags( $project->sector ) !!}
                          </div>
                        </li>
                        <li class="col-md-6">
                          <i class="fa fa-map-marker-alt"></i>
                          <div class="cont">
                            <span> Project Location </span>
                            {!! strip_tags( $project->location ) !!}
                          </div>
                        </li>
                        <li class="col-md-6">
                          <i class="fa fa-clock"></i>
                          <div class="cont">
                            <span> Project Duration </span>
                            {!! strip_tags( $project->duration ) !!}
                          </div>
                        </li>
                        <li class="col-md-6">
                          <i class="fa fa-calendar"></i>
                          <div class="cont">
                            <span> Project Completion</span>
                            {!! strip_tags( $project->completion ) !!}
                          </div>
                        </li>
                      </ul>
                    {!! $project->full_description !!}
                    <div class="row">
                        @foreach ($imgs as $item)

                        <div class="col-md-4 col-sm-6">
                            <a href="{{$item->image}}" data-fancybox="gallery" class="inner_gall">
                                <img src="{{$item->image}}">
                            </a>
                        </div>
                        @endforeach
                        <!--End Col-->
                    </div>
                    <!--End Row-->
                    @if(isset($project->video))
                        <h3>Video</h3>
                        <iframe src="https://player.vimeo.com/video/{{$project->video}}" width="100%" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                    @endif
                    @if(isset($project->link))
                        <h3>Location</h3>
                        <iframe src="{!!$project->link!!}" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    @endif
                </div>
                <div class="col-lg-4 aos-init" data-aos="fade-up" data-aos-delay="90">
                    <div class="poject_serv">
                        <h3>Project Service</h3>
                        <img src="{{$project->service_image[0]['image']}}">
                        <h4>{{$project->mainService->title}}</h4>
                        <p>
                            {!! strip_tags( $project->mainService->short_description ) !!}
                        </p>
                        <a href="{{url('singleService'.'/'.$project->mainService->id)}}" class="link">
                            <span> Get a service <i class="fa fa-angle-right"></i> </span>
                        </a>
                    </div>
                </div>
                <!--End Col-->
            </div>
        </div>
        <!--End Container-->
    </section>
    <!--End Section -->
    <!-- Section Projects
    ==========================================-->
    <section class="section_color">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_title aos-init" data-aos="fade-up" data-aos-delay="30">
                        <h3>Related Projects</h3>
                        <p>
                            BUILDIN CONTRACTING is an Egyption based general and
                            specialized construction contractor .
                        </p>
                    </div>
                </div>
                <!--End Col-12-->
                <div class="col-12 aos-init" data-aos="fade-up" data-aos-delay="60">
                    <div class="owl-carousel owl-theme projects_slider owl-loaded owl-drag">

                        <!--End Item-->
                        <div class="owl-stage-outer">
                            <div class="owl-stage"
                                style="transform: translate3d(-1140px, 0px, 0px); transition: all 0s ease 0s; width: 4560px;">
                                @foreach ($projects as $key => $project)
                                <div class="owl-item cloned" style="width: 350px; margin-right: 30px;">

                                    <div class="item">
                                        <a href="{{url('singleProject/'.$project->id)}}" class="project_item">
                                            <div class="cover">
                                                <img src="{{$project->main_image[0]['image']}}">
                                                <div class="desc">
                                                    <span>
                                                        <i class="fa fa-map-marker-alt"></i>
                                                        {!! strip_tags( $project->mapmark ) !!}
                                                    </span>
                                                    <p>
                                                        {!! $project->short_description !!}
                                                    </p>
                                                </div>
                                            </div>
                                            <!--End Cover-->
                                            <div class="cont">
                                                <h3>{{$project->title}}</h3>
                                                <p>
                                                    <i class="fa fa-link"> </i> {{$project->mainService->title}}
                                                </p>
                                            </div>
                                            <!--end Cont-->
                                        </a>
                                        <!--End Project Item-->
                                    </div>

                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="owl-nav">
                        <button type="button" role="presentation" class="owl-prev icon_link"><span
                                aria-label="Previous">‹</span>
                        </button>
                        <button type="button" role="presentation" class="owl-next icon_link"><span
                                aria-label="Next">›</span>
                        </button>
                    </div>
                    <div class="owl-dots disabled">
                    </div>
                </div>
            </div>
            <!--Ed Col-->
        </div>
        <!--End Row-->
</div>
<!--End Container-->
</section>
<!--End Section -->
</div>
@endsection
