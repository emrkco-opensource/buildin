@extends('layouts.master')

@section('content')
<div class="page_content">
    <div class="page_head" style="background-image:url('{{url('upload/page/'.$page->image)}}'); background-position-x: center;
        background-position-y: center;
        background-size: cover;
        background-repeat-x: no-repeat;
        background-repeat-y: no-repeat;
        background-attachment: fixed;">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3>{{$page->title}}</h3>
                    {!!$page->description!!}
                </div>
                <!--End Col-->
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
    </div>
    <!--End Page Head-->
  </div>
  <section class="service">
    <div class="container">
      <div class="row">
        <div class="col-12 aos-init" data-aos="fade-up" data-aos-delay="30">
          <div id="main_section" class="carousel slide main_section project_slider" data-ride="carousel" data-pause="false" data-interval="7000">
            <a class="carousel-control-prev icon_link" href="#main_section" role="button" data-slide="prev">
              <i class="fa fa-angle-left"></i>
            </a>
            <a class="carousel-control-next icon_link" href="#main_section" role="button" data-slide="next">
              <i class="fa fa-angle-right"></i>
            </a>
            <div class="carousel-inner">
            @foreach ($reslider as $key => $item)
              <div class="carousel-item @if($key == 0)active @endif">
                <a href="{{url('upload/responslider/'.$item->image)}}" data-fancybox="gallery">
                  <i class="fas fa-expand"></i>
                  <img src="{{url('upload/responslider/'.$item->image)}}">
                </a>
              </div>
            @endforeach
              <!--End Item-->
            </div>
            <!--End Div-->
          </div>
        </div>
        <!--End Col-->
      </div>
      <div class="row">
        <div class="col-lg-12 aos-init pt-3 pb-3" data-aos="fade-up" data-aos-delay="30">
          <h4>
            By 2024, we aim to be recognized as an industry leader under three
            key policy areas:
          </h4>
        </div>

        <div class="col-lg-8 aos-init list" data-aos="fade-up" data-aos-delay="60">
          <h4 class="colored">
              {{$respo->title}}
            <img class="float-left" style="margin-top:-5px" src="{{url('upload/responsibility/'.$respo->icon)}}">
          </h4>
          {!! $respo->description !!}
        </div>
        <div class="col-lg-4 aos-init" data-aos="fade-up" data-aos-delay="60">
          <img src="{{url('upload/responsibility/'.$respo->image)}}" class="inner_gall">
        </div>
        <div class="col-lg-8 aos-init list" data-aos="fade-up" data-aos-delay="90">
          <h4 class="colored">
              {{$respo->title1}}
            <img class="float-left" style="margin-top:-5px" src="{{url('upload/responsibility/'.$respo->icon1)}}">
          </h4>
          {!! $respo->description1 !!}
        </div>
        <div class="col-lg-4 aos-init" data-aos="fade-up" data-aos-delay="90">
          <img src="{{url('upload/responsibility/'.$respo->image1)}}" class="inner_gall">
        </div>
        <div class="col-lg-8 aos-init list" data-aos="fade-up" data-aos-delay="120">
          <h4 class="colored">
              {{$respo->title2}}
            <img class="float-left" style="margin-top:-5px" src="{{url('upload/responsibility/'.$respo->icon2)}}">
          </h4>
          {!! $respo->description2 !!}
        </div>
        <div class="col-lg-4 aos-init" data-aos="fade-up" data-aos-delay="120">
          <img src="{{url('upload/responsibility/'.$respo->image2)}}" class="inner_gall">
        </div>
      </div>
      <!--End Row-->
    </div>
    <!--End Container-->
  </section>
@endsection
