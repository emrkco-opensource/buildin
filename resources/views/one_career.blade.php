@extends('layouts.master')
@section('content')
<div class="page_content">
    <div class="page_head">
        <div class="container">
            <div class="row text-center">
                <div class="col-12 list">
                    <h3>CAREERS</h3>
                    <p>
                        " BUILDIN CONTRACTING is an Egyption based general and specialized construction contractor "
                    </p>
                </div>
                <!--End Col-->
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
    </div>
    <!--End Page Head-->

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ( $errors->all() as $error )
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
    @endif

    <section>
        <div class="container">
          <div class="row">
            <div class="col-lg-7 aos-init aos-animate" data-aos="fade-up" data-aos-delay="30">
              <div class="about_content list">
                <h3>{{$career->title}}</h3>
                {!! $career->description !!}
              </div>
            </div>
            <div class="col-lg-5 aos-init aos-animate" data-aos="fade-up" data-aos-delay="60">
              <form class="job_form"action="{{url('careersform')}}" method="post"
              enctype="multipart/form-data">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
              <input type="hidden" name="career_id" value="{{$career->id}}">
                <div class="form_title">Apply For Job</div>
                <div class="form-group">
                  <label> Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Full Name">
                </div>
                <div class="form-group">
                  <label> Email Address </label>
                  <input type="email" class="form-control" name="email" placeholder="Email Address">
                </div>
                <div class="form-group">
                  <label> Phone Number </label>
                  <input type="number" class="form-control" name="phone" placeholder="Phone Number">
                </div>
                <div class="form-group">
                  <label> Upload CV </label>
                  <input type="file" name="file"/>
                </div>
                <!--End Form Group-->
                <button class="link">
                  <span> Apply Now <i class="fa fa-angle-right"></i> </span>
                </button>
              </form>
            </div>
            <!--End col-->
          </div>
          <!--End Row-->
        </div>
        <!--End Container-->
      </section>
</div>
@endsection
