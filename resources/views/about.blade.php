@extends('layouts.master')

@section('content')
<div class="page_content">
    <div class="page_head" style="background-image:url('{{url('upload/page/'.$page->image)}}'); background-position-x: center;
        background-position-y: center;
        background-size: cover;
        background-repeat-x: no-repeat;
        background-repeat-y: no-repeat;
        background-attachment: fixed;">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3>{{$page->title}}</h3>
                    {!!$page->description!!}
                </div>
                <!--End Col-->
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
    </div>
    <!--End Page Head-->

    <!-- Section About
    ==========================================-->
    <section class="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="about_img aos-init" data-aos="fade-up" data-aos-delay="25">
                        <img src="{{asset('upload/setting/'.$settings->image)}}">
                    </div>
                    <!--End About Img-->
                </div>
                <!--End Col -->
                <div class="col-lg-6">
                    <div class="about_content">
                        <div class="exp aos-init" data-aos="fade-up" data-aos-delay="25">
                            <b> {{$settings->since}} </b>
                            <p>
                                YEARS OF EXPERIENCE
                                <span> since 1999 </span>
                            </p>
                        </div>
                        <h3 data-aos="fade-up" data-aos-delay="50" class="aos-init">
                            {{$settings->title}}
                        </h3>
                        <p data-aos="fade-up" data-aos-delay="75" class="aos-init">
                            {!! strip_tags( $settings->description ) !!}
                        </p>

                        <a href="{{url('ourProjects')}}" class="link aos-init" data-aos="fade-up" data-aos-delay="150">
                            <span>
                                Discover Projects <i class="fa fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <!--End Col-->
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
    </section>

    <!-- Section Features
    ==========================================-->
    <section class="features">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="section_title aos-init" data-aos="fade-up" data-aos-delay="30">
                        <h3>Why Choose Us</h3>
                    </div>
                    <!--End Section Title-->
                </div>
                @foreach ($why as $item)

                <div class="col-lg-4 col-md-6 col-sm-6 aos-init" data-aos="fade-up" data-aos-delay="60">
                    <div class="feature_item">
                        <img src="{{asset('upload/feature/'.$item->logo)}}">
                        <h3>{{$item->title}}</h3>
                        <p>
                            @if (isset($item->description))
                            {!! strip_tags( $item->description ) !!}
                            @endif
                        </p>
                    </div>
                    <!--End Feature Item-->
                </div>
                @endforeach
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
    </section>
    <!--End Section -->

    <!-- Section About
    ==========================================-->
    <section class="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="about_content">
                        <div class="section_title aos-init" data-aos="fade-up" data-aos-delay="30">
                            <h3>Core Value</h3>
                            <p>
                                Our company values enable us to offer the best possible
                                service to our clients, whilst supporting our team.
                            </p>
                        </div>
                        <div class="accordion aos-init" id="core" data-aos="fade-up" data-aos-delay="60">
                            <div class="panel">
                                <a href="#toggle1" data-toggle="collapse" class="panel-title"> // {{$valuefirst->title}}
                                </a>
                                <!--End panel-title-->
                                <div class="panel-collapse collapse show" id="toggle1" data-parent="#core">
                                    {!! strip_tags( $valuefirst->description ) !!}
                                </div>
                                <!--End Panel Collapse-->
                            </div>
                            <!--End Panel-->
                            @foreach ($ourvalue as $item)
                            <div class="panel">
                                <a href="#toggle{{$item->id}}" data-toggle="collapse" class="collapsed panel-title"> //
                                    {{$item->title}}
                                </a>
                                <!--End panel-title-->
                                <div class="panel-collapse collapse list" id="toggle{{$item->id}}" data-parent="#core">
                                    {!! $item->description !!}
                                </div>
                                <!--End Panel Collapse-->
                            </div>
                            @endforeach
                            <!--End Panel-->
                        </div>
                        <!--End Accordion-->
                    </div>
                    <!--End About Content-->
                </div>
                <!--End Col-->
                <div class="col-lg-5">
                    <div class="about_content vm">
                        <h4 data-aos="fade-up" data-aos-delay="90" class="aos-init">Our Mission</h4>
                        <p data-aos="fade-up" data-aos-delay="120" class="aos-init">
                            {!! strip_tags( $settings->mission ) !!}
                        </p>
                    </div>
                    <div class="about_content vm">
                        <h4 data-aos="fade-up" data-aos-delay="150" class="aos-init">Our Vision</h4>
                        <p data-aos="fade-up" data-aos-delay="180" class="aos-init">
                            {!! strip_tags( $settings->vision ) !!}
                        </p>
                    </div>
                </div>
                <!--End Col-->
            </div>
        </div>
        <!--End Row-->
    </section>
</div>
@endsection
