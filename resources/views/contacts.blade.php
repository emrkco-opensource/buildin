@extends('layouts.master')

@section('content')
<div class="page_content">
    <div class="page_head" style="background-image:url('{{url('upload/page/'.$page->image)}}'); background-position-x: center;
        background-position-y: center;
        background-size: cover;
        background-repeat-x: no-repeat;
        background-repeat-y: no-repeat;
        background-attachment: fixed;">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3>{{$page->title}}</h3>
                    {!!$page->description!!}
                </div>
                <!--End Col-->
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
    </div>
    <!--End Page Head-->

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ( $errors->all() as $error )
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(\session('success'))
    <div class="alert alert-success">
        {{\session('success')}}
    </div>
    @endif
    @if(\session('error'))
    <div class="alert alert-danger">
        {{\session('error')}}
    </div>
    @endif
    <!-- Section Contact
    ==========================================-->
    <section class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact_info">
                        <div class="section_title aos-init" data-aos="fade-up" data-aos-delay="50">
                            <span>CONTACT WITH US</span>
                            <h3>SPEAK WITH OUR Experts</h3>
                        </div>
                        <div class="contact_item">
                            <i class="fa fa-map-marker-alt"></i>
                            <a href="{{$allSettings->addresslink}}" target="_blank">
                                <span> Address </span>
                                {{$allSettings->address}}
                            </a>
                        </div>
                        <div class="contact_item">
                            <i class="far fa-envelope"></i>
                            <a href="mailto:{{$allSettings->email}} ">
                                <span> Email address </span>
                                {{$allSettings->email}}
                            </a>
                        </div>
                        {{-- <div class="contact_item">
              <i class="far fa-clock"></i>
              <a href="#">
                <span> Work Time </span>
                SAT : THU 9:00 AM To 5:00 PM
              </a>
            </div> --}}
                        <div class="contact_item">
                            <i class="fa fa-phone"></i>
                            <div class="call_numbers">
                                <span> Call Us </span>
                                <a href="tel:{{$allSettings->phone}}" class="d-block w-100">
                                    {{$allSettings->phone}}
                                </a>
                                {{-- <a href="tel:+201273322248" class="d-block w-100">
                  +20 127 332 2248
                </a>
                <a href="tel:+2026446620" class="d-block w-100">
                  +20 26446620
                </a> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <form class="contact_form" action="{{url('contactUSform')}}" method="post"
                        enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label> Full name</label>
                                    <input type="text" class="form-control" name="name">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input type="email" class="form-control" name="email">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label> Phone number</label>
                                    <input type="number" class="form-control" name="phone">
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label> Services </label>
                                    <select class="form-control select2" id="serviceSelect" name="service_id">
                                        <option selected="" disabled="">* Choose One </option>
                                        @foreach($services as $service)
                                        <option value="{{$service->id}}">{{$service->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label> Message </label>
                                    <textarea class="form-control" name="message"></textarea>
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="link">
                                    <span> <i class="fa fa-envelope"></i> Send message </span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
        <div class="container-fluid">
            <div class="row">
                {!!$allSettings->mapembed!!}
            </div>
        </div>
    </section>
    <!--End Section -->
</div>
@endsection
