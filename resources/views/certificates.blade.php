@extends('layouts.master')

@section('content')
<div class="page_content">
    <div class="page_head" style="background-image:url('{{url('upload/page/'.$page->image)}}'); background-position-x: center;
        background-position-y: center;
        background-size: cover;
        background-repeat-x: no-repeat;
        background-repeat-y: no-repeat;
        background-attachment: fixed;">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3>{{$page->title}}</h3>
                    {!!$page->description!!}
                </div>
                <!--End Col-->
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
    </div>
    <!--End Page Head-->
    <!-- Section Certificates
    ==========================================-->
    <section>
      <div class="container">
        <div class="row">
            @foreach ($data as $item)

            <div class="col-lg-3 col-md-6 col-sm-6 aos-init" data-aos="fade-up" data-aos-delay="20">
                <a href="{{asset('upload/certificate/'.$item->image)}}" data-fancybox="gallery" class="certify">
                <img src="{{asset('upload/certificate/'.$item->image)}}">
                </a>
            </div>
            @endforeach

          <!--End col-->
        </div>
        <!--End Row-->
      </div>
      <!--End Container-->
    </section>
    <!--End Section -->
  </div>
@endsection
