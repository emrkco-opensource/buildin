@extends('layouts.master')

@section('content')
<div class="page_content">
    <div class="page_head" style="background-image:url('{{url('upload/page/'.$page->image)}}'); background-position-x: center;
        background-position-y: center;
        background-size: cover;
        background-repeat-x: no-repeat;
        background-repeat-y: no-repeat;
        background-attachment: fixed;">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3>{{$page->title}}</h3>
                    {!!$page->description!!}
                </div>
                <!--End Col-->
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
    </div>
    <!--End Page Head-->
    <!-- Section Projects
    ==========================================-->
    <section class="section_color">
      <div class="container">
        <div class="row">
        @foreach ($projects as $project)
          <div class="col-lg-4 col-md-6 col-sm-6 aos-init" data-aos="fade-up" data-aos-delay="30">
            <a href="{{url('singleProject/'.$project->id)}}" class="project_item">
              <div class="cover">
                <img src="{{$project->main_image[0]['image']}}">
                <div class="desc">
                  <span>
                    <i class="fa fa-map-marker-alt float-left p-1"></i>
                    {{-- Cairo, Egypt --}}
                    {!! strip_tags( $project->mapmark ) !!}
                </span>
                {!!strip_tags( $project->short_description ) !!}
                </div>
              </div>
              <!--End Cover-->
              <div class="cont">
                <h3>{{$project->title}}</h3>
                <p>
                  <i class="fa fa-link"> </i>
                  {{$project->mainService->title}}
                </p>
              </div>
              <!--end Cont-->
            </a>
            <!--End Project Item-->
          </div>
        @endforeach
          <!--End col-->
        </div>
        <!--End Row-->
      </div>
      <!--End Container-->
    </section>
    <!--End Section -->
  </div>
@endsection
