@extends('layouts.master')

@section('content')
<div class="page_content">
    <div class="page_head" style="background-image:url('{{url('upload/page/'.$page->image)}}'); background-position-x: center;
        background-position-y: center;
        background-size: cover;
        background-repeat-x: no-repeat;
        background-repeat-y: no-repeat;
        background-attachment: fixed;">
        <div class="container">
            <div class="row text-center">
                <div class="col-12">
                    <h3>{{$page->title}}</h3>
                    {!!$page->description!!}
                </div>
                <!--End Col-->
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
    </div>
    <!--End Page Head-->
    <section>
        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <div class="about_content">
                <h3>WOULD YOU LIKE TO JOIN US?</h3>
                <div class="job_features">
                  <div data-aos="fade-up" data-aos-delay="30" class="aos-init aos-animate">
                    <div class="feat_item">
                      <h3><i class="far fa-star"></i>{{$allSettings->career_title1}}</h3>
                        {!! strip_tags( $allSettings->career_description1 ) !!}
                    </div>
                  </div>
                  <div data-aos="fade-up" data-aos-delay="60" class="aos-init aos-animate">
                    <div class="feat_item">
                      <h3><i class="far fa-star"></i>{{$allSettings->career_title2}}</h3>
                        {!! strip_tags( $allSettings->career_description2 ) !!}
                    </div>
                  </div>
                  <div data-aos="fade-up" data-aos-delay="90" class="aos-init aos-animate">
                    <div class="feat_item">
                      <h3><i class="far fa-star"></i>{{$allSettings->career_title3}}</h3>
                        {!! strip_tags( $allSettings->career_description3 ) !!}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 aos-init aos-animate" data-aos="fade-up" data-aos-delay="120">
              <img src="{{asset('upload/setting/'.$allSettings->career_image)}}" class="w-100">
            </div>
          </div>
          <!--End Row-->
        </div>
        <!--End Container-->
      </section>

      <section class="section_color">
        <div class="container">
          <div class="row">
            <div class="col-12 aos-init aos-animate" data-aos="fade-up" data-aos-delay="30">
              <div class="section_title">
                <h3>Current Opportunities</h3>
              </div>
            </div>
            @foreach ( $careers as $key => $value )
            <div class="col-lg-6 aos-init" data-aos="fade-up" data-aos-delay="60">
              <div class="job_item">
                <h3>{{$value->title}}</h3>
               {!! strip_tags( str_limit($value->description , 130)) !!}
               <br>
                <a href="{{url('oneCareer'.'/'.$value->id)}}"> Read More </a>
              </div>
              <!--End Job Item-->
            </div>
            <!--End col-->
            @endforeach
          </div>
          <!--End Row-->
        </div>
        <!--End Container-->
      </section>
  </div>
@endsection
