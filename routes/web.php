<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index')->name('home');
Route::get('/ourProjects', 'ProjectController@show')->name('ourProjects');
Route::get('/ourCareers', 'CareerController@allcareers')->name('ourCareers');
Route::post('/careersform', 'CareerController@career_request');
Route::get('/oneCareer/{id}', 'CareerController@show')->name('oneCareer');
Route::get('/singleService/{id}', 'ServiceController@oneService')->name('singleService');
Route::get('/service_main_category/{id}', 'ServicecategoryController@oneCategory')->name('service_main_category');
Route::get('/singleProject/{id}', 'ProjectController@oneProject')->name('singleProject');
Route::get('/clients', 'PartnerController@show')->name('clients');
Route::get('/ourCertificates', 'CertificateController@show')->name('ourCertificates');
Route::get('/about', 'SettingController@aboutUs')->name('about');
Route::post('/contactUSform', 'ContactController@contactWithUs');
Route::post('/newsletter', 'ContactController@subscribe');
Route::get('/contactUSpage', 'ContactController@contactPage')->name('contactUSpage');
Route::get('/responsibilities', 'ResponsibilityController@show')->name('responsibilities');


Auth::routes();
// Route::get('/admin', 'SettingController@index')->middleware('auth');
Route::group(['middleware' => ['auth']], function () {
    Route::resource('/admin', 'SettingController');
    Route::resource('/partners', 'PartnerController');
    Route::resource('/certificates', 'CertificateController');
    Route::resource('/whychoose', 'WhychooseController');
    Route::resource('/ourvalue', 'OurvalueController');
    Route::resource('/slider', 'SlideController');
    Route::resource('/responslider', 'ResponsibilitysliderController');
    Route::resource('/respons', 'ResponsibilityController');
    Route::resource('/services_category', 'ServicecategoryController');
    Route::resource('/contact', 'ContactController');
    Route::get('/newsletter_sub', 'ContactController@subscribe_admin');
    Route::any('/newsletter_sub_delete/{id}', 'ContactController@subscribe_delete');

    //--------- careers routes ------//
    Route::resource('/careers', 'CareerController');
    Route::get('/career_requests/{id}', 'CareerController@one_career_requests');
    Route::any('/request_delete/{id}', 'CareerController@career_request_delete');

    //--------- services routes ------//
    Route::resource('/services', 'ServiceController');
    Route::get('/services_images/{id}', 'ServiceController@service_images');
    Route::get('/services_images/{id}/delete', 'ServiceController@service_images_delete');
    Route::get('/services_images/{id}/default', 'ServiceController@service_images_default');

    //--------- projects routes ------//
    Route::resource('/projects', 'ProjectController');
    Route::get('/projects_images/{id}', 'ProjectController@project_images');
    Route::get('/projects_images/{id}/delete', 'ProjectController@project_images_delete');
    Route::get('/projects_images/{id}/default', 'ProjectController@project_images_default');

    Route::resource('/pages', 'PageController');

});


Route::get('/slides-images', function () {

    foreach (\App\Slide::cursor() as $meal) {

        $image_path = public_path() . '/' . \App\Slide::$imagePath . $meal->logo;

        if ($meal->logo != null && file_exists($image_path)) {

            $meal->logo = pathInfo($meal->logo, PATHINFO_FILENAME) . '.webp';

            $image = \Intervention\Image\Facades\Image::make($image_path)
            ->resize(1920, null, function ($constraints) {
                $constraints->aspectRatio();
            });
            $image->save(public_path() . '/' . \App\Slide::$imagePath . $meal->logo);

            $meal->save();
        }
    }

    return '<h1>Done</h1>';
});


Route::get('/service-category-images', function () {

    foreach (\App\Servicecategory::cursor() as $meal) {

        $image_path = public_path() . '/' . \App\Servicecategory::$imagePath . $meal->logo;

        if ($meal->logo != null && file_exists($image_path)) {

            $meal->logo = pathInfo($meal->logo, PATHINFO_FILENAME) . '.webp';

            $image = \Intervention\Image\Facades\Image::make($image_path)
            ->resize(1920, null, function ($constraints) {
                $constraints->aspectRatio();
            });
            $image->save(public_path() . '/' . \App\Servicecategory::$imagePath . $meal->logo);

            $meal->save();
        }
    }

    return '<h1>Done</h1>';
});

Route::get('/res-images', function () {

    foreach (\App\Responsibilityslider::cursor() as $meal) {

        $image_path = public_path() . '/' . \App\Responsibilityslider::$imagePath . $meal->image;

        if ($meal->image != null && file_exists($image_path)) {

            $meal->image = pathInfo($meal->image, PATHINFO_FILENAME) . '.webp';

            $image = \Intervention\Image\Facades\Image::make($image_path)
            ->resize(1920, null, function ($constraints) {
                $constraints->aspectRatio();
            });
            $image->save(public_path() . '/' . \App\Responsibilityslider::$imagePath . $meal->image);

            $meal->save();
        }
    }

    return '<h1>Done</h1>';
});
