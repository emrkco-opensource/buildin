/* Header
================================*/
$(document).ready(function () {
  "use strict";
  $(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 40) {
      $("header").addClass("move shadow");
    } else {
      $("header").removeClass("move shadow");
    }
  });
  $(".menu-btn").click(function () {
    $(".menu-btn i").toggleClass("fa-times");
  });
  $(".search_btn").click(function () {
    $(this).toggleClass("fa-times");
    $("header form").slideToggle();
  });
});

/* OWL Slider
=======================*/
$(document).ready(function () {
  "use strict";

  $(".services_slider").owlCarousel({
    loop: true,
    nav: true,
    dots: false,
    smartSpeed: 3000,
    autoplayHoverPause: true,
    margin: 30,
    autoplay: true,
    responsive: {
      0: { items: 1, nav: false, dots: true },
      576: { items: 2, nav: false, dots: true, loop: false },
      577: { items: 2, margin: 15, loop: false },
      768: { items: 3, loop: false },
      992: { items: 4, loop: false },
    },
  });

  $(".projects_slider").owlCarousel({
    loop: true,
    nav: true,
    dots: false,
    smartSpeed: 3000,
    autoplayHoverPause: true,
    margin: 30,
    // autoplay: true,
    responsive: {
      0: { items: 1, nav: false, dots: true },
      576: { items: 2, nav: false, dots: true },
      640: { items: 2, margin: 5 },
      768: { items: 2 },
      992: { items: 3 },
    },
  });
  $(".clients_slider").owlCarousel({
    loop: true,
    nav: false,
    dots: false,
    smartSpeed: 3000,
    autoplayHoverPause: true,
    margin: 30,
    autoplay: true,
    responsive: {
      0: { items: 1, nav: false, dots: true },
      420: { items: 3, nav: false, dots: true },
      576: { items: 3, nav: false, dots: true },
      577: { items: 4, margin: 15 },
      768: { items: 5 },
      992: { items: 6 },
    },
  });
});

/* Top
=============================*/
$(document).ready(function () {
  "use strict";
  var scrollbutton = $(".up_btn");
  $(window).scroll(function () {
    $(this).scrollTop() >= 1000
      ? scrollbutton.addClass("show")
      : scrollbutton.removeClass("show");
  });
  scrollbutton.click(function () {
    $("html , body").animate(
      {
        scrollTop: 0,
      },
      600
    );
  });
});

/* Mouse Cursor
=================================*/
$(document).mousemove(function (e) {
  if ($(window).width() > 991) {
    $(".cursor").eq(0).css({
      left: e.clientX,
      top: e.clientY,
    });
    $("a, button").mouseenter(function () {
      $(".cursor").css({
        "background-color": "rgba(160, 106, 0, 0.3)",
        width: "35px",
        height: "35px",
      });
    });
    $(".project_item").mouseenter(function () {
      $(".cursor").css({
        "background-color": "rgba(33, 33, 33, 0.3)",
        width: "35px",
        height: "35px",
      });
    });
    $("a, button ").mouseleave(function () {
      $(".cursor").css({
        "background-color": "rgba(160, 106, 0, 0.5)",
        width: "15px",
        height: "15px",
      });
    });
  }
});

/* Loading 
================================*/
$(window).on("load", function () {
  "use strict";
  AOS.init({
    offset: 100,
    duration: 500,
    easing: "ease-in-out",
  });
  $(".load_cont").fadeOut(function () {
    $(this).parent().fadeOut();
    $("body").css({
      "overflow-y": "visible",
    });
  });
});
