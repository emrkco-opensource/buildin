-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 01, 2022 at 07:46 AM
-- Server version: 5.7.37-cll-lve
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `buildin_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(2, 'Tender Estimation Engineer', '<p><strong>Job Description :&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li>Coordinate between the project&rsquo;s entities and disciplines.</li>\r\n	<li>Liaise with the client to identify and define project requirements, scope and objectives.</li>\r\n	<li>Make certain that the clients&rsquo; needs are met as the project evolves.</li>\r\n	<li>Prepare progress invoices and maintain files for due diligence and financials.</li>\r\n	<li>Keep a record for all project documents / submittals in an organized structure on server for easy follow-up &amp; tracking.</li>\r\n	<li>Monitor and track project&rsquo;s progress and handle any issues that arise.</li>\r\n	<li>Prepare tenders&rsquo; technical &amp; financial proposals.</li>\r\n	<li>Maintaining communications with current clients and establishing communications with new potential Clients.</li>\r\n	<li>Follow up contracts negotiations up to contract signature as well as participation in any contract re-negotiations during the lifetime of the project.</li>\r\n</ul>\r\n\r\n<p><strong>Job Requirements</strong></p>\r\n\r\n<ul>\r\n	<li>Bachelor in Civil or Arch. engineering is a Must</li>\r\n	<li>+3&nbsp;years of experience are required.</li>\r\n	<li>Experience within engineering consulting firms</li>\r\n	<li>Previous Experience on the same Position</li>\r\n	<li>Very good communication and presentation skills.</li>\r\n	<li>Very good English language.</li>\r\n	<li>The candidate should be motivated, creative and hard-worker.</li>\r\n	<li>Ability to work solely or as part of a team.</li>\r\n	<li>Ability to use Microsoft office Tools&nbsp;</li>\r\n	<li>Excellent Quantity surveying Experience&nbsp;</li>\r\n	<li>Good Knowledge with Electrical and MEP Basics&nbsp;</li>\r\n</ul>\r\n\r\n<p>location: Heliopolis<br />\r\nif you are interested kindly send your updated cv via email:<br />\r\n<a href=\"mailto:Nancy_ehab@buildineg.com\">Nancy_ehab@buildineg.com</a></p>', '2022-03-01 20:46:10', '2022-04-08 21:28:07'),
(4, 'Site Engineer', '<p><span style=\"font-size:16px\"><strong>Job Description</strong></span></p>\r\n\r\n<ul>\r\n	<li>Acting as the main site responsible person for the execution of the project</li>\r\n	<li>Ensuring that all materials used and work performed are as per specifications</li>\r\n	<li>Managing, monitoring and interpreting the site progress</li>\r\n	<li>Liaising with any consultants, subcontractors, supervisors and the general workforce involved in the project</li>\r\n	<li>Communicating with clients and their representatives (architects, engineers and surveyors), including attending regular meetings to keep them informed of progress</li>\r\n	<li>Day-to-day management of the site, including supervising and monitoring the site labor force</li>\r\n	<li>Resolving any unexpected technical difficulties and other problems that may arise</li>\r\n	<li>Providing technical advice and solving problems on-site</li>\r\n	<li>Preparing site reports and filling in other paperwork</li>\r\n	<li>Liaising with the technical office about the ordering and negotiating the price of materials</li>\r\n	<li>Liaising with procurement about the ordering and negotiating the price of materials and subcontractors</li>\r\n</ul>\r\n\r\n<p>Ensuring that health and safety and sustainability policies and legislation are adhered to the standards&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p><span style=\"font-size:16px\"><strong>Job Requirements</strong></span></p>\r\n\r\n<ul>\r\n	<li>Bcs. Degree in Civil Engineering class 2016-2018</li>\r\n	<li>High communication &amp; interpersonal skills</li>\r\n	<li>Must possess strong site management skills</li>\r\n	<li>Experience in using (Autocad, Sap2000,Autocad structural drawing)</li>\r\n	<li>Experience in using Photoshop is preferred</li>\r\n	<li>Basic English language</li>\r\n	<li>Excellent knowledge of Microsoft office</li>\r\n</ul>', '2022-04-13 16:19:57', '2022-04-13 16:19:57'),
(5, 'Planning & Cost Control Engineer', '<h2>Job Description</h2>\r\n\r\n<ul>\r\n	<li>Responsible to control and monitor projects including verifying and checking of invoices and claims from suppliers, vendors, and subcontractors to ensure that all project expenditures are captured and properly recorded.</li>\r\n	<li>Provide planning and cost-controlling support for all projects.</li>\r\n	<li>Perform and manage project activity scheduling and monitoring</li>\r\n	<li>Perform technical and commercial reviews with tenders.</li>\r\n	<li>Ensure effective project implementation and utilize productive reports.</li>\r\n	<li>Prepare weekly / monthly movement reports for activities on a daily basis and register all downtime, consumptions, and vessel-related issues relating to the project promptly to the management.</li>\r\n	<li>Resolve project issues.</li>\r\n	<li>Perform project costing-related reporting to the team and management&rsquo;s review on a monthly basis to ensure that expenditures are kept within the project budget.</li>\r\n</ul>\r\n\r\n<h2>Job Requirements</h2>\r\n\r\n<ul>\r\n	<li>Degree in Engineering.</li>\r\n	<li>Min 2 years in planning and cost control.</li>\r\n	<li>Technically strong in project cost planning, scheduling, and reporting.</li>\r\n	<li>Demonstrate strong interpersonal, communication, and presentation skills</li>\r\n	<li>Strong analytical and problem-solving skills and meticulous</li>\r\n	<li>Independent worker who is able to work well with minimal supervision.</li>\r\n	<li>Fast worker with the ability to work well under stress.</li>\r\n	<li>Experience with Primavera is a must</li>\r\n	<li>Good Management skills</li>\r\n</ul>', '2022-04-13 16:21:15', '2022-04-13 16:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `career_requests`
--

CREATE TABLE `career_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `career_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `career_requests`
--

INSERT INTO `career_requests` (`id`, `career_id`, `name`, `email`, `phone`, `file`, `created_at`, `updated_at`) VALUES
(2, 2, 'Moatazz Zeidan', 'mo3tazz.zeidan@yahoo.com', '01094221192', 'Image540401649717001.pdf', '2022-04-12 05:43:21', '2022-04-12 05:43:21'),
(3, 2, 'Hadier Murad', 'hadiermurad63@gmail.com', '01024990882', 'Image221941649737488.pdf', '2022-04-12 11:24:48', '2022-04-12 11:24:48'),
(4, 2, 'Ahmed Shaban', 'engahmedsh3ban@gmail.com', '01010084849', 'Image703331649758322.pdf', '2022-04-12 17:12:02', '2022-04-12 17:12:02'),
(5, 2, 'Ahmed khaled mohamed', 'ahmedkhaled8915@gmail.com', '01116211997', 'Image363131649782646.pdf', '2022-04-12 23:57:26', '2022-04-12 23:57:26'),
(6, 2, 'Amjad wael yahia fayed', 'fayedamjad@gmail.com', '01023280548', 'Image849791649789759.pdf', '2022-04-13 01:55:59', '2022-04-13 01:55:59'),
(7, 2, 'Marwan Mostafa Abd El-Twwab', 'mmarwan1997@gmail.com', '01006414422', 'Image843321649792169.pdf', '2022-04-13 02:36:09', '2022-04-13 02:36:09'),
(8, 2, 'Mohamad Ibrahim', 'Mhmd.ibrahim@protonmail.com', '201010870152', 'Image738861649800428.pdf', '2022-04-13 04:53:48', '2022-04-13 04:53:48'),
(9, 2, 'Ahmed Waleed Salah Eldeen Shaheen', 'ahmedwaleedsalaheldien@outlook.com', '01011977682', 'Image686321649801786.pdf', '2022-04-13 05:16:26', '2022-04-13 05:16:26'),
(10, 2, 'Mohamed noaman abosamra', 'mhmd_abousamra@yahoo.com', '01025056242', 'Image831121649804488.pdf', '2022-04-13 06:01:28', '2022-04-13 06:01:28'),
(11, 2, 'Mohammed Irshad', 'babunichu@gmail.com', '00971529685567', 'Image450231649824439.pdf', '2022-04-13 11:33:59', '2022-04-13 11:33:59'),
(12, 2, 'Ahmed Sayed Mahmoud Elnahas', 'Ahmed.elnahas.2013@gmail.com', '01000577937', 'Image745061649839943.pdf', '2022-04-13 15:52:23', '2022-04-13 15:52:23'),
(13, 4, 'Eslam hussien saad', 'eslamhussien092@gmail.com', '01115482234', 'Image897481649844966.pdf', '2022-04-13 17:16:06', '2022-04-13 17:16:06'),
(14, 4, 'Elshrebiny Hassan', 'elsherbinyhasan43@gmail.com', '01013123466', 'Image313751649845471.pdf', '2022-04-13 17:24:31', '2022-04-13 17:24:31'),
(15, 4, 'Mohamed Ahmed Amen', 'mido.aboamen12@gmail.com', '01102484976', 'Image436541649850545.pdf', '2022-04-13 18:49:05', '2022-04-13 18:49:05'),
(16, 4, 'Farag Eltaher', 'farageltaher202023@gmail.com', '01033047503', 'Image649251649853234.pdf', '2022-04-13 19:33:54', '2022-04-13 19:33:54'),
(17, 4, 'Mostafa Abd-Elmonaem Abd-Elbaky Ayoub', 'mostafaayoub842@gmail.com', '01092581912', 'Image772071649859958.pdf', '2022-04-13 21:25:58', '2022-04-13 21:25:58'),
(18, 4, 'Amir Alaa Sakr', 'amirsakr4444@gmail.com', '01145426786', 'Image468821649864934.pdf', '2022-04-13 22:48:54', '2022-04-13 22:48:54'),
(19, 4, 'Abdelrahman Ahmed', 'abdo.abohola92@yahoo.com', '01066553038', 'Image717241649865646.pdf', '2022-04-13 23:00:46', '2022-04-13 23:00:46'),
(20, 5, 'Khalid Essam', 'khalidessam71@gmail.com', '01212165091', 'Image612091649869626.pdf', '2022-04-14 00:07:06', '2022-04-14 00:07:06'),
(21, 4, 'Mahmoud Saber Rohiem', 'rohiem133@gmail.com', '01098875000', 'Image477541649869719.pdf', '2022-04-14 00:08:39', '2022-04-14 00:08:39'),
(22, 4, 'Khalid Essam mohamed', 'khalidessam71@gmail.com', '01212165091', 'Image180191649869723.pdf', '2022-04-14 00:08:43', '2022-04-14 00:08:43'),
(23, 2, 'Khalid essam', 'khalidessam71@gmail.com', '01212165091', 'Image797201649869836.pdf', '2022-04-14 00:10:36', '2022-04-14 00:10:36'),
(24, 2, 'Khalid essam', 'khalidessam71@gmail.com', '01212165091', 'Image838831649869851.pdf', '2022-04-14 00:10:51', '2022-04-14 00:10:51'),
(25, 4, 'maged mamdoh abdelaziz', 'magedmamdoh96@gmail.com', '01550266294', 'Image556091649884040.pdf', '2022-04-14 04:07:20', '2022-04-14 04:07:20'),
(26, 5, 'Mohamed Mahmoud ali', 'mohamed_mahmoud_civil100@yahoo.com', '01145628966', 'Image961081649887752.pdf', '2022-04-14 05:09:12', '2022-04-14 05:09:12'),
(27, 4, 'Mohamed emad', 'mohamedemadismaiel@gmail.com', '01158969776', 'Image422861649892218.pdf', '2022-04-14 06:23:38', '2022-04-14 06:23:38'),
(28, 2, 'Amany Mansour', 'amanyreda9595@gmail.com', '01094694370', 'Image884891649901101.pdf', '2022-04-14 08:51:41', '2022-04-14 08:51:41'),
(29, 4, 'Ahmed Salah', 'midodezil886@gmail.com', '2001007800419', 'Image731371649911208.pdf', '2022-04-14 11:40:08', '2022-04-14 11:40:08'),
(30, 4, 'Ahmed samer', 'Medosamer0000@gmail.com', '01007545345', 'Image881891649912039.pdf', '2022-04-14 11:53:59', '2022-04-14 11:53:59'),
(31, 5, 'mohammed omar', 'cemeo1994@outlook.com', '01281876458', 'Image515981649922880.pdf', '2022-04-14 14:54:40', '2022-04-14 14:54:40'),
(32, 4, 'Mohamed Abdelbaset Mansour Hussain', 'mbbaset455@gmail.com', '01553010954', 'Image680131649936696.pdf', '2022-04-14 18:44:56', '2022-04-14 18:44:56'),
(33, 5, 'Ahmed Hassan Megahed', 'ahmed.h.megahed96@gmail.com', '1001242628', 'Image431021649991834.pdf', '2022-04-15 10:03:54', '2022-04-15 10:03:54'),
(34, 4, 'Omar ahmed abd elazem shaqweer', 'Omar.Shaqweer23@gmail.com', '01125007992', 'Image242321650025760.pdf', '2022-04-15 19:29:20', '2022-04-15 19:29:20'),
(35, 4, 'Ibrahim radwan', 'ibrahimradwan647@gmail.com', '01090108293', 'Image736891650030192.pdf', '2022-04-15 20:43:12', '2022-04-15 20:43:12'),
(36, 4, 'Ibrahim radwan', 'ibrahimradwan647@gmail.com', '01090108293', 'Image479401650030209.pdf', '2022-04-15 20:43:29', '2022-04-15 20:43:29'),
(37, 4, 'Ibrahim radwan', 'ibrahimradwan647@gmail.com', '01090108293', 'Image962111650030699.pdf', '2022-04-15 20:51:39', '2022-04-15 20:51:39'),
(38, 4, 'Mohamed Sobhy Mosaad Abd-Elrahman', 'mohamed.sobhy6991@gmail.com', '01200565164', 'Image538491650064222.pdf', '2022-04-16 06:10:22', '2022-04-16 06:10:22'),
(39, 5, 'Sandra Adel Ghobrial', 'sandra_adel2015@yahoo.com', '01273952671', 'Image532821650084066.pdf', '2022-04-16 11:41:06', '2022-04-16 11:41:06'),
(40, 5, 'Amjad Wael Fayed', 'fayedamjad@gmail.com', '01023280548', 'Image539591650103845.pdf', '2022-04-16 17:10:45', '2022-04-16 17:10:45'),
(41, 4, 'Amjad Wael Fayed', 'fayedamjad@gmail.com', '01023280548', 'Image850631650104155.pdf', '2022-04-16 17:15:55', '2022-04-16 17:15:55'),
(42, 4, 'Maisara Mohsen Attia', 'maysaramohsen6@gmail.com', '01063663973', 'Image333591650139588.', '2022-04-17 03:06:28', '2022-04-17 03:06:28'),
(43, 4, 'Maisara Mohsen Attia', 'maysaramohsen6@gmail.com', '01063663973', 'Image636461650139659.pdf', '2022-04-17 03:07:39', '2022-04-17 03:07:39'),
(44, 4, 'Mohamed Shalby Sheashaa', 'mohamed.shalby615@gmail.com', '01020580703', 'Image153541650148810.pdf', '2022-04-17 05:40:10', '2022-04-17 05:40:10'),
(45, 2, 'Amany Mansour', 'amanyreda9595@gmail.com', '01094694370', 'Image273621650153246.pdf', '2022-04-17 06:54:06', '2022-04-17 06:54:06'),
(46, 5, 'Mohamed Mohsen Alquot', 'mohammedmohsenalquot@gmail.com', '01026161324', 'Image256291650162887.pdf', '2022-04-17 09:34:47', '2022-04-17 09:34:47'),
(47, 4, 'Hamdy Mostafa Mohamed Mostafa', 'hamdy.mostafa978@gmail.com', '01020865949', 'Image659131650165206.pdf', '2022-04-17 10:13:26', '2022-04-17 10:13:26'),
(48, 4, 'Ahmed Osama Elansary', 'ahmedelansary16516@gmail.com', '01555444471', 'Image710241650175857.pdf', '2022-04-17 13:10:57', '2022-04-17 13:10:57'),
(49, 4, 'Mokhtar elsayed mokhtar', 'mokhtarelsayed753@gmail.com', '01062827409', 'Image681771650181570.cv', '2022-04-17 14:46:10', '2022-04-17 14:46:10'),
(50, 4, 'Eslam Mohammed Ali', 'eslamelmshad@gmail.com', '01091345139', 'Image993321650301317.pdf', '2022-04-19 00:01:57', '2022-04-19 00:01:57'),
(51, 4, 'Abdallah Ahmed', 'aghanem602@gmail.com', '01064698970', 'Image389581650320197.pdf', '2022-04-19 05:16:37', '2022-04-19 05:16:37'),
(52, 2, 'Menna\'tallah Ramadan', 'menna.allah.ramadan18@gmail.com', '01148230037', 'Image427061650363219.pdf', '2022-04-19 17:13:39', '2022-04-19 17:13:39'),
(53, 4, 'Mohamed Saad', 'medosaad957@gmail.com', '01063201912', 'Image488351650371411.pdf', '2022-04-19 19:30:11', '2022-04-19 19:30:11'),
(54, 2, 'Ahmed Nabil Fouad', 'ahmednblfouad@gmail.com', '01115009300', 'Image476071650371525.pdf', '2022-04-19 19:32:05', '2022-04-19 19:32:05'),
(55, 4, 'Ahmed Taha Al-Hussaini Hassan', 'atahaalhussaini2020@yahoo.com', '01016916278', 'Image138971650374780.pdf', '2022-04-19 20:26:20', '2022-04-19 20:26:20'),
(56, 4, 'Ahmed esmat', 'ahmed.kaoud7@gmail.com', '01277049830', 'Image211961650379239.pdf', '2022-04-19 21:40:39', '2022-04-19 21:40:39'),
(57, 4, 'Ahmed esmat', 'ahmed.kaoud7@gmail.com', '01277049830', 'Image331071650379288.pdf', '2022-04-19 21:41:28', '2022-04-19 21:41:28'),
(58, 4, 'Hussam Taher', 'hussamtaherr@gmail.com', '01157848435', 'Image711621650435819.pdf', '2022-04-20 13:23:39', '2022-04-20 13:23:39'),
(59, 4, 'Mostafa Hussein Abdel-sattar', 'Mostafa.hussein197@gmail.com', '01097656456', 'Image599711650468839.pdf', '2022-04-20 22:33:59', '2022-04-20 22:33:59'),
(60, 4, 'Mostafa Hussein Abdel-sattar', 'Mostafa.hussein197@gmail.com', '01097656456', 'Image502121650468858.pdf', '2022-04-20 22:34:18', '2022-04-20 22:34:18'),
(61, 4, 'Ahmed Hassan Abdulrahman', 'a_r33333@yahoo.com', '01010057627', 'Image977501650469534.doc', '2022-04-20 22:45:34', '2022-04-20 22:45:34'),
(62, 4, 'Majed Hassan Abdulrahman', 'a_r33333@yahoo.com', '01095951517', 'Image231791650469622.pdf', '2022-04-20 22:47:02', '2022-04-20 22:47:02'),
(63, 4, 'Ibrahim Mohammed Ibrahim', 'ibrahem.m.g2017@gmail.com', '01112785934', 'Image174351650539786.pdf', '2022-04-21 18:16:26', '2022-04-21 18:16:26'),
(64, 5, 'Shaher Mohamed', 'Shaherm251@gmail.com', '01154843906', 'Image922991650568236.pdf', '2022-04-22 02:10:36', '2022-04-22 02:10:36'),
(65, 4, 'Mohamed Makram Sleim', 'mohamedmakram95@gmail.com', '01145222970', 'Image220371650619560.pdf', '2022-04-22 16:26:00', '2022-04-22 16:26:00'),
(66, 4, 'Hesham ibrahim', 'heshamibrahim23@gmail.com', '01094034322', 'Image820411650644828.pdf', '2022-04-22 23:27:08', '2022-04-22 23:27:08'),
(67, 5, 'Mahmoud Radwan', 'mahmoudradwan846@gmail.com', '01121634917', 'Image556861650646382.pdf', '2022-04-22 23:53:02', '2022-04-22 23:53:02'),
(68, 4, 'Ali Sayed Ahmed Mohamed', 'alisayedahmohamed@outlook.com', '01008654547', 'Image895511650655296.pdf', '2022-04-23 02:21:36', '2022-04-23 02:21:36'),
(69, 4, 'Amjad wael yahia fayed', 'fayedamjad@gmail.com', '01023280548', 'Image933161650668350.pdf', '2022-04-23 05:59:10', '2022-04-23 05:59:10'),
(70, 4, 'Osama Adel Ibrahim', 'osamaelmaleky61@gmail.com', '01010984509', 'Image368931650698467.pdf', '2022-04-23 14:21:07', '2022-04-23 14:21:07'),
(71, 4, 'Shaher mohamed', 'Shaherm251@gmail.com', '01154843906', 'Image725281650757070.pdf', '2022-04-24 06:37:50', '2022-04-24 06:37:50'),
(72, 4, 'SREEJITH P S', 'sreejithps71@gmai.com', '917012021634', 'Image493791650910935.pdf', '2022-04-26 01:22:15', '2022-04-26 01:22:15'),
(73, 4, 'Ahmed Othman Ibrahim Moustafa', 'ahmedothman_80@hotmail.com', '201009709755', 'Image256711650914352.pdf', '2022-04-26 02:19:12', '2022-04-26 02:19:12'),
(74, 5, 'Eslam Sobhy Hamed', 'eslamsobhy83@gmail.com', '201111624390', 'Image929891650934952.pdf', '2022-04-26 08:02:32', '2022-04-26 08:02:32'),
(75, 5, 'Omar Alaa', 'Omaralaafouad@gmail.com', '01202418456', 'Image161411650949393.pdf', '2022-04-26 12:03:13', '2022-04-26 12:03:13'),
(76, 4, 'Mohammed salah alsaadany', 'mohammedalsaadany9@gmail.com', '01068864696', 'Image798081650950580.pdf', '2022-04-26 12:23:00', '2022-04-26 12:23:00'),
(77, 5, 'Farah Al-Dweik', 'farahdweik-94@hotmail.com', '00962799141992', 'Image599811650952749.pdf', '2022-04-26 12:59:09', '2022-04-26 12:59:09'),
(78, 4, 'محمود احمد عطيه', 'mahmoudhannoura917@gmail.com', '01095525214', 'Image136511650962054.pdf', '2022-04-26 15:34:14', '2022-04-26 15:34:14'),
(79, 4, 'Moustafa Abou-Elnaga', 'Moustafaabouelnaga91@gmail.com', '01110260567', 'Image633361650963093.pdf', '2022-04-26 15:51:33', '2022-04-26 15:51:33'),
(80, 5, 'Moaaz Hassan', 'moaaz.hassanabdrabou@Gmail.com', '01004761626', 'Image282751650964001.pdf', '2022-04-26 16:06:41', '2022-04-26 16:06:41'),
(81, 5, 'Mostafa Abdelsalam', 'mostafamahmoud502@gmail.com', '01011095692', 'Image849081650968827.pdf', '2022-04-26 17:27:07', '2022-04-26 17:27:07'),
(82, 4, 'Ahmed Mosaad Abdul ghaffar', 'mosaadahmed990@gmail.com', '01015674697', 'Image877521651313505.pdf', '2022-04-30 17:11:45', '2022-04-30 17:11:45'),
(83, 4, 'Shadi Shokry Michael', 'shadishokry666@gmail.com', '01283379278', 'Image437491651384869.pdf', '2022-05-01 13:01:09', '2022-05-01 13:01:09'),
(84, 4, 'Hatem Elhamalawy', 'eng.hatem108@gmail.com', '01155848711', 'Image945341651403799.pdf', '2022-05-01 18:16:39', '2022-05-01 18:16:39');

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `certificates`
--

INSERT INTO `certificates` (`id`, `image`, `created_at`, `updated_at`) VALUES
(6, 'Image385151648398247.jpg', '2022-03-27 23:24:07', '2022-03-27 23:24:07'),
(7, 'Image477461648398522.jpg', '2022-03-27 23:28:42', '2022-03-27 23:28:42'),
(8, 'Image191101648398737.jpg', '2022-03-27 23:32:17', '2022-03-27 23:32:17'),
(9, 'Image624541648398842.jpg', '2022-03-27 23:34:02', '2022-03-27 23:34:02'),
(10, 'Image787061648399371.jpg', '2022-03-27 23:42:51', '2022-03-27 23:42:51'),
(11, 'Image974841648399873.jpg', '2022-03-27 23:51:13', '2022-03-27 23:51:13'),
(12, 'Image328091648399906.jpg', '2022-03-27 23:51:46', '2022-03-27 23:51:46');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `phone`, `email`, `service_id`, `message`, `created_at`, `updated_at`) VALUES
(88, 'HenryAnobe', '89037709692', 'susankjansen@gmail.com', 23, 'Have no financial skills? Let Robot make money for you. https://take-profitnow.life/?u=bdlkd0x&o=x7t8nng', '2022-04-24 11:21:03', '2022-04-24 11:21:03'),
(89, 'stroycaf', '85735888715', 'sappdoresym1966@plusgmail.ru', 13, '<a href=http://krym-stroy.ru>Строительная компания в Крыму</a>  - подробнее на сайте <a href=http://krym-stroy.ru>krym-stroy.ru</a>', '2022-04-24 13:50:49', '2022-04-24 13:50:49'),
(90, 'HenryAnobe', '89037721832', 'janenejacques@yahoo.com', 26, 'This robot will help you to make hundreds of dollars each day. https://take-profitnow.life/?u=bdlkd0x&o=x7t8nng', '2022-04-24 16:35:49', '2022-04-24 16:35:49'),
(91, 'HenryAnobe', '89038582112', 'carelesscarresser@yahoo.com', 26, 'Making money can be extremely easy if you use this Robot. https://take-profitnow.life/?u=bdlkd0x&o=x7t8nng', '2022-04-24 21:46:10', '2022-04-24 21:46:10'),
(92, 'HenryAnobe', '89038308838', 'olyv59_27@hotmail.com', 12, 'No need to work anymore. Just launch the robot. https://take-profitnow.life/?u=bdlkd0x&o=x7t8nng', '2022-04-25 02:34:30', '2022-04-25 02:34:30'),
(93, 'FriceTrign', '85535478669', 'vonak92@mail.ru', 25, '<a href=https://aktumauto.by/>Р°СЂРµРЅРґР° Р°РІС‚Рѕ РїРѕРґ РІС‹РєСѓРї РјРёРЅСЃРє</a> \r\n<a href=http://www.aktumauto.by>http://www.aktumauto.by</a> \r\n<a href=http://lifeyogaworld.com/?URL=aktumauto.by>http://vidoz.com.ua/go/?url=http://aktumauto.by</a>', '2022-04-25 05:26:28', '2022-04-25 05:26:28'),
(94, 'trudyav1', '84911843318', 'gregory@masaaki4310.takayuki94.officemail.in.net', 6, 'Hot galleries, thousands new daily.\r\nhttp://sex.jsutandy.com/?bailey \r\n big hairy cunts porn sex dragon moon porn pinis porn men videos hottie teen porn liza dwyer porn', '2022-04-25 14:59:44', '2022-04-25 14:59:44'),
(95, 'MiceTrign', '82666116521', 'milan.grey.79@mail.ru', 21, '<a href=https://nk-info.ru/>+РєР°Рє РїРѕРЅСЏС‚СЊ РјР°Р№РЅРёС‚СЊ</a> \r\n<a href=https://www.nk-info.ru>https://nk-info.ru/</a> \r\n<a href=https://google.co.in/url?q=http://nk-info.ru>https://www.google.md/url?q=http://nk-info.ru</a>', '2022-04-25 17:20:32', '2022-04-25 17:20:32'),
(96, 'HenryAnobe', '89032857607', 'raina.nugent@gmail.com', 22, 'Only one click can grow up your money really fast. https://take-profitnow.life/?u=bdlkd0x&o=x7t8nng', '2022-04-25 18:31:19', '2022-04-25 18:31:19'),
(97, 'HenryAnobe', '89035729596', 'tellikoroma@gmail.com', 10, 'Find out about the fastest way for a financial independence. https://breweriana.it/gotodate/promo', '2022-04-25 23:28:34', '2022-04-25 23:28:34'),
(98, 'MichaelVox', '82743396181', 'drtgergadfbg@store-site.site', 23, 'электро эпиляция \r\n<a href=https://vk.com/epilyciy_shulepova>https://vk.com/epilyciy_shulepova</a>', '2022-04-25 23:32:38', '2022-04-25 23:32:38'),
(99, 'HenryAnobe', '89038176831', 'softkisses4u@hotmail.com', 19, 'No need to stay awake all night long to earn money. Launch the robot. https://breweriana.it/gotodate/promo', '2022-04-26 04:18:50', '2022-04-26 04:18:50'),
(100, 'HenryAnobe', '89039253327', 'rochabullet1@aol.com', 22, 'Looking for additional money? Try out the best financial instrument. https://breweriana.it/gotodate/promo', '2022-04-26 09:08:58', '2022-04-26 09:08:58'),
(101, 'gordonwe16', '83892381122', 'marilyn@takumi4810.naoki61.meta1.in.net', 10, 'Sexy teen photo galleries\r\nhttp://matador.amandahot.com/?destini \r\n porn 4 u kim scooby doo lesbian cartoon porn free daily porn pictures babalu porn tube gloryhole porn movie', '2022-04-26 10:41:57', '2022-04-26 10:41:57'),
(102, 'HenryAnobe', '89030614554', 'pmqhozxd@anonemailbox.com', 2, 'Robot never sleeps. It makes money for you 24/7. https://breweriana.it/gotodate/promo', '2022-04-26 14:06:51', '2022-04-26 14:06:51'),
(103, 'HenryAnobe', '89030118578', 'thosewholikeme4real@yahoo.com', 23, 'There is no need to look for a job anymore. Work online. https://breweriana.it/gotodate/promo', '2022-04-26 18:55:48', '2022-04-26 18:55:48'),
(104, 'HenryAnobe', '89033949812', 'dd1984dd@googlemail.com', 11, 'Check out the automatic Bot, which works for you 24/7. https://breweriana.it/gotodate/promo', '2022-04-26 23:58:55', '2022-04-26 23:58:55'),
(105, 'Leonel', '0721 90 30 36', 'leonel.borden@yahoo.com', 22, 'I was wondering if you wanted to submit your new site to our free business directory? https://bit.ly/addyoursitehere', '2022-04-27 04:16:41', '2022-04-27 04:16:41'),
(106, 'HenryAnobe', '89036693881', 'nitakumar@hotmail.com', 21, 'Need some more money? Robot will earn them really fast. https://breweriana.it/gotodate/promo', '2022-04-27 05:12:01', '2022-04-27 05:12:01'),
(107, 'BitcoinRooto', '87153441866', 'x.r.umersmitrade@gmail.com', 8, 'Hello. Your chance to earn a lot! \r\n \r\nhttps://www.google.com.cy/url?sa=t&rct=j&q=&esrc=s&frm=1&source=web&cd=7&ved=0CFQQFjAG&url=https://apostas-brasil.xyz/tags/casas-de-apostas-esportivas/&ei=zZvsUqfsO8av7Aabo4CYDA&usg=AFQjCNFQ9mSGKpQJLYCqnfy27rhLVKn1Og', '2022-04-27 08:57:45', '2022-04-27 08:57:45'),
(108, 'HenryAnobe', '89035164143', 'alkakjeaday@gmail.com', 14, 'The best online job for retirees. Make your old ages rich. https://breweriana.it/gotodate/promo', '2022-04-27 10:31:05', '2022-04-27 10:31:05'),
(109, 'AmandaAmake', '81735956853', 'kxsikqqgn@gordpizza.ru', 15, '<a href=https://youloan24.com/>pay day loans company</a> \r\n<a href=\"https://youloan24.com/\">immediate cash advance</a>', '2022-04-27 12:27:45', '2022-04-27 12:27:45'),
(110, 'HenryAnobe', '89030525578', 'hmsshultz@yahoo.com', 22, 'Find out about the easiest way of money earning. https://breweriana.it/gotodate/promo', '2022-04-27 16:06:44', '2022-04-27 16:06:44'),
(111, 'helgavm1', '86178257967', 'benita@itsuki910.hiroyuki74.officemail.fun', 6, 'Sexy photo galleries, daily updated pics\r\nhttp://bizerporn.auburn.lexixxx.com/?jaclyn \r\n real interracial porn videos kids free porn videos 19 porn sites in 1 moses porn games bum poking porn', '2022-04-27 17:10:31', '2022-04-27 17:10:31'),
(112, 'HenryAnobe', '89031670946', 'marymorton1989@yahoo.com', 21, 'Make yourself rich in future using this financial robot. https://2f-2f.de/gotodate/promo', '2022-04-27 21:02:23', '2022-04-27 21:02:23'),
(113, 'HenryAnobe', '89035752115', 'cap5258@aol.com', 25, 'One click of the robot can bring you thousands of bucks. https://2f-2f.de/gotodate/promo', '2022-04-28 02:06:43', '2022-04-28 02:06:43'),
(114, 'WalterGat', '81513361887', 'vita.vinokurovas@gmail.com', 21, 'https://telegra.ph/Can-Carbon-Dating-Be-Used-On-Dinosaur-Bones-11-09', '2022-04-28 04:00:09', '2022-04-28 04:00:09'),
(115, 'HenryAnobe', '89031680629', 'sarascadoo@hotmail.com', 28, 'Trust your dollar to the Robot and see how it grows to $100. https://2f-2f.de/gotodate/promo', '2022-04-28 07:05:34', '2022-04-28 07:05:34'),
(116, 'HenryAnobe', '89038454670', 'williamr21@hotmail.com', 12, 'Make thousands every week working online here. https://2f-2f.de/gotodate/promo', '2022-04-28 12:33:37', '2022-04-28 12:33:37'),
(117, 'PieldemflueT', '88783988991', 'Smoodayafteniafot@gmail.com', 22, 'noclegi w augustowie nad jeziorem', '2022-04-28 16:15:38', '2022-04-28 16:15:38'),
(118, 'TeresaBally', '83238882982', 'omrfndolu@wowzilla.ru', 23, '<a href=https://youloan24.com/>fast payday loan company</a> \r\n<a href=\"https://youloan24.com/\">usa cash loans</a>', '2022-04-28 16:57:45', '2022-04-28 16:57:45'),
(119, 'HenryAnobe', '89033098013', 'pipemun@hotmail.com', 26, 'Provide your family with the money in age. Launch the Robot! https://2f-2f.de/gotodate/promo', '2022-04-28 17:26:28', '2022-04-28 17:26:28'),
(120, 'HenryAnobe', '89032876959', 'supcoop20@yahoo.com', 2, 'Start making thousands of dollars every week just using this robot. https://2f-2f.de/gotodate/promo', '2022-04-28 22:17:17', '2022-04-28 22:17:17'),
(121, 'HenryAnobe', '89039133588', 'dash11_99@yahoo.com', 7, 'Financial Robot is #1 investment tool ever. Launch it! https://2f-2f.de/gotodate/promo', '2022-04-29 03:12:02', '2022-04-29 03:12:02'),
(122, 'HenryAnobe', '89035570722', 'kristian.lindahl@gmail.com', 13, 'See how Robot makes $1000 from $1 of investment. https://2f-2f.de/gotodate/promo', '2022-04-29 08:22:15', '2022-04-29 08:22:15'),
(123, 'HenryAnobe', '89033445782', 'oonadae@juno.com', 11, 'Earning $1000 a day is easy if you use this financial Robot. https://2f-2f.de/gotodate/promo', '2022-04-29 14:12:20', '2022-04-29 14:12:20'),
(124, 'HenryAnobe', '89036331917', 'want_milk@hotmail.com', 7, 'Earning $1000 a day is easy if you use this financial Robot. https://2f-2f.de/gotodate/promo', '2022-04-29 19:19:40', '2022-04-29 19:19:40'),
(125, 'BriceTrign', '89517517442', 'v_aliev111@mail.ru', 24, '<a href=></a> \r\n<a href=></a> \r\n<a href=></a>', '2022-04-29 21:54:06', '2022-04-29 21:54:06'),
(126, 'HenryAnobe', '89031892733', 'a3d3g3y3@gmail.com', 12, 'No worries if you are fired. Work online. https://2f-2f.de/gotodate/promo', '2022-04-29 23:07:24', '2022-04-29 23:07:24'),
(127, 'HenryAnobe', '89038670331', 'ganeshkhairnar@gmail.com', 28, 'Watch your money grow while you invest with the Robot. https://2f-2f.de/gotodate/promo', '2022-04-30 03:55:17', '2022-04-30 03:55:17'),
(128, 'SdvillVer', '82929952766', 'revers@o5o5.ru', 6, '<a href=https://chimmed.ru/>knauer.net </a> \r\nTegs: kraeber  https://chimmed.ru/  \r\n \r\n<u>Bio Techne </u> \r\n<i>Bio-Rad </i> \r\n<b>Bio-Techne </b>', '2022-04-30 05:04:49', '2022-04-30 05:04:49'),
(129, 'HenryAnobe', '89036292053', 'debdow156@comcast.net', 29, 'Trust your dollar to the Robot and see how it grows to $100. https://2f-2f.de/gotodate/promo', '2022-04-30 08:47:25', '2022-04-30 08:47:25'),
(130, 'Kennethwrome', '83676357212', 'v.anja.kuzminep5nn@gmail.com', 26, 'homemade herbal remedies  <a href=  > http://cappelliammortizzatori.it/tavor.html </a>  unproductive cough remedy  <a href= http://www.eepinen.fi/tramse.html > http://www.eepinen.fi/tramse.html </a>  hoarseness remedies', '2022-04-30 09:39:13', '2022-04-30 09:39:13'),
(131, 'Gow', '89966269696', 'naomisholtz7@gmail.com', 2, 'At last someone wrote something very important about such hot topic and it is very relevant nowadays. \r\n \r\n<a href=https://www.kiva.org/lender/teratoh9823>נערות ליווי</a>', '2022-04-30 13:16:02', '2022-04-30 13:16:02'),
(132, 'HenryAnobe', '89036537611', 'boylan5@COX.NET', 15, 'The financial Robot is your # 1 expert of making money. https://2f-2f.de/gotodate/promo', '2022-04-30 14:00:11', '2022-04-30 14:00:11'),
(133, 'Jamespause', '84899563633', 'cyberlinkxchnage@gmail.com', 14, 'de,<a href=https://www.jackpotbetonline.com/>Sports betting</a> Guide that will help you on the right track. Get all the info you need. +18. T&C apply', '2022-04-30 15:42:37', '2022-04-30 15:42:37'),
(134, 'SdvillVer', '83725922342', 'revers@o5o5.ru', 14, '<a href=https://chimmed.ru/>пан биотеч </a> \r\nTegs: пепротеч  https://chimmed.ru/  \r\n \r\n<u>hiss-dx.de </u> \r\n<i>hk phystandard bio-tech co., ltd. </i> \r\n<b>hopaxfc </b>', '2022-04-30 15:47:12', '2022-04-30 15:47:12'),
(135, 'HenryAnobe', '89033605807', 'angelswatchinoverme08@yahoo.com', 2, 'Find out about the easiest way of money earning. https://2f-2f.de/gotodate/promo', '2022-04-30 19:12:48', '2022-04-30 19:12:48'),
(136, 'HenryAnobe', '89035904726', 'sanyoreta-94@hotmail.com', 26, 'Feel free to buy everything you want with the additional income. https://2f-2f.de/gotodate/promo', '2022-05-01 00:33:03', '2022-05-01 00:33:03'),
(137, 'ElwinsTrign', '81618358242', 'andrey.petr55@gmail.com', 14, '<a href=http://vebcamonline.ru/>реальные камеры онлайн</a> \r\n<a href=http://www.vebcamonline.ru>http://www.vebcamonline.ru/</a> \r\n<a href=http://google.li/url?q=http://vebcamonline.ru/>http://google.com.sa/url?q=http://vebcamonline.ru/</a>', '2022-05-01 06:38:14', '2022-05-01 06:38:14'),
(138, 'HenryAnobe', '89031445969', 'gobahaude@yahoo.com', 12, 'Money, money! Make more money with financial robot! https://2f-2f.de/gotodate/promo', '2022-05-01 08:34:09', '2022-05-01 08:34:09'),
(139, 'Waytig', '81828589246', 'utimorka@yandex.com', 27, '<a href=https://proxyspace.seo-hunter.com>аренда мобильных прокси info</a>', '2022-05-01 13:40:25', '2022-05-01 13:40:25'),
(140, 'HenryAnobe', '89039734379', 'sander.megens@gmail.com', 22, 'Looking for additional money? Try out the best financial instrument. https://2f-2f.de/gotodate/promo', '2022-05-01 13:46:00', '2022-05-01 13:46:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2022_02_01_195010_create_settings_table', 2),
(4, '2022_02_01_210044_create_partners_table', 3),
(5, '2022_02_01_214201_create_whychooses_table', 4),
(8, '2022_02_01_221453_create_slides_table', 5),
(9, '2022_02_01_230511_create_servicecategories_table', 6),
(10, '2022_02_04_162520_create_services_table', 7),
(11, '2022_02_04_163522_create_service_images_table', 8),
(12, '2022_02_04_191227_create_projects_table', 9),
(13, '2022_02_04_191400_create_project_images_table', 9),
(14, '2022_02_04_203742_add_mapmark_to_projects', 10),
(15, '2022_02_05_144003_add_about_to_settings', 11),
(17, '2022_02_05_145355_add_description_to_whychooses', 12),
(18, '2022_02_09_172241_create_certificates_table', 13),
(20, '2022_02_10_170123_create_contacts_table', 14),
(21, '2022_02_11_142153_add_mapemped_to_settings', 15),
(22, '2022_02_11_174527_create_ourvalues_table', 16),
(24, '2022_02_24_222222_create_responsibilities_table', 18),
(25, '2022_02_24_222813_create_responsibilitysliders_table', 18),
(27, '2022_03_01_210252_add_careers_to_settings', 19),
(30, '2022_03_01_215532_create_careers_table', 20),
(31, '2022_03_01_214908_create_career_requests_table', 21),
(32, '2022_03_02_002003_add_marks_to_projects', 22),
(33, '2022_03_07_123837_add_since_to_settings', 23),
(34, '2022_03_07_124244_add_sort_to_projects', 23),
(35, '2022_03_07_125035_create_newsletters_table', 24),
(36, '2022_03_11_184233_create_pages_table', 25),
(37, '2022_03_22_153807_add_video_to_projects', 26),
(38, '2022_03_22_154423_add_video_to_services', 26);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'kareemkima@gmail.com', '2022-03-07 11:22:03', '2022-03-07 11:22:03'),
(4, 'mamdoohdodo@gmailcom', '2022-03-24 19:57:55', '2022-03-24 19:57:55'),
(5, 'emadelsawah222@gmail.com', '2022-04-13 01:04:03', '2022-04-13 01:04:03'),
(6, 'kemomostafa17@yahoo.com', '2022-04-13 04:39:46', '2022-04-13 04:39:46'),
(7, 'fadybadr2015@gmail.com', '2022-04-13 04:52:30', '2022-04-13 04:52:30'),
(8, 'hossamramadan480@gmail.com', '2022-04-13 08:05:05', '2022-04-13 08:05:05'),
(9, 'eng.mohamed.mo1@gmail.com', '2022-04-13 11:21:49', '2022-04-13 11:21:49'),
(10, 'mohamed_mahmoud_civil100@yahoo.com', '2022-04-14 05:02:43', '2022-04-14 05:02:43'),
(11, 'mohamedemadismaiel@gmail.com', '2022-04-14 06:22:33', '2022-04-14 06:22:33'),
(12, 'cemeo1994@outlook.com', '2022-04-14 14:55:56', '2022-04-14 14:55:56'),
(13, 'mohammedmomen163@gmail.com', '2022-04-16 22:27:57', '2022-04-16 22:27:57'),
(14, 'islamabdalrhman127@gmail.com', '2022-04-18 22:40:03', '2022-04-18 22:40:03'),
(15, 'ahmed.kaoud7@gmail.com', '2022-04-19 21:40:51', '2022-04-19 21:40:51'),
(16, 'heshamibrahim23@gmail.com', '2022-04-22 23:28:24', '2022-04-22 23:28:24'),
(17, 'mahmoudradwan846@gmail.com', '2022-04-22 23:53:25', '2022-04-22 23:53:25'),
(18, 'shadishokry666@gmail.com', '2022-05-01 13:01:27', '2022-05-01 13:01:27');

-- --------------------------------------------------------

--
-- Table structure for table `ourvalues`
--

CREATE TABLE `ourvalues` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ourvalues`
--

INSERT INTO `ourvalues` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'CLIENT FOCUSED', '<p>Dedicated to clients&rsquo; needs and adding value at every touchpoint to achieve a great experience an end result. We innovate on behalf of the client in every aspect of our work.</p>', '2022-02-11 16:04:50', '2022-02-16 10:08:28'),
(2, 'TEAMWORK', '<p>We achieve the best results for our clients by collaborating and working together. Working together substantiates the team and empowers individual strengths.</p>', '2022-02-11 16:06:44', '2022-02-11 16:06:44'),
(3, 'RESPECT', '<p>We behave genuinely towards everyone, both inside and outside of the business and recognise the importance of inclusion. All backgrounds, beliefs and cultures are valued, celebrated and mutually respected.</p>', '2022-02-11 16:07:22', '2022-02-11 16:07:22'),
(4, 'HONESTY', '<p>We are truthful and open in our interactions with each other and our clients. Honesty is something that might not always be easy to deal with or say, but it has been and always will be the best way to handle situations.</p>', '2022-02-11 16:08:02', '2022-02-11 16:08:02'),
(5, 'CONTINUOUS IMPROVEMENT', '<p>We challenge norms by thinking in new and creative ways to continuously improve how we work and our performance. We embrace change and are committed to growth and improvement through constant learning and feedback. Yesterday was good, today is great and tomorrow will be better.</p>', '2022-02-11 16:08:33', '2022-02-11 16:08:33');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Responsibilities', '<p>&quot; BUILDIN CONTRACTING is an Egyptian based general and specialized construction contractor &quot;</p>', 'Image801681649082111.jpg', '2022-03-11 17:25:37', '2022-04-04 21:21:51'),
(2, 'Our Projects', '<p>&quot; BUILDIN CONTRACTING is an Egyptian based general and specialized construction contractor &quot;</p>', 'Image973481647026956.jpg', '2022-03-11 17:29:16', '2022-03-11 17:44:33'),
(3, 'Certificates', '<p>&quot; BUILDIN CONTRACTING is an Egyptian based general and specialized construction contractor &quot;</p>', 'Image579091647026983.jpg', '2022-03-11 17:29:43', '2022-03-11 17:29:43'),
(4, 'Clients', '<p>&quot; BUILDIN CONTRACTING is an Egyptian based general and specialized construction contractor &quot;</p>', 'Image665801647027016.jpg', '2022-03-11 17:30:16', '2022-03-11 17:30:16'),
(5, 'Contact Us', '<p>&quot; BUILDIN CONTRACTING is an Egyptian based general and specialized construction contractor &quot;</p>', 'Image677421647027050.jpg', '2022-03-11 17:30:50', '2022-03-11 17:30:50'),
(6, 'About Us', '<p>&quot; BUILDIN CONTRACTING is an Egyptian based general and specialized construction contractor &quot;</p>', 'Image912581647537814.jpg', '2022-03-11 17:31:25', '2022-03-17 21:23:34'),
(7, 'Careers', '<p>&quot; BUILDIN CONTRACTING is an Egyptian based general and specialized construction contractor &quot;</p>', 'Image667431647027109.jpg', '2022-03-11 17:31:49', '2022-03-11 17:31:49');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `logo`, `created_at`, `updated_at`) VALUES
(8, 'Image556211648400014.png', '2022-03-27 23:53:34', '2022-03-27 23:53:34'),
(10, 'Image150961648400066.png', '2022-03-27 23:54:26', '2022-03-27 23:54:26'),
(11, 'Image683851648401437.png', '2022-03-27 23:54:56', '2022-03-28 00:17:17'),
(12, 'Image945721648401453.png', '2022-03-27 23:55:47', '2022-03-28 00:17:33'),
(13, 'Image121301648401465.png', '2022-03-27 23:56:01', '2022-03-28 00:17:45'),
(14, 'Image206141648401476.png', '2022-03-27 23:56:12', '2022-03-28 00:17:56'),
(15, 'Image133761648401506.png', '2022-03-27 23:56:28', '2022-03-28 00:18:26'),
(16, 'Image995001648401954.png', '2022-03-27 23:56:41', '2022-03-28 00:25:54'),
(17, 'Image860641648401573.png', '2022-03-27 23:56:52', '2022-03-28 00:19:33'),
(18, 'Image407311648400256.png', '2022-03-27 23:57:36', '2022-03-27 23:57:36'),
(19, 'Image284001648400296.png', '2022-03-27 23:58:16', '2022-03-27 23:58:16'),
(20, 'Image527071648400323.png', '2022-03-27 23:58:43', '2022-03-27 23:58:43'),
(21, 'Image986411648401666.png', '2022-03-27 23:59:37', '2022-03-28 00:21:06'),
(22, 'Image801311648400394.png', '2022-03-27 23:59:54', '2022-03-27 23:59:54'),
(23, 'Image471521648401680.png', '2022-03-28 00:00:11', '2022-03-28 00:21:20'),
(24, 'Image889261648401695.png', '2022-03-28 00:00:25', '2022-03-28 00:21:35'),
(25, 'Image715151648400442.png', '2022-03-28 00:00:42', '2022-03-28 00:00:42'),
(26, 'Image653911648400457.png', '2022-03-28 00:00:57', '2022-03-28 00:00:57'),
(27, 'Image832651648400471.png', '2022-03-28 00:01:11', '2022-03-28 00:01:11'),
(28, 'Image272131648401650.png', '2022-03-28 00:01:26', '2022-03-28 00:20:50'),
(29, 'Image272181648402145.png', '2022-03-28 00:01:39', '2022-03-28 00:29:05'),
(30, 'Image712511648400554.png', '2022-03-28 00:02:34', '2022-03-28 00:02:34'),
(31, 'Image292021648400566.png', '2022-03-28 00:02:46', '2022-03-28 00:02:46'),
(32, 'Image722731648400805.png', '2022-03-28 00:06:45', '2022-03-28 00:06:45'),
(33, 'Image618031648400859.png', '2022-03-28 00:07:39', '2022-03-28 00:07:39'),
(34, 'Image721921648400922.jpg', '2022-03-28 00:08:42', '2022-03-28 00:08:42'),
(35, 'Image166251648400937.png', '2022-03-28 00:08:57', '2022-03-28 00:08:57'),
(36, 'Image279971648400956.png', '2022-03-28 00:09:16', '2022-03-28 00:09:16'),
(37, 'Image633241648400971.png', '2022-03-28 00:09:31', '2022-03-28 00:09:31'),
(38, 'Image339411648400989.png', '2022-03-28 00:09:49', '2022-03-28 00:09:49'),
(39, 'Image677951648401009.png', '2022-03-28 00:10:09', '2022-03-28 00:10:09'),
(40, 'Image444001648401030.png', '2022-03-28 00:10:30', '2022-03-28 00:10:30'),
(41, 'Image156921648401053.png', '2022-03-28 00:10:53', '2022-03-28 00:10:53'),
(42, 'Image838581648401066.png', '2022-03-28 00:11:06', '2022-03-28 00:11:06'),
(43, 'Image547391648401078.png', '2022-03-28 00:11:18', '2022-03-28 00:11:18'),
(44, 'Image748171648401098.png', '2022-03-28 00:11:38', '2022-03-28 00:11:38'),
(45, 'Image340161648402345.png', '2022-03-28 00:11:50', '2022-03-28 00:32:25'),
(46, 'Image480301648401124.png', '2022-03-28 00:12:04', '2022-03-28 00:12:04'),
(47, 'Image879661648401140.png', '2022-03-28 00:12:20', '2022-03-28 00:12:20'),
(48, 'Image915771648402297.png', '2022-03-28 00:12:38', '2022-03-28 00:31:37'),
(49, 'Image762671648401779.png', '2022-03-28 00:12:54', '2022-03-28 00:22:59'),
(50, 'Image411971648402260.png', '2022-03-28 00:13:06', '2022-03-28 00:31:00'),
(51, 'Image450091648401200.png', '2022-03-28 00:13:20', '2022-03-28 00:13:20'),
(53, 'Image937811648401819.png', '2022-03-28 00:14:01', '2022-03-28 00:23:39'),
(54, 'Image996731648401382.png', '2022-03-28 00:16:22', '2022-03-28 00:16:22');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mapmark` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stakeholders` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sector` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `completion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `service_id`, `title`, `short_description`, `full_description`, `link`, `created_at`, `updated_at`, `mapmark`, `duration`, `stakeholders`, `sector`, `location`, `completion`, `sort`, `video`) VALUES
(1, 14, 'Sanofi – Cairo Plant', '<p style=\"text-align:justify\"><span style=\"color:null\">Sanofi S.A. is a French multinational pharmaceutical company headquartered in Paris, France, as of 2013 the world&#39;s fifth-largest by prescription sales.</span></p>', '<p><span style=\"color:#d35400\"><span style=\"font-size:20px\">S</span>anofi</span><span style=\"color:#c0392b\"> </span>S.A. is a French multinational pharmaceutical company headquartered in Paris, France, as of 2013 the world&#39;s fifth-largest by prescription sales.</p>\r\n\r\n<p><span style=\"color:#d35400\"><span style=\"font-size:20px\">B</span>uildIn</span> has successfully executed and delivered over 10 projects to Sanofi as a general contractor. Our scope of work included strengthening of elements of the existing structure in several buildings of the plant, using different methods and techniques, according to building nature and structural requirements:</p>\r\n\r\n<ul>\r\n	<li>Carbon Fiber Strengthening of reinforced concrete beams</li>\r\n	<li>Strengthening of reinforced concrete beams using heavy duty steel built-up sections</li>\r\n	<li>Strengthening of 140 columns using steel jacketing method</li>\r\n	<li>Concrete Jacketing of other columns</li>\r\n	<li>Strengthening of reinforced concrete slabs of warehouse, semi-solid area, and other buildings using steel plates and beams</li>\r\n</ul>\r\n\r\n<p><span style=\"color:#d35400\"><span style=\"font-size:20px\">B</span>uildIn&#39;s</span> role also included construction works of a new steel structure for chillers, with sandwich panel roofing. Additionally, we also have successfully made several finishing items and external works for Sanofi, including:</p>\r\n\r\n<ul>\r\n	<li>Supply and installation of raised floor for transformers rooms</li>\r\n	<li>Installation of curtain wall fa&ccedil;ade for IA Building</li>\r\n	<li>Fa&ccedil;ades clean and repair for all buildings, consuming over 7,000 m of paint coverage</li>\r\n	<li>Epoxy and Granite flooring made in syrup production zone</li>\r\n</ul>\r\n\r\n<p>As part of its commitment to the customer after project delivery, BuildIN has maintained a sustainable relationship with Sanofi as a trusted partner and we have recently engaged in new projects together.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3451.5624570860055!2d31.29668091493008!3d30.106714981859724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1458157b3ed8a655%3A0x47f6d985a6772123!2sSanofi%20Egypt!5e0!3m2!1sen!2seg!4v1649941457043!5m2!1sen!2seg', '2022-02-04 18:02:57', '2022-04-14 20:05:01', 'Cairo, Egypt', '2 Years', '<p>Client: Sanofi&nbsp; |&nbsp; Consultant: ECC&nbsp; |&nbsp; Contractor: BuildIn</p>', '<p>Industrial - Pharmaceutical</p>', '<p>Amireya, Cairo &ndash; Egypt</p>', '<p>(month, year)</p>', '2', NULL),
(8, 7, 'Novartis – Cairo Plant', '<p>Novartis is a global healthcare company based in Switzerland. Novartis operates businesses focusing on innovative patented medicines and generics</p>', '<p style=\"text-align:justify\"><span style=\"color:#d35400\"><span style=\"font-size:20px\">N</span>ovartis</span> is a global healthcare company based in Switzerland. Novartis operates businesses focusing on innovative patented medicines and generics, medicines including more than 50 keys marketed products, many of which are market leaders among their therapeutic area, and generics offering more than 1,000 different types of high quality, affordable products across a broad range of therapeutic areas.</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:#d35400\"><span style=\"font-size:20px\">B</span>uildIn</span>&nbsp;secured a general contracting agreement with Novartis for several of its renovation projects including civil, infrastructure, exterior and landscape works. Scope of work comprises of column strengthening and mechanical demolition and managing the full external interface, including the fa&ccedil;ades renovation, installation of the cladding systems, infrastructure re-routing, road network upgrade and landscaping (softscape and hardscape) as detailed below:</p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Column Strengthening of building B20 using different methods of steel and concrete jacketing</li>\r\n	<li style=\"text-align:justify\">Mechanical Demolition of Lab Building</li>\r\n	<li style=\"text-align:justify\">Fa&ccedil;ade Renovation of building B20 &amp; B43, installing curtain wall system, aluminum cladding, brick cladding as well as paint</li>\r\n	<li style=\"text-align:justify\">Road Network upgrade by removing the old pre-cast concrete slabs, re-leveling of the factory main roads, executing a new drainage system below the new road network, and lastly casting fairfaced concrete roads finished by helicopter technique</li>\r\n	<li style=\"text-align:justify\">Landscape design and execution including curbs, interlock sidewalks, green areas softscaping and outdoor lighting elements</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">As part of its commitment to the customer after project delivery, <span style=\"color:#d35400\">BuildIn</span>&nbsp;has maintained a sustainable relationship with Novartis as a trusted partner and we have recently engaged in new projects together.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3451.601263660908!2d31.28952131493013!3d30.105603981860057!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14581563dd93b807%3A0x79a5b1fe3cd38d7d!2sNovartis%20Pharma%20(Al%20Zaytoun)!5e0!3m2!1sen!2seg!4v1649941539991!5m2!1sen!2seg', '2022-03-01 23:16:45', '2022-04-14 20:06:33', 'Cairo, Egypt', '5 Months', '<p>Client: Novartis&nbsp; |&nbsp; Consultant: Howeedy&nbsp; |&nbsp; Contractor: BuildIn</p>', '<p>Industrial - Pharmaceutical</p>', '<p>Cairo &ndash; Egypt</p>', '<p>(month, year)</p>', '3', NULL),
(9, 14, 'October Plaza Compound', '<p>October Plaza SODIC Compound is one of Cairo&rsquo;s unique residential communities that spreads over more than 30 acres in Westown, 6th of October City.</p>', '<p>October Plaza SODIC Compound is one of Cairo&rsquo;s unique residential communities that spreads over more than 30 acres in Westown, 6th of October City.</p>\r\n\r\n<p>It is master planned to be the main urban hub of Cairo&rsquo;s western suburbs. Sodic October Plaza is located away from the noise of Cairo, known for its tranquility and sophistication at the same time. It combines modern living spaces and community garden with a commercial center. Specifically, the services provided at the compound are unparalleled, whether in the same region or in general.</p>\r\n\r\n<p><span style=\"color:#c0392b\">BuildIn&#39;s</span> role included strengthening works in this project. Our team of professionals has successfully delivered the critical scheme of strengthening unsafe reinforced concrete slabs using fast modern construction methods and techniques. By working closely with our consultants and supply chain we have delivered the work to our customer&rsquo;s satisfaction and within the budget set. We look forward to working with SODIC again in the near future.</p>\r\n\r\n<p>&nbsp;</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3455.3209844443236!2d30.919358214927367!3d29.99893838189891!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145857b6a4769db5%3A0xb6dd749329af5f72!2sOctober%20plaza%20sodic!5e0!3m2!1sen!2seg!4v1649941309343!5m2!1sen!2seg', '2022-03-12 03:20:40', '2022-04-14 20:02:55', 'October – Egypt', '3 Months', '<p>Client: SODIC<br />\r\nConsultant: EHAF<br />\r\nContractor: CONSTEC</p>', '<p>Residential</p>', '<p>Waslet Dahshur Rd, 6th of October &ndash; Egypt</p>', '<p>(month, year)</p>', '2', NULL),
(10, 11, 'Arab League Headquarters', '<p style=\"text-align:justify\">League of Arab States LAS is a regional organization of Arab States in the Middle East and North Africa formed in Cairo on March 22, 1945. It aims to be a regional organization of Arab States with a focus of developing the economy, resolving disputes and coordination political aims.</p>', '<p style=\"text-align:justify\">League of Arab States LAS is a regional organization of Arab States in the Middle East and North Africa formed in Cairo on March 22, 1945. It aims to be a regional organization of Arab States with a focus of developing the economy, resolving disputes and coordination political aims. The League&#39;s main goal is to &quot;draw closer the relations between member states and co-ordinate collaboration between them, to safeguard their independence and sovereignty, and to consider in a general way the affairs and interests of the Arab countries&quot;</p>\r\n\r\n<p style=\"text-align:justify\">BuildIN carried out specialized works that included water proofing, cementitious injection, slab strengthening, chemical anchoring and steel construction. The nature of work in this particular project due to high flow rates of water into the basement of the new building continuously leaking from the Nile River has imposed significant challenges to the team on site. However, our team of experts have professionally handled the project, successfully delivered the critical cementitious injection in the basement that have completely stopped the water flow, and then cementitious waterproofing has been applied in order to prevent future water leakage.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIN scope also involved strengthening the slabs of old building using spray concrete method, the construction of new steel structure on top of the new building roof to accommodate the chillers equipment, and chemical anchoring for beams and columns due to structural modifications that took place during the execution of new building.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.7253040835812!2d31.23108921492853!3d30.044737681882232!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840c5f0af4755%3A0xbd1fb1d00661a573!2sArab%20League%20Headquarters!5e0!3m2!1sen!2seg!4v1649941616610!5m2!1sen!2seg', '2022-03-12 03:25:37', '2022-04-14 20:07:49', 'Cairo – Egypt', '4 Months', '<p>Client: League of Arab States|Consultant: Prof. Dr. Mahamed Zanaty|Contractor: CONSTEC</p>', '<p>Commercial</p>', '<p>Tahrir Square, Cairo &ndash; Egypt</p>', '<p>(month, year)</p>', '4', NULL),
(11, 2, 'Cira Pharma – Cairo Plant', '<p style=\"text-align:justify\">Cira Pharma is an Egyptian pharmaceutical company established in 2020 specialized in manufacturing and promoting medical devices, as well as promoting cosmeceuticals &amp; pharmaceutical products and food supplements.</p>', '<p style=\"text-align:justify\">Cira Pharma is an Egyptian pharmaceutical company established in 2020 specialized in manufacturing and promoting medical devices, as well as promoting cosmeceuticals &amp; pharmaceutical products and food supplements.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIN was appointed as the main contractor for this project. Our work takes the building from a ground floor to ground + 3 floors building. Scope of work comprises of concrete construction, strengthening works using different techniques and exterior finishes:</p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Vertical extension of the existing building by the construction of new additional 3 floors on top of the existing building</li>\r\n	<li style=\"text-align:justify\">Column strengthening using two methods, the CFRP wrapping and the concrete jacketing</li>\r\n	<li style=\"text-align:justify\">Openings strengthening using CFRP laminates</li>\r\n	<li style=\"text-align:justify\">Exterior finishes of fa&ccedil;ade including plaster and paint works, installation of curtain wall system and aluminum cladding</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">By working closely with our consultant and supply chain, we have delivered the work to our customer&rsquo;s satisfaction and within the budget set. We look forward to working with Cira Pharma again in the near future.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3451.9190095901936!2d31.251580614929832!3d30.09650578186338!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145841cb9a0fd16b%3A0x7a72125b52211465!2sCIRA%20Pharma!5e0!3m2!1sen!2seg!4v1649941791461!5m2!1sen!2seg', '2022-03-12 03:36:58', '2022-04-14 20:10:30', 'Cairo – Egypt', '6 Months', '<p>Client: Cira Pharma Group |&nbsp;Consultant: Prof. Dr. Ibrahim Mahfouz | Contractor: BuildIn</p>', '<p>Industrial - Pharmaceutical</p>', '<p>Al-Rubaiki, Cairo &ndash; Egypt</p>', '<p>(month, year)</p>', '5', NULL),
(12, 10, 'Cairo American College', '<p style=\"text-align:justify\">Cairo American College is a pre-K&ndash;12 International American School located in Maadi, Cairo, Egypt. It caters mainly to dependents of the local American embassy and other international students.</p>', '<p style=\"text-align:justify\">Cairo American College is a pre-K&ndash;12 International American School located in Maadi, Cairo, Egypt. It caters mainly to dependents of the local American embassy and other international students. The school aims to recreate an American schooling experience within a Middle-Eastern nation.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIN was selected to install fire stop system for the school. Our works included sealing of fire susceptible zones, joints and penetrations. Around 3000 m&#39; was tightly sealed and delivered.</p>\r\n\r\n<p style=\"text-align:justify\">By working closely with our consultant and supply chain, we have delivered the work to our customer&rsquo;s satisfaction and within the budget set. We look forward to working with American Embassy again in the near future.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3456.7179639663736!2d31.272662814926402!3d29.958790081913538!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14583812970bbd17%3A0x7f10782ad907abac!2sCairo%20American%20College!5e0!3m2!1sen!2seg!4v1649941848658!5m2!1sen!2seg', '2022-03-12 03:40:10', '2022-04-14 20:11:30', 'Cairo, Egypt', '4 Months', '<p>Client: American Embassy ||&nbsp;Consultant: Nature || Contractor: GCC</p>', '<p>Educational</p>', '<p>Maadi, Cairo &ndash; Egypt</p>', '<p>(month, year)</p>', '6', NULL),
(13, 28, 'Kellogg\'s (Bisco Misr) – Cairo Plant', '<p style=\"text-align:justify\">Kellogg&#39;s is the world&#39;s leading cereal company; second largest producer of cookies, crackers and savory snacks; and a leading North American frozen foods company.</p>', '<p style=\"text-align:justify\">Kellogg&#39;s is the world&#39;s leading cereal company; second largest producer of cookies, crackers and savory snacks; and a leading North American frozen foods company. Kellogg Company was the first foreign investor to enter the Egyptian market back in 2013.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIN has successfully completed demolition works required for this project. The work consisted of demolition of several elements like cantilevers and parts of the concrete slab of the existing structure in order to re-shape the fa&ccedil;ade of production building. Diamond cutting technique as well as mechanical demolition were used during execution.</p>\r\n\r\n<p style=\"text-align:justify\">By working closely with our consultant and supply chain, we have delivered the work to our customer&rsquo;s satisfaction and within the budget set. We look forward to working with Kellogg&#39;s again in the near future</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3451.633628259981!2d31.294470514930147!3d30.104677381860377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145815634c08616f%3A0xc2f1d006405afd62!2sBisco%20Misr%20-%20Egyptian%20Co.%20For%20Foods!5e0!3m2!1sen!2seg!4v1649942963963!5m2!1sen!2seg', '2022-03-12 03:43:39', '2022-04-14 20:30:01', 'Cairo, Egypt', '1 Months', '<p>Client: Kellogg&#39;s || Consultant: ECC</p>', '<p>Industrial &ndash; F&amp;B</p>', '<p>Amireya, Cairo &ndash; Egypt</p>', '<p>(month, year)</p>', '7', NULL),
(14, 28, 'Al-Jazi JW Marriott Residence', '<p style=\"text-align:justify\">Marriott International and Al-Jazi Egypt join forces to present Cairo with AL JAZI FIRST Marriott Residences Offering Luxurious state of the art branded apartments, and high-end amenities for an absolutely pampered living. Units range on average from 80-280 meters.</p>', '<p style=\"text-align:justify\">Marriott International and Al-Jazi Egypt join forces to present Cairo with AL JAZI FIRST Marriott Residences Offering Luxurious state of the art branded apartments, and high-end amenities for an absolutely pampered living. Units range on average from 80-280 meters.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIN carried out specialized works due to structural modifications in 5 of the buildings due to design requirements. New openings were created, others were closed, and some slabs were extended. These works required mechanical demolition, chemical anchoring, repairing and concrete works.</p>\r\n\r\n<p style=\"text-align:justify\">By working closely with our consultants and supply chain we have delivered the work to our customer&rsquo;s satisfaction and within the budget set. We look forward to working with Al-Jazi again in the near future.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.0395675997756!2d31.476063714928358!3d30.035722681885492!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1458228de31f1a13%3A0xa0937db810373a94!2sAl%20Jazi%20Egypt!5e0!3m2!1sen!2seg!4v1649942177870!5m2!1sen!2seg', '2022-03-12 03:47:37', '2022-04-14 20:16:55', 'New Cairo , Egypt', '4 Months', '<p>Client: Al-Jazi&nbsp; ||&nbsp;&nbsp;Consultant: AACE&nbsp; ||&nbsp; Contractor: Sectors</p>', '<p>Residential</p>', '<p>5th Settlement, New Cairo &ndash; Egypt</p>', '<p>(month, year)</p>', '8', NULL),
(15, 16, 'Egyptian Parliament Building', '<p>Parliament Building in the New Administrative Capital is built upon 126000m, it consists of the main building, the service building and the landscape works includes the fences, internal roads and the green areas.</p>', '<p>Parliament Building in the New Administrative Capital is built upon 126000m, it consists of the main building, the service building and the landscape works includes the fences, internal roads and the green areas.</p>\r\n\r\n<p>The main building is built on 18000m; it comprises a basement, a ground floor, 8 typical floors, a dome with 50m diameter, the main hall accommodates of 1000 member and the administrative offices.</p>\r\n\r\n<p>BuildIN carried out strengthening works including carbon fiber wrapping and CFRP laminates installation. We have successfully applied and delivered seven layers of FRP wrapping for reinforced beams in the parliament main hall. In addition to that, some of the openings were strengthened using CFRP laminates.</p>\r\n\r\n<p>BuildIN is proud to have actively taken part in the construction of New Administrative Capital, towards a brighter Egyptian future.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.8822513134605!2d31.23466331492847!3d30.040235781883812!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840c9154d6c23%3A0x97c8820fd31e3799!2sEgyptian%20Parliament!5e0!3m2!1sen!2seg!4v1649942252475!5m2!1sen!2seg', '2022-03-12 03:50:57', '2022-04-14 20:18:19', 'New Capital City, Egypt', '1 Month', '<p>Consultant: DAR&nbsp; ||&nbsp;&nbsp;Contractor: Arab Contractors</p>', '<p>Commercial</p>', '<p>New Capital City &ndash; Egypt</p>', '<p>(month, year)</p>', '9', NULL),
(16, 27, 'Allegria Country Club House', '<p style=\"text-align:justify\">Allegria Country Club House is a high-end luxurious country club in Allegria, one of Cairo&#39;s most exclusive residential communities on the Cairo Alexandria Desert Road, adjacent to the Allegria signature golf course designed by the legendary golfer, Greg Norman to be a natural extension of the Allegria community.</p>', '<p style=\"text-align:justify\">Allegria Country Club House is a high-end luxurious country club in Allegria, one of Cairo&#39;s most exclusive residential communities on the Cairo Alexandria Desert Road, adjacent to the Allegria signature golf course designed by the legendary golfer, Greg Norman to be a natural extension of the Allegria community. Allegria includes a variety of areas including Golf Club, Spa, Gym, F&amp;B outlets, Ballroom, kids&rsquo; area, swimming pools, indoor pool and sports courts.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIN was responsible for demolition works in this project. We have carried out concrete diamond cutting using floor sawing technique due to fa&ccedil;ade re-design requirements. We have also removed parts of slab to create new openings for stairs. Then, these openings were strengthened by steel sections.</p>\r\n\r\n<p style=\"text-align:justify\">As part of its commitment to the customer after project delivery, BuildIN has maintained a sustainable relationship with SODIC as a trusted partner and we have engaged in other projects together.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3452.8972123670483!2d30.962733714929122!3d30.068480681873545!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1458597f9e4a4465%3A0x13c8399c20c46068!2sAllegria%20Clubhouse!5e0!3m2!1sen!2seg!4v1649941702940!5m2!1sen!2seg', '2022-03-12 03:55:22', '2022-04-14 20:09:11', 'Sheikh Zayed, Egypt', '1 Month', '<p>Client: SODIC&nbsp; ||&nbsp;&nbsp;Consultant: EHAF&nbsp; ||&nbsp; Contractor: RME</p>', '<p>Commercial</p>', '<p>Alex-Cairo Desert Rd, Sheikh Zayed &ndash; Egypt</p>', '<p>(month, year)</p>', '4', NULL),
(17, 25, 'The Strip II Mall', '<p style=\"text-align:justify\">Strip II Mall introduces a modern, streamlined and easily accessible shopping experience destined to become the destination of high-end fashion brands shopping and leisure Strategically located in SODIC west, one of the hottest urban hubs in the country, the strip II Mall caters West Cairo residents and the surrounding elite community.</p>', '<p style=\"text-align:justify\">Strip II Mall introduces a modern, streamlined and easily accessible shopping experience destined to become the destination of high-end fashion brands shopping and leisure Strategically located in SODIC west, one of the hottest urban hubs in the country, the strip II Mall caters West Cairo residents and the surrounding elite community.</p>\r\n\r\n<p style=\"text-align:justify\">Headquartered in Cairo, SODIC brings to the market award-winning large-scale developments, meeting Egypt&#39;s ever-growing needs for high quality housing, commercial and retail spaces.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIN was responsible for demolition works in this project. We have carried out concrete diamond cutting using floor sawing technique due to fa&ccedil;ade re-design requirements. We have also removed parts of slab to create new openings for stairs and others for electro-mechanical requirements. Then, these openings were strengthened by steel sections.</p>\r\n\r\n<p style=\"text-align:justify\">We pride ourselves on our passion for excellence and commitment to fostering long-term relationships with our clients and business partners even after project delivery. BuildIN has maintained a sustainable relationship with SODIC as a trusted partner and we have engaged in other projects together.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.1436161839106!2d30.935318614929027!3d30.061417581876125!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14585938c5724f77%3A0xdeb1b4392c6b4157!2sThe%20Strip%20II%20Sodic%20West!5e0!3m2!1sen!2seg!4v1649942364876!5m2!1sen!2seg', '2022-03-12 04:01:02', '2022-04-14 20:20:34', 'Sheikh Zayed, Egypt', '1 Month', '<p>Client: SODIC&nbsp; ||&nbsp;&nbsp;Consultant: DMA&nbsp; ||&nbsp; Contractor: RME</p>', '<p>Commercial</p>', '<p>Waslet Dahshur Rd, Sheikh Zayed &ndash; Egypt</p>', '<p>(month, year)</p>', '11', NULL),
(18, 2, 'Fly Emirates Airport Lounge', '<p style=\"text-align:justify\">Fly Emirates flies to 157 destinations in 83 countries across six continents, providing air services that enable trade and travel to its home base in Dubai and beyond.</p>', '<p style=\"text-align:justify\">Fly Emirates flies to 157 destinations in 83 countries across six continents, providing air services that enable trade and travel to its home base in Dubai and beyond. A multinational and multicultural employer with over 160 nationalities working together, Emirates is committed to connecting people around the globe, and helping them to discover, enjoy and share new experiences together. It provides its customers with a more rewarding experience through Emirates lounge experience at selected airports around the world.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIN was assigned to this project to take care of demolition, strengthening and concrete works. We have carried out controlled demolition to provide 14 new openings in the concrete slab using mechanical demolition. New dowels have been inserted for the strengthening of these openings. New concrete footings were casted for the chiller rooms as well.</p>\r\n\r\n<p style=\"text-align:justify\">We pride ourselves on our passion for excellence and commitment to fostering long-term relationships with our clients and business partners even after project delivery. BuildIN has maintained a sustainable relationship with CAI as a trusted partner, have engaged in other projects with them and we look forward to working together again in the near future.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3451.3385202474747!2d31.3969305149304!3d30.11312538185744!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1458170a7d889a85%3A0x56798fccc00d520!2sEmirates%20New%20Lounge%20-%20Terminal%202!5e0!3m2!1sen!2seg!4v1649942470799!5m2!1sen!2seg', '2022-03-12 04:04:29', '2022-04-14 20:21:46', 'El-Nozha,Egypt', '1 Month', '<p>Client: Fly Emirates&nbsp; ||&nbsp;&nbsp;Consultant: CAC&nbsp; ||&nbsp;&nbsp;Contractor: Sectors</p>', '<p>Commercial</p>', '<p>Cairo International Airport, El-Nozha &ndash; Egypt</p>', '<p>(month, year)</p>', '12', NULL),
(19, 10, 'Smart Village Administrative Building', '<p style=\"text-align:justify\">Smart Village is a high-technology business district in the city of 6th of October, Egypt, established by Presidential Decree no.355 in 2000, with activities starting in 2001.</p>', '<p style=\"text-align:justify\">Smart Village is a high-technology business district in the city of 6th of October, Egypt, established by Presidential Decree no.355 in 2000, with activities starting in 2001. It is a business district that occupies an area of 450 feddans with office buildings, retail shops, entertainment, factories and green spaces.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIN was assigned to install firestop systems, including joints and penetrations sealing for administrative building B2116. This project constitutes around 600 m&#39; of firestop head of wall joint sealing of project fire zones. Fire zones penetrations were sealed using acrylic and intumescent sealant. Underground penetrations waterproofing was also carried out.</p>\r\n\r\n<p style=\"text-align:justify\">By working closely with our consultants and supply chain we have delivered the work to our customer&rsquo;s satisfaction and within the budget set. We look forward to working with Smart Village again in the near future.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3452.548781923996!2d31.011697614929414!3d30.078465781870012!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14585ba3024b8275%3A0x9fbd976cf0e573b1!2sSmart%20Villages%20Development%20and%20Management%20Company!5e0!3m2!1sen!2seg!4v1649942534197!5m2!1sen!2seg', '2022-03-12 04:07:30', '2022-04-14 20:23:02', '6th of October,Egypt', '1 Month', '<p>Client: Smart Villages Development &amp; Management Company&nbsp; ||&nbsp; Contractor: Safwa Group</p>', '<p>Commercial</p>', '<p>Alex-Cairo Desert Rd, 6th of October &ndash; Egypt</p>', '<p>(month, year)</p>', '13', NULL),
(20, 13, 'Auto Ghabbour Service Centers', '<p style=\"text-align:justify\">GB Auto is a leading automotive company in the Middle East and North Africa and non-bank financial services provider in Egypt. With seven primary lines of business across four major markets in the MENA, GB Auto has a strong operational footprint in key markets and sectors throughout the region, with plans to expand into new geographical areas.</p>', '<p style=\"text-align:justify\">GB Auto is a leading automotive company in the Middle East and North Africa and non-bank financial services provider in Egypt. With seven primary lines of business across four major markets in the MENA, GB Auto has a strong operational footprint in key markets and sectors throughout the region, with plans to expand into new geographical areas. The company has grown from an automotive player to a fully diversified company with a host of non-cyclical businesses that add significant value to the group.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIN has successfully provided epoxy flooring for 2 of Ghabbour&#39;s service centers, one located in Obour and another in Kattameya. We understand that automotive industry generally, from attractive auto showrooms to the busiest repair shops, requires a floor that provides durability for vehicular traffic, resist chemicals such as oil, transmission oil, automotive fluids, etc. Our experienced team has applied epoxy paint floors that are chemical, stain, &amp; impact resistant.</p>\r\n\r\n<p style=\"text-align:justify\">We pride ourselves on our passion for excellence and commitment to fostering long-term relationships with our clients and business partners even after project delivery. BuildIN has maintained a sustainable relationship with Ghabbour as a trusted partner and we have engaged in other projects together.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3455.5483005796636!2d31.381528714927235!3d29.9924087819013!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14583b883e673b4d%3A0x4c8207f5f2952cdf!2sGhabbour%20Hyundai!5e0!3m2!1sen!2seg!4v1649942720941!5m2!1sen!2seg', '2022-03-12 04:11:07', '2022-04-14 20:25:52', 'Cairo, Egypt', '1 week', '<p>Client: Ghabbour</p>', '<p>Industrial &ndash; Automotive</p>', '<p>Cairo &ndash; Egypt</p>', '<p>(month, year)</p>', '14', NULL),
(21, 14, 'One - Ninety', '<p style=\"text-align:justify\">One-Ninety is a well-rounded mixed-use destination where vibrancy amplifies, and energy multiplies.</p>', '<p style=\"text-align:justify\">One-Ninety is a well-rounded mixed-use destination where vibrancy amplifies, and energy multiplies. One-Ninety adopts a unique leveling technique to elevate a holistic outlook on integrated living, rolling out on 344,315 m2. A new degree of extraordinary, inviting all corners of the world in the heart of New Cairo to dictate a trendsetting lifestyle, as well as a sensory feast, complemented by a 32,980 m2 Urban Park where native flora and cosmopolitan cultures thrive. Here, every aspect is expertly planned to enhance safe walkability, thanks to interconnected pedestrian paths that not only amplify discovery, but also maximize views on breathing landscapes. Envisaged as the epicenter of life, One Ninety boasts a multitude of enchanting experiences; from a cutting-edge Business Quarter, luxury living and world-class entertainment. As for hospitality, W Cairo and A Loft Hotels guarantee legendary pampering and memorable stays, perfected by breathing beauty at One-Ninety&rsquo;s Urban Park.</p>\r\n\r\n<p style=\"text-align:justify\"><span style=\"color:#d35400\">BuildIN</span> scope of work included column strengthening works. Six columns were concrete jacketed by using spray concrete technique. Steel strengthening for one column was made to prevent it against punching stresses.</p>\r\n\r\n<p style=\"text-align:justify\">By working closely with our consultants and supply chain we have delivered the work to our customer&rsquo;s satisfaction and within the budget set. We look forward to working with LMD again in the near future.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.7748155761724!2d31.40394891492783!3d30.014621681893047!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14583d25cfccf5f1%3A0x672454fe995dc294!2sOne%20Ninety!5e0!3m2!1sen!2seg!4v1649942768703!5m2!1sen!2seg', '2022-03-12 04:15:59', '2022-04-14 20:26:35', 'New Cairo , Egypt', 'X Months', '<p>Client: LMD&nbsp; ||&nbsp; Consultant: LMS&nbsp; ||&nbsp; Contractor: Sabbour</p>', '<p>Residential</p>', '<p>90 Street, New Cairo &ndash; Egypt</p>', '<p>(month, year)</p>', '15', NULL),
(22, 2, 'Porto Pyramids', '<p style=\"text-align:justify\">Porto Pyramids lies on an area of 100,000 m2 and is conveniently located minutes away from the airport and the new Egyptian National Museum, close to major business and shopping centers.</p>', '<p style=\"text-align:justify\">Porto Pyramids lies on an area of 100,000 m2 and is conveniently located minutes away from the airport and the new Egyptian National Museum, close to major business and shopping centers. Living in a world characterized by changing lifestyles, busier schedules and greater responsibilities, the need for a balance of tranquil natural landscapes and serene family living has become essential to balance the demands from today&rsquo;s professionals. Services integrated into the project include hotel serviced apartments, cafes and restaurants, swimming pools, club house, cinema, hyper market, shopping mall, spa and Gym and sports club.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIN was assigned to do the concrete works required due to structural modifications. Some slabs were extended, other cantilevers were demolished. Also, rebar chemical fitting has been carried out in 6 of the project buildings.</p>\r\n\r\n<p style=\"text-align:justify\">By working closely with our consultants and supply chain we have delivered the work to our customer&rsquo;s satisfaction and within the budget set. We look forward to working with Porto Group again in the near future.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.734366645536!2d31.094796714927874!3d30.015782881892775!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145845f8f5bcfb87%3A0x56141428c72ff660!2sPorto%20pyramids!5e0!3m2!1sen!2seg!4v1649942828377!5m2!1sen!2seg', '2022-03-12 04:19:19', '2022-04-14 20:27:34', 'Giza, Egypt', '3 Months', '<p>Client: Porto Group</p>', '<p>Residential</p>', '<p>Alex-Cairo Desert Rd, Giza &ndash; Egypt</p>', '<p>(month, year)</p>', '16', NULL),
(23, 8, 'Royal Star – Cairo Plant', '<p style=\"text-align:justify\">Royal Star for Plastic is one of the leading companies in the plastic industry in Egypt. It was established in 1973 and continued to grow and develop to reach all parts of Egypt and some Arab countries.</p>', '<p style=\"text-align:justify\">Royal Star for Plastic is one of the leading companies in the plastic industry in Egypt. It was established in 1973 and continued to grow and develop to reach all parts of Egypt and some Arab countries. Royal star has always been committed to produce more than 300 products with high quality, safe and practical features matching excellent food standards.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIN was appointed as the main contractor for this project. Scope of work constitutes concrete construction for the new extension, renovations of existing production zone and interior fit-out of administrative offices.</p>\r\n\r\n<p style=\"text-align:justify\">By working closely with our consultant and supply chain, we have delivered the work to our customer&rsquo;s satisfaction and within the budget set. We look forward to working with Royal Star again in the near future.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3450.047009230954!2d31.732453314931224!3d30.150071981843983!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1457fa09d9a32a33%3A0x2ac4f6b0200ebd21!2sRoyal%20Star%20For%20Plastic%20Industry!5e0!3m2!1sen!2seg!4v1649942882530!5m2!1sen!2seg', '2022-03-12 04:22:25', '2022-04-14 20:28:32', 'Cairo, Egypt', '6 Months', '<p>Client: Royal Star&nbsp; ||&nbsp;&nbsp;Consultant: Prof. Dr. Mohammed Shennawi</p>', '<p>Industrial - Plastics</p>', '<p>3rd Industrial Zone-Badr, Cairo &ndash; Egypt</p>', '<p>(month, year)</p>', '17', NULL),
(24, 9, 'Ball – Cairo Plant', '<p style=\"text-align:justify\">Ball leads in aluminum packaging because we defy the status quo with creative can and bottle designs based on sustainable manufacturing principles. Building on 140 years of packaging innovation, Ball has launched a global collaboration with beverage customers and consumers to build a circular economy</p>', '<p style=\"text-align:justify\">Ball leads in aluminum packaging because we defy the status quo with creative can and bottle designs based on sustainable manufacturing principles. Building on 140 years of packaging innovation, Ball has launched a global collaboration with beverage customers and consumers to build a circular economy. They reuse materials instead of throwing them away after a single use. The work is essential to the future of human prosperity and the basis of brand loyalty among climate-conscious consumers.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIn has been appointed by Ball to provide epoxy flooring for the plant, specifically in the product area and the mezzanine. We understand that floors in factories are constantly exposed to mechanical wear (e.g. friction, impact etc.) and chemical damage from thinned, inorganic and organic acids, alkalis, petroleum products, wastes etc. Therefore, the final surface on such floors must have considerable mechanical and chemical strength as well as being easy to clean. Our experienced team has provided self-leveling epoxy flooring solution that forms an easily cleaned final surface that displays great durability and strength against friction (abrasive resistance) as well as resistance to chemical effects.</p>\r\n\r\n<p style=\"text-align:justify\">By working closely with our consultant and supply chain, we have delivered the work to our customer&rsquo;s satisfaction and within the budget set. We look forward to working with Ball again in the near future.</p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3457.650615470948!2d30.88512321492565!3d29.931959081923416!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1458ff666fc2107d%3A0x96f3bf6d3b6f856a!2sBall%20Beverage%20Packaging%20-%20Egypt!5e0!3m2!1sen!2seg!4v1649502197000!5m2!1sen!2seg', '2022-03-12 04:26:25', '2022-04-13 06:29:46', '6th of October, Egypt', '3 week', '<p>Client: Ball Beverage Packaging</p>', '<p>Industrial &ndash; Packaging</p>', '<p>3rd Industrial Zone, 6th of October &ndash; Egypt</p>', '<p>February,2022</p>', '1', '679696726');

-- --------------------------------------------------------

--
-- Table structure for table `project_images`
--

CREATE TABLE `project_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1' COMMENT '0:main , 1:sub',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_images`
--

INSERT INTO `project_images` (`id`, `project_id`, `image`, `role`, `created_at`, `updated_at`) VALUES
(37, 11, 'master852291647038218.jpg', 1, '2022-03-12 03:36:58', '2022-03-14 13:18:37'),
(106, 1, 'master290881647187969.jpg', 1, '2022-03-13 20:12:49', '2022-03-13 20:26:05'),
(107, 1, 'master984081647187969.jpg', 1, '2022-03-13 20:12:49', '2022-03-13 20:26:05'),
(108, 1, 'master511691647187969.jpg', 1, '2022-03-13 20:12:49', '2022-03-13 20:26:05'),
(109, 1, 'master844661647187969.jpg', 1, '2022-03-13 20:12:49', '2022-03-13 20:26:05'),
(110, 1, 'master125571647187969.jpg', 1, '2022-03-13 20:12:49', '2022-03-13 20:26:05'),
(111, 1, 'master501311647187969.jpg', 1, '2022-03-13 20:12:49', '2022-03-13 20:26:05'),
(112, 1, 'master898661647187969.jpg', 1, '2022-03-13 20:12:49', '2022-03-13 20:26:05'),
(113, 1, 'master194861647187969.jpg', 1, '2022-03-13 20:12:49', '2022-03-13 20:26:05'),
(114, 1, 'master945391647187969.jpg', 0, '2022-03-13 20:12:49', '2022-03-13 20:26:05'),
(115, 1, 'master181281647187969.jpg', 1, '2022-03-13 20:12:49', '2022-03-13 20:26:05'),
(116, 8, 'master729591647188472.jpg', 1, '2022-03-13 20:21:12', '2022-03-13 20:24:15'),
(117, 8, 'master345911647188472.jpg', 1, '2022-03-13 20:21:12', '2022-03-13 20:24:15'),
(118, 8, 'master730201647188472.jpg', 0, '2022-03-13 20:21:12', '2022-03-24 20:48:46'),
(119, 8, 'master989571647188472.jpg', 1, '2022-03-13 20:21:12', '2022-03-13 20:24:15'),
(120, 8, 'master841501647188472.jpg', 1, '2022-03-13 20:21:12', '2022-03-13 20:24:15'),
(122, 8, 'master223421647188472.jpg', 1, '2022-03-13 20:21:12', '2022-03-13 20:24:15'),
(123, 8, 'master740071647188472.jpg', 1, '2022-03-13 20:21:12', '2022-03-13 20:24:15'),
(124, 8, 'master753421647188472.jpg', 1, '2022-03-13 20:21:12', '2022-03-13 20:24:15'),
(126, 8, 'master356911647188472.jpg', 1, '2022-03-13 20:21:12', '2022-03-13 20:24:15'),
(127, 8, 'master449341647188472.jpg', 1, '2022-03-13 20:21:12', '2022-03-13 20:24:15'),
(128, 8, 'master763631647188472.jpg', 1, '2022-03-13 20:21:12', '2022-03-13 20:24:15'),
(129, 8, 'master413861647188472.jpg', 1, '2022-03-13 20:21:12', '2022-03-13 20:24:15'),
(131, 1, 'master233701647189240.jpg', 2, '2022-03-13 20:34:00', '2022-03-13 20:34:00'),
(132, 1, 'master674111647189240.jpg', 2, '2022-03-13 20:34:00', '2022-03-13 20:34:00'),
(133, 1, 'master406801647189240.jpg', 2, '2022-03-13 20:34:00', '2022-03-13 20:34:00'),
(134, 1, 'master291591647189240.jpg', 2, '2022-03-13 20:34:00', '2022-03-13 20:34:00'),
(135, 8, 'master148801647189791.jpg', 2, '2022-03-13 20:43:11', '2022-03-13 20:43:11'),
(136, 8, 'master126881647189791.jpg', 2, '2022-03-13 20:43:11', '2022-03-13 20:43:11'),
(137, 8, 'master700131647189791.jpg', 2, '2022-03-13 20:43:11', '2022-03-13 20:43:11'),
(138, 8, 'master752001647189791.jpg', 2, '2022-03-13 20:43:11', '2022-03-13 20:43:11'),
(139, 11, 'master136531647190540.jpg', 1, '2022-03-13 20:55:40', '2022-03-14 13:18:37'),
(140, 11, 'master855301647190540.jpg', 1, '2022-03-13 20:55:40', '2022-03-14 13:18:37'),
(141, 11, 'master670641647190540.jpg', 1, '2022-03-13 20:55:40', '2022-03-14 13:18:37'),
(142, 11, 'master356911647190540.jpg', 1, '2022-03-13 20:55:40', '2022-03-14 13:18:37'),
(143, 11, 'master582941647190540.jpg', 1, '2022-03-13 20:55:40', '2022-03-14 13:18:37'),
(144, 11, 'master647631647190747.jpg', 0, '2022-03-13 20:59:07', '2022-03-14 13:18:37'),
(155, 24, 'master757181647954959.jpg', 0, '2022-03-22 17:15:59', '2022-03-22 17:22:19'),
(164, 24, 'master446331647965162.jpg', 1, '2022-03-22 20:06:02', '2022-03-22 20:06:02'),
(166, 24, 'master440431647965162.jpg', 1, '2022-03-22 20:06:02', '2022-03-22 20:06:02'),
(172, 9, 'master507711648125292.jpg', 2, '2022-03-24 19:34:52', '2022-03-24 19:34:52'),
(173, 9, 'master476551648125292.jpg', 2, '2022-03-24 19:34:52', '2022-03-24 19:34:52'),
(174, 9, 'master569511648125553.jpg', 1, '2022-03-24 19:39:13', '2022-03-24 19:39:13'),
(175, 9, 'master611591648125553.jpg', 1, '2022-03-24 19:39:13', '2022-03-24 19:39:13'),
(176, 9, 'master914901648125553.jpg', 1, '2022-03-24 19:39:13', '2022-03-24 19:39:13'),
(180, 9, 'master813181648125882.jpg', 0, '2022-03-24 19:44:42', '2022-03-24 19:45:19'),
(181, 9, 'master158751648126139.jpg', 2, '2022-03-24 19:48:59', '2022-03-24 19:48:59'),
(183, 24, 'master655771648127247.jpg', 1, '2022-03-24 20:07:27', '2022-03-24 20:07:27'),
(184, 24, 'master164651648127247.jpg', 1, '2022-03-24 20:07:27', '2022-03-24 20:07:27'),
(185, 24, 'master413431648127346.jpg', 2, '2022-03-24 20:09:06', '2022-03-24 20:09:06'),
(186, 24, 'master881581648127688.jpg', 2, '2022-03-24 20:14:48', '2022-03-24 20:14:48'),
(187, 24, 'master253761648127688.jpg', 2, '2022-03-24 20:14:48', '2022-03-24 20:14:48'),
(188, 24, 'master206621648127688.jpg', 2, '2022-03-24 20:14:48', '2022-03-24 20:14:48'),
(189, 24, 'master678271648128316.jpg', 1, '2022-03-24 20:25:16', '2022-03-24 20:25:16'),
(190, 24, 'master389101648128404.jpg', 1, '2022-03-24 20:26:44', '2022-03-24 20:26:44'),
(192, 8, 'master414581648130067.jpg', 1, '2022-03-24 20:54:27', '2022-03-24 20:54:27'),
(193, 16, 'master576701648131598.jpg', 0, '2022-03-24 21:19:58', '2022-03-24 21:29:39'),
(194, 11, 'master801091648380729.jpg', 2, '2022-03-27 18:32:09', '2022-03-27 18:32:09'),
(195, 11, 'master659121648380729.jpg', 2, '2022-03-27 18:32:09', '2022-03-27 18:32:09'),
(196, 10, 'master168901648386611.jpg', 0, '2022-03-27 20:10:11', '2022-03-27 20:15:16'),
(197, 10, 'master578641648386897.jpg', 1, '2022-03-27 20:14:57', '2022-03-27 20:14:57'),
(198, 10, 'master847811648386897.jpg', 1, '2022-03-27 20:14:57', '2022-03-27 20:14:57'),
(199, 10, 'master542731648386897.jpg', 1, '2022-03-27 20:14:57', '2022-03-27 20:14:57'),
(200, 10, 'master178611648386897.jpg', 2, '2022-03-27 20:14:57', '2022-03-27 20:14:57'),
(201, 12, 'master302791648387534.jpg', 0, '2022-03-27 20:25:34', '2022-03-27 20:32:31'),
(202, 13, 'master298591648388157.jpg', 0, '2022-03-27 20:35:57', '2022-03-27 21:16:22'),
(203, 14, 'master503711648392989.jpg', 0, '2022-03-27 21:56:29', '2022-03-27 21:56:58'),
(204, 13, 'master940861648393065.jpg', 2, '2022-03-27 21:57:45', '2022-03-27 21:57:45'),
(205, 14, 'master602811648393116.jpg', 2, '2022-03-27 21:58:36', '2022-03-27 21:58:36'),
(206, 15, 'master812701648393364.jpg', 0, '2022-03-27 22:02:44', '2022-03-27 22:03:17'),
(207, 17, 'master593331648394200.jpg', 2, '2022-03-27 22:16:40', '2022-03-27 22:16:40'),
(208, 17, 'master493981648394242.jpg', 0, '2022-03-27 22:17:22', '2022-03-27 22:17:56'),
(209, 18, 'master283491648394410.jpg', 0, '2022-03-27 22:20:10', '2022-03-27 22:28:07'),
(210, 18, 'master142301648394957.jpg', 2, '2022-03-27 22:29:17', '2022-03-27 22:29:17'),
(211, 20, 'master448221648395148.jpg', 0, '2022-03-27 22:32:28', '2022-03-27 22:41:51'),
(212, 20, 'master220861648395812.jpg', 1, '2022-03-27 22:43:32', '2022-03-27 22:43:32'),
(213, 20, 'master344231648395812.jpg', 1, '2022-03-27 22:43:32', '2022-03-27 22:43:32'),
(214, 20, 'master265071648395812.jpg', 1, '2022-03-27 22:43:33', '2022-03-27 22:43:33'),
(215, 15, 'master119651648396001.jpg', 2, '2022-03-27 22:46:41', '2022-03-27 22:46:41'),
(216, 20, 'master922401648396002.jpg', 2, '2022-03-27 22:46:42', '2022-03-27 22:46:42'),
(217, 21, 'master928211648396520.png', 0, '2022-03-27 22:55:20', '2022-03-27 22:59:11'),
(218, 21, 'master400651648396558.png', 2, '2022-03-27 22:55:58', '2022-03-27 22:55:58'),
(219, 22, 'master310481648396791.jpg', 2, '2022-03-27 22:59:51', '2022-03-27 22:59:51'),
(220, 22, 'master221481648396909.jpg', 0, '2022-03-27 23:01:49', '2022-03-27 23:02:53'),
(221, 19, 'master295881648397505.png', 2, '2022-03-27 23:11:45', '2022-03-27 23:11:45'),
(222, 19, 'master613201648397637.png', 0, '2022-03-27 23:13:57', '2022-03-27 23:14:12'),
(223, 23, 'master724191648397930.png', 2, '2022-03-27 23:18:50', '2022-03-27 23:18:50'),
(224, 23, 'master617011648397960.png', 0, '2022-03-27 23:19:20', '2022-03-27 23:19:54');

-- --------------------------------------------------------

--
-- Table structure for table `responsibilities`
--

CREATE TABLE `responsibilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description1` text COLLATE utf8mb4_unicode_ci,
  `image1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description2` text COLLATE utf8mb4_unicode_ci,
  `image2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `responsibilities`
--

INSERT INTO `responsibilities` (`id`, `icon`, `title`, `description`, `image`, `icon1`, `title1`, `description1`, `image1`, `icon2`, `title2`, `description2`, `image2`, `created_at`, `updated_at`) VALUES
(1, 'Image748261645746112.PNG', 'PEOPLE', '<p>We work closely with all of our stakeholders as we believe in the power of collaboration in nurturing sustainable, supportive partnerships with our clients. We aim to provide the best possible service to our customers, and to ensure ongoing advancement within our industry sector and beyond. Our approach of transparency and openness about both our goals and how we will achieve them, helps us attract, develop, support and retain a talented team of employees, which will ultimately secure our long-term success.</p>', 'Image373051649139682.jpg', 'Image685141645746093.PNG', 'PLACE', '<p>We aim to contribute positively to our surrounding and to leave a positive legacy in the communities in which we work and have often been present. We are fully aware of our responsibility in raising the industry standards in Egypt and in enhancing public image of contractors by providing beneficial service and building trust relationships with our customers and the wider population. We are also focused on creating social value nurturing local employment and use of local trades, helping grow local smaller businesses, setting up social enterprises, providing training opportunities, participating in and encouraging community activities. We also have short-term plans of engaging in local education programmes.</p>', 'Image123321649946341.jpg', 'Image971951645746093.PNG', 'PLANET', '<p>We are committed to achieving Net Zero Carbon for our Scope 1 and 2 carbon emissions by 2025 and by 2045 for our Scope 3 emissions.We are dedicated to developing increased care and attention to the environment by minimising the environmental impact of our work, by promoting environmentally positive solutions, and increasing awareness of environmental issues throughout the industry. We ensure that all our site operations follow and exceed best practice guidelines, encouraging better practice throughout the sector. As a responsible business, we believe that construction industry has a vital role to play in reducing the carbon in its operations and that we need to take actions to tackle climate change, so we plan to take a whole lifecycle approach to every project and to make environmental concerns central to all our processes.</p>', 'Image210391649945633.jpg', NULL, '2022-04-14 21:29:28');

-- --------------------------------------------------------

--
-- Table structure for table `responsibilitysliders`
--

CREATE TABLE `responsibilitysliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `responsibilitysliders`
--

INSERT INTO `responsibilitysliders` (`id`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Image618141649946858.jpg', '2022-02-24 20:50:15', '2022-04-14 21:34:18'),
(3, 'Image232381649139588.jpg', '2022-02-24 20:51:28', '2022-04-05 13:19:48'),
(4, 'Image660131649947050.jpg', '2022-04-14 21:36:38', '2022-04-14 21:37:30');

-- --------------------------------------------------------

--
-- Table structure for table `servicecategories`
--

CREATE TABLE `servicecategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `servicecategories`
--

INSERT INTO `servicecategories` (`id`, `logo`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Image787741643758123.jpg', 'BUILD', '2022-02-01 21:28:43', '2022-02-01 21:28:43'),
(2, 'Image671961649138632.jpg', 'FIT-OUT', '2022-02-01 21:33:03', '2022-04-05 13:03:52'),
(3, 'Image586011649033178.jpg', 'CONTROLLED DEMOLISH', '2022-02-01 21:38:16', '2022-04-05 13:04:24'),
(4, 'Image132351649032945.jpg', 'STRENGTHEN', '2022-02-01 21:39:30', '2022-04-04 07:42:25'),
(5, 'Image378251649032840.jpg', 'REPAIR', '2022-02-01 21:40:26', '2022-04-04 07:40:41'),
(6, 'Image213571649032642.jpg', 'ANCHOR', '2022-02-01 21:41:59', '2022-04-04 07:37:22'),
(7, 'Image883681648992996.jpg', 'PROTECT', '2022-02-01 21:43:01', '2022-04-03 20:36:36'),
(8, 'Image889271648992111.jpg', 'TEST', '2022-02-01 21:43:20', '2022-04-03 20:21:51');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_category_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_part_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `second_part_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_category_id`, `title`, `short_description`, `first_part_description`, `second_part_description`, `created_at`, `updated_at`, `video`) VALUES
(1, 2, 'Shell & Core', '<p style=\"text-align:center\"><strong>&quot;Shell and Core&rdquo; is a method where a building is constructed in its most basic form, also known as &quot;Base Build&quot;</strong></p>', '<p>&ldquo;Shell and Core&rdquo; is a method where a building is constructed in its most basic form, also known as &quot;Base Build&quot;. This approach takes the space to its most primitive condition &ndash; a blank canvas for tenants to create a space tailored to meet their particular needs.</p>\r\n\r\n<p>Shell and Core includes the core and exterior elements of a building (shell), while interior fit-out works are left to the occupier and their fit-out partner.</p>\r\n\r\n<h4>WHY SHELL &amp; CORE</h4>\r\n\r\n<p>Shell and core method is commonly used for buildings that are usually occupied by multiple tenants. This approach best suits commercial buildings with businesses that have bespoke layout or design requirements, which makes it almost impossible for landlords to predict their needs. It is more cost &amp; time-effective to leave the space empty until a tenant moves in.</p>\r\n\r\n<p>Both owners and occupiers benefit from this approach:</p>\r\n\r\n<ul>\r\n	<li>Owners Offer Flexible Spaces, Which Enables Occupiers To Craft The Space To Fit Their Own Needs.</li>\r\n	<li>Tenants Can Have Fully Customized Spaces Without Having To Develop A Full Building.</li>\r\n	<li>Construction Process Becomes Faster, While Reducing Costs And Waste As Alterations Of Landlord Installations And Fittings Is Now Avoided.</li>\r\n	<li>Energy Efficient Elements And Common Utilities Can Be Built Into The Project, Offering Their Benefits To All Tenants.</li>\r\n</ul>\r\n\r\n<p>Shell and Core originated in USA. Before this technique was developed, landlords offered buildings with complete fit-out services that was often unsuitable for tenants needs. Consequently, landlords used to waste large amounts of time and money fitting tenant spaces, which often required modifications.</p>', '<h4>WHAT DO SHELL &amp; CORE BUILDINGS INCLUDE</h4>\r\n\r\n<p>Shell and Core consists of the main structural elements of a building, including walls, openings and windowsills. Main electrical and mechanical utilities are often in place, ready for a tenant to connect to. Basically, developers create the main structure (shell) and tenants are responsible to customize the space to meet their own demands.</p>\r\n\r\n<p>Shell and Core provisions usually are:</p>\r\n\r\n<ul>\r\n	<li>Building Skeleton, Foundation And Structural Elements</li>\r\n	<li>Communal Areas, Such As Lobbies, Receptions, Staircases, Toilets, Elevators, Basements, Parking Lots, And Loading Bays.</li>\r\n	<li>Mechanical, Electrical, Plumbing, Fire Detection, And Security Systems</li>\r\n	<li>Building Envelope, Including Insulation, External Walls, Cladding, Glazing, And Roofs</li>\r\n	<li>Landscape (Softscape And Hardscape) Including Plantations, Pavement, Pathways, Fencing, And Boundary Walls</li>\r\n	<li>Signage And Statutory Requirements</li>\r\n	<li>Fire Barriers</li>\r\n</ul>\r\n\r\n<p>There is no standard that defines which elements must be provided by the developer in a core and shell project. Therefore, scope of Shell and Core is best determined in the lease agreement and having meetings to clear any doubts before signing.</p>', '2022-02-04 15:42:10', '2022-03-22 20:30:38', '76979871'),
(2, 1, 'Concrete Structures', '<p>Looking for a Concrete Expert?</p>\r\n\r\n<p style=\"text-align:justify\">BuildIn has a wide experience in achieving self-perform heavy concrete work. BuildIn has served a number of owners, contractors, and construction managers&rsquo; concrete needs over the years.</p>', '<p style=\"text-align:justify\">Our experienced carpenters and concrete finishers excel in forming and pouring concrete for building foundations, walls, slabs, tilt-up panels, exterior paving, and heavy equipment foundations and pits. Projects we typically perform are for the industrial and commercial markets.</p>\r\n\r\n<p style=\"text-align:justify\">What&rsquo;s more, as a general contractor ourselves, we understand the challenges each project faces&mdash;which is why you&rsquo;ll find us to be extremely dependable. BuildIn team are familiar with Egyptian code standards and requirements which allow them to deliver the required structure safely as per the local code.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIn has an experienced management team who can manage the project duration and resources to finish your project on required time with the best quality.</p>', '<p><strong>Why BuildIn as a concrete contractor? </strong></p>\r\n\r\n<ul>\r\n	<li>Foundations, slabs, walls, pits, tilt-up panels, and civil concrete</li>\r\n	<li>Self-perform capabilities</li>\r\n	<li>Outstanding safety record</li>\r\n	<li>Shutdowns/critical schedules</li>\r\n</ul>', '2022-02-04 17:54:52', '2022-03-12 01:19:16', NULL),
(6, 1, 'Steel Structures', '<p style=\"text-align:justify\">BuildIn provides comprehensive structural steel solutions &amp; serves the markets as a designer, fabricator, and erector of specialized steel structures and components.</p>', '<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><span style=\"font-size:12pt\"><span style=\"color:black\"><span style=\"font-size:11.5pt\">We provide one stop shop steel services, from value engineering, design, shop drawing and detailing, fabrication till the erection, no matter if it&rsquo;s a totally new building or an extension or a structural strengthening detail. We also are specialized in steel structures fireproofing protection with a wide experience in achieving required coat thickness to achieve required fire rating resistance. </span></span></span></p>\r\n\r\n<p style=\"margin-left:0in; margin-right:0in; text-align:justify\"><span style=\"font-size:12pt\"><span style=\"color:black\"><span style=\"font-size:11.5pt\">If it&rsquo;s turnkey support you need, we also offer detailing, delivery, and erection services. All our work is performed in accordance with </span><span style=\"font-size:11.5pt\"><span style=\"color:#ec7c30\">BuildIn</span></span><span style=\"font-size:11.5pt\">&rsquo;s strict Quality Management System (QMS) procedures. </span></span></span></p>', '<p>We can produce your:</p>\r\n\r\n<ul>\r\n	<li>Structural Packages</li>\r\n	<li>Stairs</li>\r\n	<li>Plates/Embeds</li>\r\n	<li>Ladders</li>\r\n	<li>Angles</li>\r\n	<li>Platform/Mezzanines</li>\r\n	<li>Sheet Metal</li>\r\n	<li>Handrails</li>\r\n	<li>Primed, Painted or Galvanized Finishes</li>\r\n	<li>Metal Siding and Accessories</li>\r\n</ul>\r\n\r\n<p>With our state-of-the-art equipment and highly experienced team, BuildIn is ready to fabricate anything from your smallest embed to your largest beam.</p>\r\n\r\n<p>Quality on time, every time! To know more about our steel services, check out our projects or get in touch with our experts.</p>', '2022-03-12 01:26:24', '2022-03-12 01:28:22', NULL),
(7, 2, 'Exterior Outfit', '<p style=\"text-align:justify\">Architectural finishes in a holistic sense can refer to a variety of textures, solidities, colors and materials and refers to hard and soft permanently fixed finishes such as plaster or render and other surface coatings, such as paint and wallpaper, internal and external claddings of wood, bricks, stone, glass, resins and aggregates, metals, ceramics and polycarbonates.</p>', '<p style=\"text-align:justify\">Architectural finishes in a holistic sense can refer to a variety of textures, solidities, colors and materials and refers to hard and soft permanently fixed finishes such as plaster or render and other surface coatings, such as paint and wallpaper, internal and external claddings of wood, bricks, stone, glass, resins and aggregates, metals, ceramics and polycarbonates.</p>\r\n\r\n<p style=\"text-align:justify\">This emphasis on a fixed nature indicates a line of separation between architectural and decorative finishes which typically encompass loose decorative finishes for example, curtains and blinds, screens and cushions and temporarily attached fixtures that are designed to be removable for ease of regular updates due to changes in style and taste.</p>\r\n\r\n<p style=\"text-align:justify\">Architectural finishes can serve a supportive structural role, an integral functional role such as protection against water permeability and improvement of the resistance of the primary structure to dirt, as well serve an aesthetic role by enhancing the overall beauty of a building or structure.</p>', '<p>BuildIn can deliver a complete project including an architectural package, focusing on the supply and<br />\r\ninstallation of different fa&ccedil;ade systems and total exterior outfitting for any type of building across Egypt.&nbsp;</p>\r\n\r\n<p>Our wide range of services include:&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Masonry Cladding&nbsp;</li>\r\n	<li>Metal Panels&nbsp;</li>\r\n	<li>Curtain Wall System&nbsp;</li>\r\n	<li>Precast Concrete&nbsp;</li>\r\n	<li>Exterior Louvers&nbsp;</li>\r\n	<li>Wood works&nbsp;</li>\r\n	<li>Roofing&nbsp;</li>\r\n	<li>Landscaping (Soft &amp; Hard Scapes)&nbsp;</li>\r\n</ul>\r\n\r\n<p>Through our highly experienced, skilled manpower and management team we can create and deliver projects on time, to superior quality, and within the approved budget.&nbsp;</p>\r\n\r\n<p>As a pre-qualified contractor for finishes, we provide an effective management solution to resolve technical problems on the site and we are proud of our high levels of professional service and quality standards.&nbsp;</p>\r\n\r\n<p>Our extensive knowledge and experience of different contract types have given us flexibility in our execution methodology for any size or complex project.&nbsp;</p>\r\n\r\n<p>To know more about our services, check out our projects or get in touch with our experts.</p>', '2022-03-12 01:34:00', '2022-03-13 19:38:16', NULL),
(8, 2, 'Interior Fit-Out', '<p style=\"text-align:justify\">As your trusted fit-out contractor, BuildIn will be your full-service solution partner for your fit-out project; handling all phases of the construction process from start to finish in the most professional way.</p>', '<p style=\"text-align:justify\">We respond to our clients&rsquo; demands and know how to specifically craft their vision into reality.BuildIn guarantees to fit-out your space with absolute quality, reliability and integrity. To know more about our services, checkout our projects or get in touch with our experts.</p>\r\n\r\n<p style=\"text-align:justify\">&ldquo;Fit-Out&rdquo; is the process of making interior spaces livable. Under fit-out package falls all the activities related to interior works, starting from mechanical installations, lighting and electrical fixtures and linking them to the main services and utilities, decoration, false ceilings, flooring, partitioning, interior walls, paintings, woodworks (Doors and Windows), etc. up to furnishing the spaces to be ready for use. A completed fit-out means that the building is now ready for occupancy.<br />\r\n&nbsp;</p>', '<p style=\"text-align:justify\">Fit-Outs can generally be categorized into two types:</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Category A fit-out</strong></p>\r\n\r\n<p style=\"text-align:justify\">&ldquo;Cat A fit-out&rdquo; provides a fully functional yet a blank space. This type of fit-out is a step up from shell and core where basic installations, mechanical and electrical services are all in place. There is no standard industry definition, but it typically includes the following elements:</p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Suspended or exposed ceilings&nbsp;</li>\r\n	<li style=\"text-align:justify\">Recessed or suspended lighting&nbsp;</li>\r\n	<li style=\"text-align:justify\">Raised floors&nbsp;</li>\r\n	<li style=\"text-align:justify\">Power distribution to each floor&nbsp;</li>\r\n	<li style=\"text-align:justify\">Internal surface finishes&nbsp;</li>\r\n	<li style=\"text-align:justify\">Ventilation system&nbsp;</li>\r\n	<li style=\"text-align:justify\">Local distribution board&nbsp;</li>\r\n	<li style=\"text-align:justify\">Basic smoke detection and firefighting systems&nbsp;</li>\r\n	<li style=\"text-align:justify\">Blinds</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\"><strong>Category B fit-out</strong></p>\r\n\r\n<p style=\"text-align:justify\">&ldquo;Cat B fit-out&rdquo; provides a fully fitted space that is ready for the tenant to move in. This type of fit-out is usually a bespoke design tailored for the tenant to meet their expectations and needs. Physical elements include:</p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Partitioning (glazed, solid, or bespoke)&nbsp;</li>\r\n	<li style=\"text-align:justify\">Special facilities such as meeting and conference rooms, workstations, specialist lighting, etc...&nbsp;</li>\r\n	<li style=\"text-align:justify\">Joinery (typically around Reception, the tea point/kitchen, and breakout areas)&nbsp;</li>\r\n	<li style=\"text-align:justify\">Finishes and branding&nbsp;</li>\r\n	<li style=\"text-align:justify\">Audio, visual, and multimedia equipment&nbsp;</li>\r\n	<li style=\"text-align:justify\">Furniture</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Want to further discuss your project or custom fit-out needs, check out our projects, and get in touch with our experts for more information.</p>', '2022-03-12 01:41:04', '2022-03-17 15:55:58', NULL),
(9, 2, 'Industrial Renovations', '<p style=\"text-align:justify\">Whether it be the construction of a brand-new shell or renovations to your plant - BuildIn guarantees &ldquo;minimal disruption&rdquo; to your manufacturing process.</p>', '<p style=\"text-align:justify\">Because we understand that the less downtime - the better, we tend to make the transition as smooth and rapid as possible through project planning tailored precisely in line with your business priorities and considerations.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIn is a market-leading construction company in the industrial sector &ndash; we never run out of solutions to bring you the maximum value with minimum disturbance to your production flow. Our wide experience in dealing with running factories has equipped us with various flexible work techniques such as dividing project activities into phases, working out of hours, relocating your team while work is completed - that enables us to handle unique site challenges with the best quality and time-efficient result. Our expertise in costs, time scales, and levels of disruption of different work options enable us to guide you through the decision-making process that best suits your business.</p>\r\n\r\n<p style=\"text-align:justify\">At BuildIn, we aim to create strong long-term connections with the world&#39;s leading experts in our field and close professional relationships with industry specialists in the various fields in which we have worked. Over the last few years, we have successfully partnered up with market-leading Multinational as well as National Manufacturers across Egypt in a wide range of industrial sectors, including but not limited to Automotive, Food, and Beverage, Cement, Steel, Petro-chemicals, and Pharmaceutical Industry. To know more about our services, check out our projects or get in touch with our experts.</p>', '<p style=\"text-align:justify\"><strong>OUR FULL-SERVICE INDUSTRIAL GENERAL CONTRACTING</strong></p>\r\n\r\n<p style=\"text-align:justify\">It&#39;s pretty much common for industrial facilities such as manufacturing factories and smaller production studios to be repurposed for a new business or new product focus. They also need regular maintenance and repair to ensure the best quality of their production process. As a general contracting full-services and construction support solutions provider for industrial sector, our team has mastered the art of industrial projects including new construction, buildouts, additions and remodels. Let our experienced and professional staff take special interest in your unique building requirements. From start to finish, we will coordinate, handle, and manage every aspect of you project to assure the timely completion of your project. Throughout every phase of your project, BuildIn will partner with you to build, expand, renovate, design or update your building&rsquo;s presence.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>EXCELLENT SAFETY RECORDS</strong></p>\r\n\r\n<p style=\"text-align:justify\">Because safety is our prime concern at BuildIn, in contrast to many of our competitors, we allocate a full-time safety manager who develops a project-specific solution to each of our major projects. Our experienced in-house team is highly oriented with construction safety regulations on-site. All our projects are audited monthly on-site for health and safety, ensuring maximum safety to the whole site environment where everyone can go home safely by the end of their shift.</p>', '2022-03-12 01:44:20', '2022-03-12 01:44:20', NULL),
(10, 7, 'Firestop Systems', '<p style=\"text-align:justify\">Fire protection is an increasingly important part of today&rsquo;s building industry largely due to urbanization, which increases the risk for a life-threatening fire to spread quickly.</p>', '<p style=\"text-align:justify\">Passive fire protection systems have become a code requirement in any building design, right from the start of a project, as modern building frameworks are interspersed with multiple utility and network services including cables, pipes and ventilation ducts in both horizontal and vertical directions, resulting in countless penetrations through most of the walls and floors. Each of these penetrations and every joint between different building structures, elements and areas, is potentially a passage for the spread of flames, heat and toxic smoke into adjoining rooms and areas, which &ndash; in the worst case &ndash; may result in uncontrolled fire propagation throughout the entire building. Here comes the role of passive fire protection systems as they are designed to impede the spread of flames, deadly gases and toxic smoke by blocking gaps in walls, ceilings and floors caused by penetrations from pipes, cables and joints in fire-rated areas.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>', '<p style=\"text-align:justify\">BuildIn has successfully installed firestop systems at educational buildings, commercial buildings and other sectors. To know more about our services, checkout our projects or get in touch with our experts.</p>', '2022-03-12 01:45:30', '2022-03-12 01:45:30', NULL),
(11, 7, 'Waterproofing Systems', '<p style=\"text-align:justify\">Waterproofing is the process of making a building&#39;s structure water-resistant so that it remains relatively unaffected by the ingress of water under specified conditions which can cause expensive and irreversible damage to the building.</p>', '<p>It is mostly invisible after construction is completed. Every day the structure of a building is exposed to intense fluctuations in moisture levels and temperatures, resulting in problems such as mold and rot, corrosion and degradation, leaking and damage, all contributing to its deterioration.</p>\r\n\r\n<p>Our team&#39;s hands-on experience and know-how of the most up-to-date waterproofing system solutions and application techniques is a key added value for your next project. Count on BuildIn to provide long-lasting waterproof systems to protect your constructions. Waterproofing Solutions include:</p>\r\n\r\n<p><strong>1. Sheet Waterproofing Membrane</strong></p>\r\n\r\n<p>Sheet membrane systems are reliable and durable thermoplastic waterproofing solutions that can fulfill the requirements of even the most demanding below-ground structures, including those exposed to highly aggressive ground conditions and stress. Sheet membrane systems are designed to withstand exposure and stress extremes such as:</p>\r\n\r\n<ul>\r\n	<li>High level of water pressure&nbsp;</li>\r\n	<li>Aggressive groundwater containing chemicals (sulfates and chlorides in solution)&nbsp;</li>\r\n	<li>Unequal static forces due to settlements or uplift&nbsp;</li>\r\n	<li>Dynamic forces due to earthquakes</li>\r\n</ul>\r\n\r\n<p><strong>Applications:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li>Below-grade Areas, Basements and other concrete structures</li>\r\n	<li>Tunnels&nbsp;</li>\r\n	<li>Pools&nbsp;</li>\r\n	<li>Drinking and Clean Water Storage&nbsp;</li>\r\n	<li>Ponds&nbsp;</li>\r\n	<li>Tank Linings</li>\r\n</ul>\r\n\r\n<p>2. Injections</p>\r\n\r\n<p>Injection is a procedure of pumping cement-based, polyurethane-based, epoxy-based or acrylate-based material where the structural integrity of concrete has been compromised. Injection resins securely fill and seal cracks and voids that have occurred from corrosion or damage during seismic activity and physical impact to re-establish the load bearing capacity and structural integrity which make the structure watertight again for the long term.</p>\r\n\r\n<p>&nbsp;</p>', '<p style=\"text-align:justify\">Injection Materials</p>\r\n\r\n<ul>\r\n	<li style=\"text-align: justify;\"><strong>Polyurethane Foam Resins</strong></li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Polyurethane foaming resins are designed to expand with water to temporarily block the passage of water through the crack or void. Their fast expansive reaction with water forms a tough and flexible elastic foam. For permanent waterproofing, these polyurethane foaming resins are re-injected with a suitable non-foaming injection resin, usually also based on polyurethane.</p>\r\n\r\n<ul>\r\n	<li style=\"text-align: justify;\"><strong>Polyurethane Resins</strong></li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Polyurethane resins are hydrophobic, flexible and used for non-structural injection sealing and waterproofing voids, cracks and joints. Their low viscosity allows good penetration into the concrete structure to seal leaks and achieve a durable elastic seal. Polyurethane resins seal with very strong edge adhesion to concrete and have hydrophobic characteristics. In voids, cracks and joints with high water ingress, pre-injection as temporary water stopping with a polyurethane foaming resin is required.</p>\r\n\r\n<ul>\r\n	<li style=\"text-align: justify;\"><strong>Acrylate Resins</strong></li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Acrylate resins are hydrophilic, very flexible and used for non-structural injection of cracks, joints and voids, including injection hose systems, compartment systems and area injections (e.g. grid and curtain). During application, acrylate resins have extremely low viscosity (similar to water) and therefore have ideal penetration abilities. Their reaction or hardening time is also adjustable, which allows flexibility to adapt the injection material to the prevailing conditions on site (temperature and injection distance, for example). Acrylate resin-based materials seal and waterproof leaks through their hydrophilic swelling behavior in contact with water. The injection equipment is also easily cleaned with water.</p>\r\n\r\n<ul>\r\n	<li style=\"text-align: justify;\"><strong>Epoxy Resins</strong></li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Epoxy resins have relatively high tensile and compressive strengths in relation to concrete. They are generally regarded as &lsquo;rigid&rsquo; materials and are widely used for structural repairs by injecting cracks and voids in load bearing reinforced concrete structures or elements. Their low viscosity allows excellent penetration into cracks and strong adhesion to concrete, which helps to ensure permanent and durable load transfer. Epoxy resin-based materials are suitable for many different structural injection requirements and applications in dry to slightly damp conditions.</p>\r\n\r\n<ul>\r\n	<li style=\"text-align: justify;\"><strong>Microfine Cement Suspension </strong></li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Microfine cement suspensions are non-flexible and therefore non-movement-accommodating, rigid, polymer-modified, injection materials. Also known as microfine cement grouts, they are based on blended microfine cement. They are now widely used for structural injection to seal non-moving cracks, voids and construction joints. Due to their polymer modification, these cement-based materials can also have high flow characteristics and very good penetration ability.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>3. Waterproofing Mortars</strong></p>\r\n\r\n<p style=\"text-align:justify\">Waterproofing Mortar can be used to waterproof interior and exterior concrete, masonry and brick surfaces&mdash;both above and below grade. They are used to protect structures against water infiltration. They also provide solutions to seal against damp soil, seepage and percolating water. Waterproofing Mortar contributes to LEED credits for its ability to reduce a project&rsquo;s carbon footprint and environmental impact.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Applications:</strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align: justify;\">Below-Grade Structures and Basements&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Balconies and Terraces&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Swimming Pools&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Wet Rooms and Bathrooms</li>\r\n	<li style=\"text-align: justify;\">Bridges&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Tanks and Reservoirs&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Podiums&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Bridge Decks&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Tank Lining&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Wet Rooms</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\"><strong>4. Bituminous Membrane</strong></p>\r\n\r\n<p style=\"text-align:justify\">Bitumen, also known as asphalt, is a sticky, black and highly viscous liquid or semi-solid form of petroleum. It is widely used in construction as it has an exceptionally long lifespan. Bituminous membranes are suitable for waterproofing roofs, basements, below-ground structures, bridges and other structures.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Applications:</strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align: justify;\">Flat Roofs&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Pitched Roofs&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Basements&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Podiums&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Terraces&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Green Roofs&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Below-Grade Structures&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Bridge Decks&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Tank Lining&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Parking Levels&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Roof Renovations</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">To know more about our services, checkout our projects or get in touch with our experts.</p>', '2022-03-12 01:55:10', '2022-03-12 01:55:10', NULL),
(12, 7, 'Joint Sealing', '<p style=\"text-align:justify\">It is at the joints where a building is weakest to leakage because this is where different building elements, materials and trades meet.</p>', '<p style=\"text-align:justify\">A joint is where two components meet and may occur within an element of construction or at part of an interface between two elements to control cracking and to provide relief for concrete expansion and contraction caused by temperature and moisture changes. Joints allow for needed movement of the materials but must remain water, air, heat, cold and vapor tight. Joint seals correctly specified and professionally applied will manage to keep the structure sustainably tight during its entire lifespan. You can count on BuildIn as a certified applicator and as an experienced contractor to handle such a critical<br />\r\nactivity of your project. To know more about our services, checkout our projects or get in touch with our experts.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Typical Types of Joints in Structures</strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\"><strong>Construction joint</strong></li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Construction joints are designed between concrete placements intentionally created to facilitate construction, or as a structural measure to transfer load, for example. The reinforcement in construction joints is therefore continuous through the joint.</p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\"><strong>Connection joint</strong></li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Connection joints are a means of joining individual members of concrete sections through its full thickness without a defined joint gap to form a complete assembly. The reinforcement in connection joints is discontinuous. When the concrete section contracts, joint movement (joint opening) is possible. When it expands, pressure transmission is possible.</p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\"><strong>Movement joint (Expansion joint)</strong></li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Movement or expansion joints split components through their full thickness with a joint gap of defined width to allow and accommodate the expansion of adjacent elements. The reinforcement in movement joints is discontinuous. Movement joints allow differential movement primarily due to thermal movement, but also many other possible influences such as load or settlement in one or more directions of the areas, sections or structures separated by the joint. An expansion joint sealing system must be designed and installed to effectively compress or expand to accommodate this movement, whilst maintaining a watertight seal.</p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\"><strong>Control Joint (Controlled Crack-Induced, Dummy or Contraction Joint)</strong></li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Control joints are intended to control cracks by engineered concrete cross-section weakening and formation of a controlled point of cracking through the concrete. This relieves stresses due to temperature and shrinkage, which thereby prevents uncontrolled cracking in the wall. Cracking takes place in the designed and crack-induced position instead.</p>', '<p style=\"text-align:justify\"><strong>Joint Sealing Solutions</strong></p>\r\n\r\n<p style=\"text-align:justify\"><strong>1. Construction Foams</strong></p>\r\n\r\n<p style=\"text-align:justify\">Construction foams are mainly polyurethane (PU) resin-based foams, which cure in contact with moisture when applied. They can be used as a gap-filling, adhesive<br />\r\nsealant to fix, fill and insulate against noise, cold, and drafts because they have good insulation properties and are effective at dampening sound. They are also used to bond insulation panels and insulating system components on top of each other due to their excellent adhesive properties and fast curing ability.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>2. Building Envelope Systems</strong></p>\r\n\r\n<p style=\"text-align:justify\">Sustainable construction begins with an efficient building envelope as the energy lost through a building&#39;s walls, roofs and windows is the largest single waste of energy. As the single largest building element, the building envelope (shell) separates the interior of the building from the external environment and includes the roof, walls, windows, glazing, floors and all the joints in between. A high-performance envelope must meet the project&rsquo;s requirements for acoustic, thermal, structural, visual, air-quality, fire-resistant, watertight, temperature-control and aesthetic needs.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>3. Firestop Systems</strong></p>\r\n\r\n<p style=\"text-align:justify\">Fire protection is an increasingly important part of today&rsquo;s building industry largely due to urbanization as it increases the risk is for a life-threatening fire to spread quickly. Buildings and structures need protection against this imminent threat, therefore firestops are installed to prevent the spread of flames, deadly gases and toxic smoke through openings created during building upgrades and the installation of electrical, communications, plumbing, and ventilation systems, as well as grease ducts. That&#39;s why it is important to include passive fire protection &ndash; firestop systems &ndash; in any building design, right from the start of a project.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>4. Civil Engineering Joints</strong></p>\r\n\r\n<p style=\"text-align:justify\">Civil engineering generally deals with the construction, maintenance and repair of all types of utility and infrastructure projects. All these projects contain joints and connections which need to be securely sealed and protected.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>5. Joint Waterproofing</strong></p>\r\n\r\n<p style=\"text-align:justify\">Due to the nature of concrete and reinforced concrete, structures are divided into sections by forming joints that are designed to accommodate movement, namely three types &ndash; construction joints, movement joints and connection joints. The key function of joint sealing is to minimize water ingress and create a secure waterproofing barrier. It is important to consider when waterproofing joints in concrete structures that the seal used can accommodate the type of joint and the anticipated joint movement due to static reasons or temperature changes. The sealants must be permanently elastic, form stable and offer UV-resistance to protect against damage that may occur from any future movements. Joint sealing solutions are recommended for use in all kinds of construction and joints for waterproofing.</p>', '2022-03-12 02:00:30', '2022-03-17 18:03:45', NULL),
(13, 7, 'Flooring Systems', '<p style=\"text-align:justify\">Flooring is the most important feature in any building to make it beautiful, practical and add a sense of personality.</p>', '<p style=\"text-align:justify\">In addition to aesthetic purposes, flooring also contributes to the protection of building structure. The floor is the finishing and the material most touched by the regular user of any building, structure or home. Any building you set foot into, you come in contact with its floor. Beneath your feet, there is an entire system of carefully crafted layers and parts which go together to create this finished look. We focus on indoor air quality, low VOC and odor, system durability with minimal maintenance requirements, easy and economic floor refurbishment and low energy demand in the life cycle.</p>\r\n\r\n<p style=\"text-align:justify\">Our flooring solutions meet the highest requirements for industrial floor systems. BuildIN has successfully executed flooring projects with automotive sector, pharmaceuticals and a wide range of other industrial operations. Based on our decades of experience we will provide you with the best solution for ys3our individual needs. We take into account the design life, your operational requirements, surface design and installation details. To know more about our services, checkout our projects or get in touch with our experts. The flooring system solutions that we provide include:</p>\r\n\r\n<p style=\"text-align:justify\"><strong>1. Concrete Flooring</strong></p>\r\n\r\n<p style=\"text-align:justify\">Concrete floors are used as the base slab for most commercial and industrial floor finishes. They are characterized by high quality, durability and versatility relative to cost. Concrete also contributes to hygiene and good air quality as it&#39;s resistant to fire, stains, water, bacteria, odors, and it cures to provide a completely seamless, smooth floor with no cracks, gaps or joints. Since one of the key characteristics of concrete flooring is ability to withstand pressure from heavy equipment, such as trucks or forklifts, it&rsquo;s frequently used in warehouses, factories and production facilities.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>2. Industrial Floor Coating</strong></p>\r\n\r\n<p style=\"text-align:justify\">Industrial coating systems and topcoats are the most important and often only practical solution to protect steel against corrosion, acid and fire. Lack of protection or improper application of coating systems frequently lead to problems with the visual appearance of the structure and even more crucially, structural degradation and possible failure. Appropriate protective coatings and regular maintenance ensure long-term protection of steel structures and can avoid cost-intensive total repair or even decommissioning. Our extensive engineering background at BuildIn enables us to offer expert tailoring of practical, robust and high-performance acid protection, corrosion protection and fire protection solutions based on your coating requirements.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>3. Decorative Floor Coating</strong></p>\r\n\r\n<p style=\"text-align:justify\">Decorative, seamless resin floor systems offer endless possibilities for creative flooring design. It can be stained by any color which gives you the options to combine it with decorative aggregates, graphics, brand imagery, logos applied in different application methods to achieve unique interior design. Our selection of decorative floor coating systems gives you the options to suit any space with the combination of beauty and strength that you need.</p>', '<p style=\"text-align:justify\"><strong>4. Car Park &amp; Traffic Coating</strong></p>\r\n\r\n<p style=\"text-align:justify\">Floor coating systems for parking garages must meet the highest demands as outdoor, multi-story and underground car parks are subject to many different stresses, including atmospheric conditions, automotive fluids as well as vehicular and pedestrian traffic load. BuildIN uses a wide range of durable floor coating systems with high wear and chemical resistance features that that reduces vibrations and traffic noise and ensures watertightness and are designed to meet the requirements of specific areas of a car park structure, including ground bearing slabs, intermediate decks, top decks and exposed areas as well as ramps and entrance areas. We also use flooring solutions with a wide range of color options, which fulfills specific design demands. Our team of experts uses application technologies that ensure short downtime of parking garages with money saving and the best quality.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>5. Food &amp; Beverage Factory Flooring</strong></p>\r\n\r\n<p style=\"text-align:justify\">Food and beverage factories include a vast array of project types - A dairy or beverage plant. A catering or industrial kitchen. A slaughterhouse or fish packing facility. A bakery. A fruit and vegetable processing plant. A snack production facility. Freezers, bottling lines, raw material processing, packaging and storage areas. These are a few examples of business and areas that exist in the food processing industry. Each of them has its own specific requirements for flooring, walls, and other surfaces whether for new construction or renovation. Choosing and installing the right floor is critical to every work environment, and here comes the role of BuildIN expertise to assist with our strong technical background in order to fulfill the three critical factors in a food processing factory &ndash; Hygiene, Safety &amp; Durability.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>6. Sports Flooring</strong></p>\r\n\r\n<p style=\"text-align:justify\">Multi-purpose sports floor systems should provide comfort and safety for training and competition combined with different levels of resistance to mechanical loads. It is installed in sports facilities, schools, universities, ballet academies, tennis and fitness centers, you will find our sports floors everywhere.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>APPLICATIONS:</strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Indoors Sports Floors&nbsp;</li>\r\n	<li style=\"text-align:justify\">Outdoors Sports Floors&nbsp;</li>\r\n	<li style=\"text-align:justify\">Fitness Floors&nbsp;</li>\r\n	<li style=\"text-align:justify\">Indoor/Outdoor Tennis Floors</li>\r\n</ul>', '2022-03-12 02:18:17', '2022-03-17 20:54:32', NULL),
(14, 4, 'Concrete Jacketing', '<p style=\"text-align:justify\">One of the challenges in strengthening concrete structures is the selection of strengthening methods that will enhance the strength and serviceability of the structure while addressing limitations such as constructability, building operations, and budget.</p>', '<p>Additional strength may be needed due to the deficiency in the structure&#39;s ability to carry the original design loads. Deficiencies may be the result of deterioration. Jacketing is the most popularly used method for strengthening of building Columns.</p>\r\n\r\n<p>Concrete jacketing is probably the mostly used technique for the strengthening of RC members. The jacket increases both the flexural strength and the shear strength of the beam or the column. It is constructed either with cast-in-place concrete or, more often, with shotcrete. The method involves the addition of a layer of reinforced concrete in the form of the jacket using longitudinal steel reinforcement and transverse steel ties outside the perimeter of the existing member (Figure 1).</p>\r\n\r\n<p>Concrete jacketing involves adding a new layer of concrete with longitudinal reinforcement and closely spaced ties. The jacket increases both the flexural strength and the shear strength of the beam or the column.</p>\r\n\r\n<p>&nbsp;</p>', '<p>Advantages:</p>\r\n\r\n<ul>\r\n	<li>Increases the seismic resistance of the building without any demolition.&nbsp;</li>\r\n	<li>Increases the ductile behavior and lateral load capability of the building strength&nbsp;</li>\r\n	<li>Improves the stiffness of the building</li>\r\n</ul>\r\n\r\n<p>Reinforced concrete jacketing can be employed as a repair or strengthening scheme. Damaged regions of the existing members should be repaired prior to their jacketing. There are two main purposes of jacketing of columns:</p>\r\n\r\n<p>1. Increase in the shear capacity of columns in order to accomplish a strong column-weak beam design. 2. To improve the column&#39;s flexural strength by the longitudinal steel of the jacket made continuous through the slab system are anchored with the foundation. It is achieved by passing the new longitudinal reinforcement through holes drilled in the slab and by placing new concrete in the beam-column joints as illustrated.</p>\r\n\r\n<p>Objectives:</p>\r\n\r\n<ul>\r\n	<li>Strengthening of column using additional concrete</li>\r\n	<li>Analyzing the existing building for the additional live load</li>\r\n</ul>\r\n\r\n<p>BuildIn has extensive resources to undertake any concrete strengthening project that utilizes advanced composite materials. From preliminary design concepts to budgeting, to execution, we can be your single-source solution for concrete structural strengthening.</p>', '2022-03-12 02:22:36', '2022-03-12 02:22:36', NULL),
(15, 4, 'Steel Jacketing', '<p style=\"text-align:justify\">Confining reinforced concrete columns in steel jackets is an effective method to increase basic strength capacity.</p>', '<p style=\"text-align:justify\">Steel jacketing not only provides enough confinement but also prevents deterioration of shell concrete, which is the main reason of bond failure and buckling of longitudinal bars. In most applications the steel plates are bonded to the concrete by means of epoxy adhesives, and in addition to bonding, it is a good practice to also bolt the steel plates to the concrete.</p>\r\n\r\n<p style=\"text-align:justify\">While this process is relatively labour and equipment intensive, there are many applications where the use of steel plates is the best alternative for structural strengthening.</p>', '<p><strong>Applications:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li>Bridges&nbsp;</li>\r\n	<li>Parking Structures&nbsp;</li>\r\n	<li>Industrial Plants&nbsp;</li>\r\n</ul>\r\n\r\n<p><strong>Advantages:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li>Low cost&nbsp;</li>\r\n	<li>Vandalism and wear-resistant</li>\r\n</ul>', '2022-03-12 02:24:24', '2022-03-12 02:24:24', NULL),
(16, 4, 'Prefabricated Carbon Fiber Reinforced Polymer (CFRP) Plates', '<p>We use Fiber Reinforced Polymer (FRP) composite systems to strengthen a wide variety of structures for flexure, shear, axial loads and seismic upgrading.</p>', '<p style=\"text-align:justify\">Both Glass FRP (GFRP) and Carbon FRP (CFRP) are used in these applications with CFRP being used for most structural applications. FRP composites have been used for decades in aerospace and manufacturing applications where low weight, high tensile strength, and non-corrosive structural properties are required.</p>\r\n\r\n<p style=\"text-align:justify\">In civil engineering applications, CFRP has proven to be extremely effective as externally bonded reinforcement. CFRP materials are successful because they exhibit low creep, and compared with steel, are thinner, lighter, and have 10 times the tensile strength. In addition to bonding the CFRP to the surface, it can also be placed in slots routed into the concrete to strengthen the structure for both flexural and shear strengthening applications. This type of strengthening system is commonly referred to as near surface mounted (NSM) FRP strengthening.</p>', '<p><strong>Applications:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li>Concrete structures&nbsp;</li>\r\n	<li>Wooden structures&nbsp;</li>\r\n	<li>Steel structures</li>\r\n</ul>\r\n\r\n<p><strong>Advantages:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li>Quick completion time&nbsp;</li>\r\n	<li>Cost-effective compared to remove and replace option&nbsp;</li>\r\n	<li>No additional dead load added to the structure&nbsp;</li>\r\n	<li>Minimal access to work area required&nbsp;</li>\r\n	<li>No heavy equipment involved&nbsp;</li>\r\n	<li>Corrosion-resistant system&nbsp;</li>\r\n	<li>No maintenance required</li>\r\n</ul>', '2022-03-12 02:27:14', '2022-03-12 02:27:14', NULL),
(17, 4, 'Fiber Reinforced Polymer (FRP) Fabrics', '<p>Carbon fiber has very high tensile strength and is also very lightweight.</p>', '<p style=\"text-align:justify\">When bonded to the exterior of a concrete column, beam, or slab, it can add significant strength without adding weight that would increase the load on foundations and other structural members. The composite material is called fiber-reinforced plastic (FRP). FRP wraps are easy to apply and can be used on any size or shape of structural member. Traditional techniques for strengthening, such as adding concrete and reinforcing steel around the outside of a structural member (often with shotcrete), external post-tensioning, or adding structural steel supports (shoring) often are more expensive due to the extra work to get everything into place.</p>\r\n\r\n<p style=\"text-align:justify\">The primary reason to use this technique is to add strength to an existing structure. In some cases, it might be used on new construction, although at this time that is usually only in response to some sort of design or construction error. Carbon fiber wrapping for columns has become is an ideal alternative to the traditional method of structural strengthening due to the flexibility in use of carbon fiber mesh which makes it easy to wrap it around structures that are unsuitable to traditional methods. Fiber wrapping concrete columns improve durability and reduces the need for further maintenance and repair work.</p>', '<p style=\"text-align:justify\">Advantages:</p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Carbon fiber wrapping for columns is beneficial for improving the performance of the structure against seismic activity.</li>\r\n	<li style=\"text-align:justify\">Fiber wrapping concrete columns act as a shield to corrosion. The carbon fiber wrap concrete repairs and reinforce the concrete structures ▪ Application of carbon fiber to the exterior of a concrete column, beam, or slab, adds significant strength without giving additional weight on foundations and other structural members .</li>\r\n	<li style=\"text-align:justify\">Fiber wrapping are a cost-effective method and safe against seismic events or natural disasters .</li>\r\n	<li style=\"text-align:justify\">Carbon fiber wrap concrete repairs the buildings that are damaged or dilapidated .</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\"><strong>Carbon Fiber Wrapping for Industrial and Commercial Buildings</strong></p>\r\n\r\n<p style=\"text-align:justify\">Carbon fiber wrapping for columns or carbon fiber reinforced concrete is applicable in various construction projects. They work towards building maintenance or concrete structure strengthening for residential or industrial buildings, commercial properties, industrial chimneys and bridges. Carbon fiber wrapping for industrial purposes applies to strengthen structures subject to heavy loading and machinery vibrations. The application of carbon fiber wrapping for commercial enables repairing apartment buildings, sports arenas, concrete slabs, and more</p>', '2022-03-12 02:29:46', '2022-03-29 21:08:11', NULL);
INSERT INTO `services` (`id`, `service_category_id`, `title`, `short_description`, `first_part_description`, `second_part_description`, `created_at`, `updated_at`, `video`) VALUES
(18, 5, 'Concrete Repair', '<p>As a specialty repair contractor, one of BuildIn&rsquo;s core business is concrete repair and restoration.</p>', '<p style=\"text-align:justify\">BuildIn is not limited to a single concrete repair method; our experience and expertise combine specialized and conventional concrete repair methods to provide the most practical solutions for all concrete repair projects. From small concrete repair projects to multi-faceted full scale restoration projects, BuildIn can deliver on every phase including:</p>\r\n\r\n<ul>\r\n	<li style=\"text-align: justify;\">Assisting owners and engineers in problem identification.</li>\r\n	<li style=\"text-align: justify;\">Development of concrete repair options and feasibility studies.</li>\r\n	<li style=\"text-align: justify;\">Budget pricing of repair options and delivery options to meet your facility&#39;s needs.</li>\r\n	<li style=\"text-align: justify;\">Turn-key delivery and management of the complete repair process.</li>\r\n	<li style=\"text-align: justify;\">Design-build contracts</li>\r\n	<li style=\"text-align: justify;\">Maintenance contracts</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Common Reasons Why Concrete Requires Repair</p>\r\n\r\n<p style=\"text-align:justify\">Concrete is widely used for everything from foundations to bridges. Water tanks, buildings, and even dams can be made with concrete. It&rsquo;s a strong, durable, and well understood material. But that doesn&rsquo;t mean it&rsquo;s going to last forever without a bit of attention and sometimes repair. In fact, concrete today isn&rsquo;t even the same as the concrete that was poured for your commercial parking garage twenty years ago. Below are some of the most common reasons that concrete requires repair:</p>\r\n\r\n<p style=\"text-align:justify\">o Rebar Corrosion</p>\r\n\r\n<p style=\"text-align:justify\">Concrete is often reinforced with steel rebar. The rebar is meant to help keep the concrete from crumbling. Unfortunately, the opposite seems to be the real problem. If rebar is exposed to excess carbon chlorides or dioxide, it will begin to corrode, causing the concrete around it to crumble too. Rust can burst concrete.</p>\r\n\r\n<p style=\"text-align:justify\">o Alkali-Silica Reactions (ASR)</p>\r\n\r\n<p style=\"text-align:justify\">This is a serious problem for some concrete structures that were built before the reaction was well understood. ASR is a reaction within the concrete due to the chemical make-up of some aggregates. Alkaline cement will react with water to form a gel, causing cracks in the concrete.</p>\r\n\r\n<p style=\"text-align:justify\">o Cavitation | Abrasion</p>\r\n\r\n<p style=\"text-align:justify\">Water will wear away at concrete over time, which is commonly seen in pipes. Abrasion can also be due to physical wear from items such as forklifts.</p>\r\n\r\n<p style=\"text-align:justify\">What to Look For</p>\r\n\r\n<p style=\"text-align:justify\">Visual signs of concrete deterioration are cracks, rust stains, efflorescence, water leakage, concrete spalling and hollow sounding delamination. Other signs of deterioration include wear, exposed aggregates, discoloration and settlement.</p>\r\n\r\n<p style=\"text-align:justify\">Repair is Possible!</p>\r\n\r\n<p style=\"text-align:justify\">Fortunately, it is possible to repair concrete that is showing signs of any of these problems. Ensuring that the concrete structures will remain safe and functional for years to come requires maintenance and repair, but most importantly the cause of the deterioration should be identified so that the concrete repairs last and the problem doesn&rsquo;t reoccur. Depending on the severity, large portions of the structure may need to be re-built. But with the right concrete repair team, you can often avoid a complete tear down.</p>\r\n\r\n<p style=\"text-align:justify\">To ensure that the concrete structures will remain safe and functional for years to come requires maintenance and repair.</p>', '<p style=\"text-align:justify\">BuildIn offers a range of durable concrete repair and protection techniques that we mostly use in our projects including:</p>\r\n\r\n<p style=\"text-align:justify\">1. Concrete Replacement</p>\r\n\r\n<p style=\"text-align:justify\">Replacement concrete is a method of repair for defective concrete when the defects or cracks in concrete have large area. In this method of concrete repair, defective concrete is removed from the structural member and area is prepared for repairs and then replacement concrete is used. This is often still the most effective and economical way to repair concrete. Removing deteriorated concrete and replacing it with traditional Portland cement concrete is often still the most effective and economical way to repair concrete.</p>\r\n\r\n<p style=\"text-align:justify\">Advantages:</p>\r\n\r\n<ul>\r\n	<li style=\"text-align: justify;\">BuildIn understands concrete mix design and can assure optimum performance with good concreting practices&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Materials and equipment readily available throughout construction industry&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Easy to assess the future performance and life expectancy of the repair&nbsp;</li>\r\n	<li style=\"text-align: justify;\">The cost of conventional repairs is very competitive&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Can be combined with cathodic protection to substantially extend the life of structures</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">2. Specialized Repair</p>\r\n\r\n<p style=\"text-align:justify\">This includes repairs where specialized methods are used for concrete repair, such as fast-sealing mortars, acid resistant mortars and coatings and other special methods. BuildIn has the in-house expertise and equipment to offer innovative solutions for completing the most challenging concrete repairs in time and on budget. Many of our projects are carried out during brief plant shutdowns when there is no<br />\r\ntime for the use of conventional repair methods or materials. Hence, specialized repair techniques are used instead, and these may include hydro demolition, surface preparation using high pressure water blasting, concrete pours with pre-placed aggregate, tremie concrete, shotcrete repair, work with cofferdams, overhead pumping of concrete into suspended forms, and the use of a wide variety of fast setting or other special performance repair materials.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Applications:</strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align: justify;\">Bridges&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Pulp and Paper Industry&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Chemical Plants&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Mining and Refining&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Steel Plants&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Hydro Electric&nbsp;</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\"><strong>Advantages:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align: justify;\">Quick results, reducing plant downtime&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Cost effective when compared with conventional solutions available&nbsp;</li>\r\n	<li style=\"text-align: justify;\">High performance repairs&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Aid Resistance&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Abrasion Resistant</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">3. Concrete Overlays &amp; Resurfacing</p>\r\n\r\n<p style=\"text-align:justify\">BuildIn applies overlays to resurface deteriorated floors that vary from several inch thick structural toppings to thin 1/4 inch-thick polymer concrete overlays. Bonded overlays can be installed on new precast slabs to increase the load carrying capacity of the slab or can be installed on top of repaired/patched concrete slabs to extend the durability of the repairs. For concrete overlays to perform as required, specially formulated shrinkage compensated, latex modified, high density, low slump concrete can be used. Thin systems provide resurfacing without adding much weight to the existing slab and can be trowel applied, spray applied, or self-leveling. Specialized iron-aggregates overlays can be used for high impact and abrasion resistance.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Applications:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align: justify;\">Bridges&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Parking Structures&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Buildings&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Chemical Plants&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Steel Plants&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Mining and Refining</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\"><strong>Advantages:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align: justify;\">Will extend the life of the structure&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Economical repair when compared with the replacing slab option&nbsp;</li>\r\n	<li style=\"text-align: justify;\">High performance wearing surface&nbsp;</li>\r\n	<li style=\"text-align: justify;\">Corrosion protection by slowing down penetration of salts into the structure&nbsp;</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\"><strong>Why Choose BuildIn&nbsp;</strong></p>\r\n\r\n<p style=\"text-align:justify\">BuildIn has extensive experience in the rehabilitation of all sorts of concrete structures, from small repairs to major concrete restoration projects. We have mastered the process of selecting the most suitable materials and equipment for your application. Our team can meet the precise requirements of any client, as our repair works comply to the standards and guidelines set by the International Concrete Repair Institute (ICRI). Contact us and let us prove why BuildIn should be your single source contractor for all your concrete problems.</p>', '2022-03-12 02:39:14', '2022-03-12 02:40:05', NULL),
(19, 5, 'Grouting', '<p>Grouting and Injection are important components of the modern era concrete construction industry.</p>', '<p>Construction, one of the recognized leaders in the concrete grouting and crack injection technology, has perfected the delivery of these systems over hundreds of projects. Experienced BuildIn Construction personnel are completing concrete grouting injection projects worldwide, sometimes in very challenging situations such as subzero temperatures, nuclear radiation, remote locations, leaking structures, or working with concrete contaminated with oil or other contaminants. Whatever your grouting and injection requirements are, as an experienced concrete company BuildIn has the resources (technical expertise, skilled labour, a wide selection of materials, and the right equipment) to accommodate all your needs. Some of the main grouting and injection solutions that we are involved with are:</p>\r\n\r\n<ul>\r\n	<li>Cementitious Grouting</li>\r\n	<li>Chemical Grouting</li>\r\n</ul>\r\n\r\n<p>1. Cementitious Grouting</p>\r\n\r\n<p>Cementitious grouting is the process where a fluid cementitious grout is pumped under pressure to fill forms, voids, and cracks. Pressure grouting using a relatively low-cost cementitious material is used to fill cracks and voids where large volumes of material are required or where high service temperatures are anticipated. Cementitious grouting is also used to bond and protect rock anchors in concrete or rock. BuildIn Construction has a wide assortment of specialized equipment for cementitious pressure grouting.</p>\r\n\r\n<p><strong>Applications:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li>Bridges&nbsp;</li>\r\n	<li>Marine Structures&nbsp;</li>\r\n	<li>Hydroelectric Structures and Dams&nbsp;</li>\r\n	<li>Mining and Refining&nbsp;</li>\r\n</ul>\r\n\r\n<p><strong>Advantages:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li>Excellent Performance&nbsp;</li>\r\n	<li>Cost Effective Solution&nbsp;</li>\r\n	<li>Permanent Solution&nbsp;</li>\r\n	<li>Can be designed to provide predictable and measurable structural properties</li>\r\n</ul>\r\n\r\n<p>Cementitious grouting applications that BuildIn is involved with are broken down in the following categories, which differ only by the properties of the material used and the scope of the grouting:</p>\r\n\r\n<ul>\r\n	<li>Cementitious Pressure Grouting (rock, concrete, and anchor bolt applications)</li>\r\n	<li>Microfine Cement Grouting (crack repair applications)&nbsp;</li>\r\n	<li>Cement Fondue Grouting (24 hour &ndash; fast setting applications)</li>\r\n</ul>', '<p>2. Chemical Grouting Systems</p>\r\n\r\n<p>Chemical grouting systems use materials that react with water to set and are ideal for solving non-structural water leakage problems. Applications include waterproofing cracks and joints, mine shafts, sewer lines, potable water tanks and dams. Once the chemical grouting is completed the end result is a crack, joint or void that is completely filled with a flexible (rubber like) product that provides a leak resistant repair while allowing minor movements of the structure without compromising the water tightness of the repair. BuildIn Construction is well known worldwide to be a recognized leader in the chemical grouting technology. BuildIn with its wide selection of repair materials, specialized equipment and experienced technicians is able to seal cracks as small as 0.05 mm (0.002&Prime;) in width.</p>\r\n\r\n<p><strong>Applications:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li>All concrete structures</li>\r\n</ul>\r\n\r\n<p><strong>Advantages:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li>Seal cracks from further deterioration&nbsp;</li>\r\n	<li>Stop the leakage of air, water and fluids through concrete structures&nbsp;</li>\r\n	<li>Inexpensive when compared with the remove and replace option</li>\r\n</ul>', '2022-03-12 02:44:10', '2022-03-12 02:44:10', NULL),
(20, 5, 'Spray Concrete (ShotCreting)', '<p>It is a technique which is used generally in lieu of conventional concrete. It gives an excellent bonding to other materials.</p>', '<p style=\"text-align:justify\">It can be applied as dry-mix or wet-mix process.<br />\r\nShotcreting gives high strength, low absorption, good resistance to weathering and resistance to some forms of chemical attack. It has a dense composition and has low shrinkage and low permeability which gives it a good durability.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>', NULL, '2022-03-12 02:45:43', '2022-03-12 02:45:43', NULL),
(21, 5, 'Injection', '<p style=\"text-align:justify\">Epoxy injection is an ideal way to structurally repair cracked concrete, seal leaks, and stabilize vibrating machine bases.</p>', '<p>Epoxy crack injection products are specifically designed to be dispensed through two components. Mixing and metering injection equipment, are all 100% solids and contain no solvents or fillers. BuildIn offers installations with a wide selection of specialized materials for architectural precast panels, underwater, low and high application temperatures, high structure temperatures, chemical and radiation resistance, large voids, narrow cracks, plate bonding, wooden beams, and structures subject to vibration during installation. BuildIn is well known in Egypt to be a recognized leader in the crack repair technology. BuildIn, with its wide selection of concrete repair materials, specialized equipment and experienced technicians, is able to repair and seal concrete cracks as small as 0.05 mm (0.002&Prime;) in width.</p>\r\n\r\n<p>&nbsp;</p>', '<p><strong>Applications:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li>All concrete and wooden structures&nbsp;</li>\r\n</ul>\r\n\r\n<p><strong>Advantages:&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li>Restore structural and/or design strength to the cracked concrete structure&nbsp;</li>\r\n	<li>Seal and protect cracks from further deterioration.&nbsp;</li>\r\n	<li>Eliminate concrete spalling initiated by cracking and aggravated by freeze thaw cycling&nbsp;</li>\r\n	<li>Stop the leakage of air, water and fluids through concrete structures&nbsp;</li>\r\n	<li>Inexpensive when compared with the remove and replace option</li>\r\n</ul>', '2022-03-12 02:48:43', '2022-03-12 02:48:43', NULL),
(22, 6, 'Anchors Chemical Dowelling', '<p style=\"text-align:justify\">Chemical anchoring is a technique for fastening to concrete and similar substrates that provides more flexibility than mechanical anchoring.</p>', '<p style=\"text-align:justify\">A mechanical anchor, such as a sleeve anchor, wedge anchor or drop-in anchor, is inserted in the concrete and expands upon tightening. This expansion causes the anchor to grip the wall of the hole and provide an extremely strong hold. Whilst being a very popular and economical option, there are, however, some limitations.</p>\r\n\r\n<p style=\"text-align:justify\">With chemical anchoring, a resin is injected into the hole prior to insertion of the stud. With this, the chemical naturally fills in all irregularities and therefore makes the hole airtight and waterproof, with 100% adhesion. And with mechanical anchors, each predetermined size&mdash;length (embedment) and diameter&mdash;has its own load capacity limits. Chemical anchors have virtually unlimited embedment depth, so you can embed any length of rod into the hole to increase the load capacity. And if you choose to use a larger-diameter hole with a thicker rod, you increase load capacity again.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>', '<p style=\"text-align:justify\">Chemical anchors &mdash;also known as chemical studs&mdash;can also be placed towards the edge of concrete substrates and through masonry block. The non-expanding nature of a chemically held rod drastically reduces the chance of the surrounding concrete cracking. This is very good for securing railings close to edge, or concrete stairs, and similar applications. Finally, chemical anchoring gives you the opportunity to make slight adjustments to the stud&rsquo;s alignment during the chemicals open working time, whereas a mechanical anchor needs a hole to be drilled millimeter-perfect every time to correct depth and angle, and if it isn&rsquo;t, it cannot be used.</p>\r\n\r\n<p style=\"text-align:justify\">The downside of chemical anchors to some contractors, is that they are more complicated to install, and incorrect installation habits can jeopardize the anchor&rsquo;s capacity. BuildIn experienced team are well trained with all installation techniques. We also perform pull-out testing to confirm the quality of chemical installation.</p>', '2022-03-12 02:50:14', '2022-03-12 02:50:14', NULL),
(23, 6, 'Rebar Chemical Dowelling', '<p><strong>Post Installed Rebar </strong></p>\r\n\r\n<p style=\"text-align:justify\">When the construction is in progress, we often encountered unforeseen circumstances in the site that leads to revisions and rectifications.</p>', '<p style=\"text-align:justify\">Though the engineering plans and drawing have already been approved for construction, still it is considered as a work in progress until fully constructed. These modifications may be due to client requests, it can be because of further development and design enhancement and structural strengthening. Whatever the reason may be, it can lead to omission or inclusion of structural members.</p>\r\n\r\n<p style=\"text-align:justify\">The most common modification that we encountered during site construction is the addition of structural members to the existing one. In any means, the only way to connect an additional structure from an existing is though drill and fix method. For this, the proper and step by step procedure should be followed by the engineers in order to attain a safe and sound execution.</p>', '<p style=\"text-align:justify\">To give you an idea, let us take a look at the example figure below; an additional slab will be fixing to an existing core wall as shown. Refer to the succeeding paragraph for the methodology guide. This method statement for rebar post-fix is a general procedure and may not only apply to the sample detail below but can be also applied to any concrete structure, cast in place or post-tensioned and therefore can be adapted accordingly.</p>', '2022-03-12 02:51:49', '2022-03-12 02:51:49', NULL),
(24, 6, 'Underwater Drilling', '<p>Tasks like industrial drilling can be both dangerous and complicated at the best of times.</p>', '<p style=\"text-align:justify\">If a mistake were made while drilling a hole then the consequences could be<br />\r\ncatastrophic, which is why only highly trained operators are employed to carry out these tasks and stringent health and safety regulations are followed.</p>\r\n\r\n<p style=\"text-align:justify\">It&rsquo;s understandable that this process can become extraordinarily complicated when industrial drilling has to be carried out underwater. Occasionally underwater drilling is required, particularly in the ship maintenance and offshore oil drilling industries. And the stakes are incredibly high when working in these difficult conditions.</p>\r\n\r\n<p style=\"text-align:justify\">Diamond drilling is probably the most efficient type of drilling. By using diamond drill heads, it is possible to cut through almost any material &ndash; including metal, glass and concrete. At the same time, Diamond Drilling is faster and less labour intensive than other means of cutting. These two features make diamond drilling an appropriate choice for underwater drilling.</p>', '<p style=\"text-align:justify\">There are a few additional reasons why underwater drilling is best carried out using a diamond drill. Firstly, diamond drills must be lubricated with water to perform at their best, so underwater is actually the ideal operating condition for diamond drills. A diamond drill bit could last up to ten times longer when used underwater than on land.</p>\r\n\r\n<p style=\"text-align:justify\">Secondly, diamond drills are lightweight and portable, so it&rsquo;s easier to maneuver them underwater. Often multiple drills are mounted on special frames when used underwater in order to speed the work and help ensure that the precision of the cutting. The portability of diamond drills makes them ideal for this type of deployment.</p>', '2022-03-12 02:53:30', '2022-03-12 02:53:30', NULL),
(25, 3, 'Diamond Blade Sawing', '<p>Diamond blade sawing is divided into two main types, according to the machinery used in cutting</p>', '<p style=\"text-align:justify\">1- Diamond Floor Sawing</p>\r\n\r\n<p style=\"text-align:justify\">Floor saws are preferred when straight and clean cuts are required. They are commonly used to cut through roads, driveways, pavements, yards, slabs, and any other horizontally aligned structure. Floor saws are also opted to form expansion joints, remove the damaged layer of an upper surface of the structure and to perform partial demolition. At BuildIn, we have a wide range of floor saws to ensure we best fit your project. We have both diesel and electrically powered floor saws to ensure we can fulfil diverse requirements or purposes. Our floor saws can efficiently and easily cut through concrete, asphalt and any other construction material. So, whether it is a road, pavement, column or any other structure, you can depend on our high-quality floor sawing services.</p>\r\n\r\n<p style=\"text-align:justify\">Key Advantages of Diamond Floor Sawing Services:</p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Straight and precise cutting of concrete and asphalt structures up to 500mm in depth&nbsp;</li>\r\n	<li style=\"text-align:justify\">No vibration, no dust and no fume produced at the work site, making it an environment-friendly concrete cutting equipment&nbsp;</li>\r\n	<li style=\"text-align:justify\">Floor sawing is a one-man job&nbsp;</li>\r\n	<li style=\"text-align:justify\">Section-wise and partial cutting can be performed to minimize the risk of cracks or damage in the structure&nbsp;</li>\r\n	<li style=\"text-align:justify\">Need zero to minimal reinstatement work upon completion of the cutting, thus floor sawing is opted over other concrete cutting methods when cutting is needed in finished areas like supermarket and hospital&nbsp;</li>\r\n	<li style=\"text-align:justify\">Cutting through reinforced concrete turns hassle-free with the use of diamond floor saws&nbsp;</li>\r\n	<li style=\"text-align:justify\">Suitable for both indoor and outdoor cutting projects</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Why Choose BuildIn for Floor Sawing</p>\r\n\r\n<p style=\"text-align:justify\">With our extensive experience of +8 years in concrete cutting industry, today we are capable of meeting any challenging project specifications in the most effective and efficient manner. We have vast experience working with tens of contractors and project owners to assist them in their building or renovation projects. We assure you fast and reliable floor sawing services. We have most up-to-date floor sawing equipment to ensure best quality outcome, with clean and accurate results in every project we undertake. BuildIn also adheres to the health and safety guidelines in all our projects.</p>\r\n\r\n<p style=\"text-align:justify\">In addition to floor sawing, we also specialise in track sawing, wire sawing, diamond core sawing, floor preparation and hydraulic bursting.</p>', '<p style=\"text-align:justify\">2- Diamond Track (Wall) Sawing</p>\r\n\r\n<p style=\"text-align:justify\">Our track mounted saws feature a diamond blade that can create a clean and sharp cut to create openings in walls or floors for staircases, doorways, etc. The track is safely mounted to the wall and then the diamond blade saw moves to and from until<br />\r\nthe desired cut is achieved. Track saws are quite useful when you need to demolish a particular area of the structure, instead of the entire structure. They make an ideal choice when a steep incline or wall doesn&rsquo;t allow use of floor saws. While typically used for making openings for doors, windows and vents, track saws can be used to remove any size of the section in any concrete structure. These saws are operated remotely, thus reducing the safety risk for the operator. In addition to concrete, track saws are ideal for cutting through masonry, block work and other materials.</p>\r\n\r\n<p style=\"text-align:justify\">Key Advantages of Track Sawing:</p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Quick and precise cutting with depth up to 750mm&nbsp;</li>\r\n	<li style=\"text-align:justify\">Minimal noise and dust production during operation&nbsp;</li>\r\n	<li style=\"text-align:justify\">Least possible remedial works&nbsp;</li>\r\n	<li style=\"text-align:justify\">Suitable for both indoor and outdoor concrete cutting jobs&nbsp;</li>\r\n	<li style=\"text-align:justify\">Highly versatile&nbsp;</li>\r\n	<li style=\"text-align:justify\">Light in weight&nbsp;</li>\r\n	<li style=\"text-align:justify\">Perfect of cutting large sections of reinforced concrete</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">We Are an Experienced And Skilled Track Sawing Specialist</p>\r\n\r\n<p style=\"text-align:justify\">Here at BuildIn, we have a dedicated team of skilled and experienced track sawing professionals. We specialize in offering diamond track sawing services with many custom-designed track saws suitable for a host of applications. We have a perfect equipment to create openings, take out large sections and perform any other cutting job required for your project. When you hire our diamond sawing services, we assure you the best industry standards. We pay equal attention to every project we undertake, regardless of its size. Our track sawing experts are here to provide you with all necessary guidance and recommend the most efficient solution of the time with the best value for money.</p>', '2022-03-12 02:59:33', '2022-04-03 19:16:59', NULL),
(26, 3, 'Diamond Wire Sawing', '<p style=\"text-align:justify\">If you want to remove a large area of heavily reinforced concrete at a location where there are access restriction and speed as well as a depth of cutting are critical factors, diamond wire sawing is the ideal solution.</p>', '<p style=\"text-align:justify\">For instance, if you want to cut a large section of columns and beams in a bridge deck ensuring excellent speed and extensive depth of cutting, then diamond wire sawing &amp; cutting is the right alternative. When track saws fail to deliver the desired result, consider wire sawing as the perfect choice. The technology is also commonly used in the construction industry to cut flush to existing walls. It is also a perfect alternative to traditional stitch drilling.</p>\r\n\r\n<p style=\"text-align:justify\">How Cutting Is Performed Using Diamond Wire Sawing:</p>\r\n\r\n<p style=\"text-align:justify\">In Diamond wire sawing and cutting, a wire permeated with diamond beads is wrapped around the structure to be cut and run through a series of pulleys to<br />\r\nperform cuts. We can use wire of any desired length to obtain any required depth. Using this diamond encrusted wire and pulley system, we can cut any awkward shape much easier and faster way. While cutting operation is on, the pulley system auto adjusts the tension to achieve desired depth of the cut until the job is finished. The wire saw can be operated from a remote distance so increases the safety onsite. The technique is also used to cut concrete, or any other construction material located beneath the water. In short, when no other cutting or drilling technology fits the project, wire sawing makes the right choice.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>', '<p style=\"text-align:justify\">Key Benefits of Diamond Wire Sawing:</p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Suitable for endless cutting depth&nbsp;</li>\r\n	<li style=\"text-align:justify\">Smooth, clean finish cuts in an easier and faster way&nbsp;</li>\r\n	<li style=\"text-align:justify\">Non-percussive cutting method&nbsp;</li>\r\n	<li style=\"text-align:justify\">Doesn&rsquo;t produce noise and fume onsite&nbsp;</li>\r\n	<li style=\"text-align:justify\">Safe concrete cutting and drilling assured&nbsp;</li>\r\n	<li style=\"text-align:justify\">Whether you need to cut vertically, horizontally or at any angle, diamond wire saws are highly flexible&nbsp;</li>\r\n	<li style=\"text-align:justify\">Using diamond wire sawing technology, there is no damage caused to the surrounding structure onsite.&nbsp;</li>\r\n	<li style=\"text-align:justify\">Remove large areas of reinforced concrete within access restricted locations&nbsp;</li>\r\n	<li style=\"text-align:justify\">Alternative to traditional stitch drilling&nbsp;</li>\r\n	<li style=\"text-align:justify\">Can cut underwater&nbsp;</li>\r\n	<li style=\"text-align:justify\">Cut flush to existing walls</li>\r\n</ul>\r\n\r\n<p style=\"text-align:justify\">Why Choose BuildIn for Your Wire Sawing Needs</p>\r\n\r\n<p style=\"text-align:justify\">1. We have a team of professional engineers who can assess the cutting job, providing the most engineering effective solutions</p>\r\n\r\n<p style=\"text-align:justify\">2. We are a most prominent diamond wire sawing &amp; cutting contractor</p>\r\n\r\n<p style=\"text-align:justify\">3. We have trained and skilled technicians who can cut thickest sections of concrete in any complex condition easily and quickly</p>\r\n\r\n<p style=\"text-align:justify\">4. We have an experienced safety team who can assess all risks of any job to perform the required cut using the safest solutions</p>', '2022-03-12 03:02:42', '2022-04-03 19:45:09', NULL),
(27, 3, 'Diamond Core Sawing', '<p style=\"text-align:justify\">BuildIn is committed to offering quality diamond drilling services in Egypt. We provide diamond drilling and concrete cutting services of the property, size, and MoHS (scale of Material hardness).</p>', '<p style=\"text-align:justify\">The diamond core drilling services we provide in Egypt are applied on hard materials such as brick or concrete to form the roundest and smoothest holes. Whether small holes for wiring or plumbing or huge holes for the larger spaces like doors and windows, our diamond drilling process ensures that the vibration-free technique produces as little smoke and dust as possible. Diamond drilling is a very versatile methodology that can be implemented in various industries and areas of commerce. Hire our diamond drilling experts to ensure the project is accomplished much simpler and efficiently in terms of time, cost, and technique.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>', '<p style=\"text-align:justify\">BEST-KNOWN FEATURES OF DIAMOND DRILLING:</p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Diamond core drilling minimizes incidents of spalling, eliminates fractures, and doesn&rsquo;t transfer vibrations to the surrounding structure.</li>\r\n	<li style=\"text-align:justify\">The most important and general application of core diamond concrete drilling is to install any electrical, plumbing, and water pipelines and air conditioning pipe fitting through the floors and walls.</li>\r\n	<li style=\"text-align:justify\">The drilling process can be done in a horizontal as well as vertical manner with the help of the drill bit, it is quite suitable for operating in remote and restricted areas of the construction site.</li>\r\n</ul>', '2022-03-12 03:04:15', '2022-04-03 19:51:25', NULL),
(28, 3, 'Mechanical Demolition', '<p style=\"text-align:justify\">BuildIn offers thoroughly planned and controlled mechanical demolition services that cover all types of projects, small or large, simple or complex.</p>', '<p style=\"text-align:justify\">We plan each project meticulously and with careful consideration, whilst making safety our top priority. Owning our own machinery and equipment also means we can pass any cost savings directly to you.</p>\r\n\r\n<p style=\"text-align:justify\">Each project comprises of an initial building or site survey which in turn contains detailed information about the structures to be demolished, location of the works, the surrounding environment and any risks involved. This then forms the method used to undertake the works.</p>', '<p style=\"text-align:justify\">Safety is our top priority, and the Safety-First initiative is taught throughout the company. Our operatives are well trained, ensuring they meet the highest standards for safe demolition works.</p>\r\n\r\n<p style=\"text-align:justify\">BuildIn guarantees minimal disruption to the surrounding area through numerous methods including dust suppression with a managed amount of water, noise and traffic management. We adhere to appropriate delivery times, undertake leaflet drops in the local area prior to works and segregate zones for public and employee safety.</p>', '2022-03-12 03:05:43', '2022-03-12 03:05:43', NULL),
(29, 8, 'Pull Out Testing', '<p style=\"text-align:justify\">At the site it is often seen the tensile test on the anchors or post installed rebars fixed with chemicals.</p>', '<p style=\"text-align:justify\">BuildIn provides qualified personnel to perform the tests, which are carried out using equipment that is calibrated regularly, with diameter range from 6mm to 50 mm diameter of anchor or rebar and tension forces from 100 kg to 60 tons.</p>\r\n\r\n<p style=\"text-align:justify\">Reasons To Perform Pull Out Test:</p>\r\n\r\n<p style=\"text-align:justify\">1. Determine the failure load of anchor or verification of supporting resistance of anchorage - by loading tests (stress test)</p>\r\n\r\n<p style=\"text-align:justify\">2. Tests carried out with perfectly calibrated test equipment and by competent and trained personnel.</p>\r\n\r\n<p style=\"text-align:justify\">3. Check the structural strength of the anchoring system or base material</p>\r\n\r\n<p style=\"text-align:justify\">4. The report is specific to the test carried out</p>', '<p style=\"text-align:justify\">Test Evaluation report:</p>\r\n\r\n<p style=\"text-align:justify\">BuildIn provide test evaluation report after the pull-out test is done. This test evaluation is done by our extremely trained and competent engineers. Test evaluation report helps in the determination of resistance of the calculated results and it helps in verification of the quality: assessment and confirmation</p>', '2022-03-12 03:07:43', '2022-03-12 03:07:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_images`
--

CREATE TABLE `service_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1' COMMENT '0:main , 1:sub',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_images`
--

INSERT INTO `service_images` (`id`, `service_id`, `image`, `role`, `created_at`, `updated_at`) VALUES
(20, 11, 'master844381647032110.jpg', 0, '2022-03-12 01:55:10', '2022-03-12 01:55:10'),
(27, 18, 'master280901647034754.jpg', 0, '2022-03-12 02:39:14', '2022-03-12 02:39:14'),
(30, 21, 'master445011647035323.jpg', 0, '2022-03-12 02:48:43', '2022-03-12 02:48:43'),
(40, 7, 'master802631647185896.jpg', 0, '2022-03-13 19:38:16', '2022-03-13 20:03:09'),
(49, 7, 'master884881647186987.jpg', 1, '2022-03-13 19:56:27', '2022-03-13 20:03:09'),
(53, 1, 'master631291647508509.jpg', 1, '2022-03-17 13:15:09', '2022-03-17 15:09:06'),
(54, 1, 'master961021647508509.jpg', 1, '2022-03-17 13:15:09', '2022-03-17 15:09:06'),
(55, 1, 'master691351647509281.jpg', 0, '2022-03-17 13:28:01', '2022-03-17 15:09:06'),
(56, 2, 'master141471647515163.jpg', 1, '2022-03-17 15:06:03', '2022-03-17 15:08:29'),
(57, 2, 'master193701647515163.jpg', 1, '2022-03-17 15:06:03', '2022-03-17 15:08:29'),
(58, 2, 'master895711647515163.jpg', 0, '2022-03-17 15:06:03', '2022-03-17 15:08:29'),
(59, 7, 'master422601647515479.jpg', 0, '2022-03-17 15:11:19', '2022-03-17 15:11:19'),
(60, 7, 'master547711647515479.jpg', 1, '2022-03-17 15:11:19', '2022-03-17 15:11:19'),
(61, 7, 'master134481647515479.jpg', 1, '2022-03-17 15:11:19', '2022-03-17 15:11:19'),
(63, 7, 'master331011647515480.jpg', 1, '2022-03-17 15:11:20', '2022-03-17 15:11:20'),
(68, 6, 'master420031647516120.jpg', 1, '2022-03-17 15:22:00', '2022-03-29 17:17:14'),
(69, 8, 'master250041647518158.jpg', 1, '2022-03-17 15:55:58', '2022-03-17 15:56:41'),
(70, 8, 'master937211647518158.jpg', 0, '2022-03-17 15:55:58', '2022-03-17 15:56:41'),
(73, 8, 'master590221647518295.jpg', 0, '2022-03-17 15:58:15', '2022-03-17 15:58:15'),
(74, 10, 'master942131647525042.jpg', 1, '2022-03-17 17:50:42', '2022-03-17 17:57:15'),
(75, 10, 'master745531647525042.jpg', 0, '2022-03-17 17:50:42', '2022-03-17 17:57:15'),
(76, 10, 'master840951647525042.jpg', 1, '2022-03-17 17:50:42', '2022-03-17 17:57:15'),
(80, 12, 'master192841647525830.jpg', 1, '2022-03-17 18:03:50', '2022-03-29 20:02:17'),
(81, 12, 'master977011647525830.jpg', 0, '2022-03-17 18:03:50', '2022-03-29 20:02:17'),
(83, 13, 'master856401647536072.jpg', 1, '2022-03-17 20:54:32', '2022-03-29 20:14:02'),
(84, 13, 'master832841647536072.jpg', 0, '2022-03-17 20:54:32', '2022-03-29 20:34:33'),
(93, 14, 'master279961647955724.jpg', 1, '2022-03-22 17:28:44', '2022-03-22 17:28:44'),
(94, 14, 'master315701647955724.jpg', 1, '2022-03-22 17:28:44', '2022-03-29 20:51:45'),
(95, 14, 'master314801647955724.jpg', 1, '2022-03-22 17:28:44', '2022-03-22 17:28:44'),
(96, 14, 'master596881647955724.jpg', 1, '2022-03-22 17:28:44', '2022-03-22 17:48:09'),
(97, 14, 'master832271647955724.jpg', 1, '2022-03-22 17:28:44', '2022-03-22 17:28:44'),
(100, 14, 'master959391647956569.jpg', 0, '2022-03-22 17:42:49', '2022-03-29 20:51:45'),
(101, 6, 'master152611648548068.jpg', 0, '2022-03-29 17:01:08', '2022-03-29 17:23:52'),
(103, 6, 'master254481648548068.jpg', 1, '2022-03-29 17:01:08', '2022-03-29 17:23:52'),
(104, 9, 'master665971648551707.jpg', 1, '2022-03-29 18:01:47', '2022-03-29 19:54:01'),
(106, 9, 'master358201648551707.jpg', 1, '2022-03-29 18:01:47', '2022-03-29 18:01:47'),
(107, 9, 'master828681648551707.jpg', 0, '2022-03-29 18:01:47', '2022-03-29 19:54:01'),
(108, 9, 'master943691648558303.jpg', 1, '2022-03-29 19:51:43', '2022-03-29 19:54:01'),
(109, 9, 'master795141648558303.jpg', 1, '2022-03-29 19:51:43', '2022-03-29 19:51:43'),
(110, 9, 'master809911648558303.jpg', 1, '2022-03-29 19:51:43', '2022-03-29 19:51:43'),
(111, 9, 'master642801648558303.jpg', 1, '2022-03-29 19:51:43', '2022-03-29 19:51:43'),
(115, 9, 'master485301648558303.jpg', 1, '2022-03-29 19:51:43', '2022-03-29 19:51:43'),
(116, 9, 'master282701648558303.jpg', 1, '2022-03-29 19:51:43', '2022-03-29 19:51:43'),
(117, 12, 'master123881648558738.jpg', 1, '2022-03-29 19:58:58', '2022-03-29 20:01:49'),
(125, 13, 'master966881648560146.jpg', 1, '2022-03-29 20:22:26', '2022-03-29 20:34:33'),
(128, 13, 'master918211648561314.jpg', 0, '2022-03-29 20:41:54', '2022-03-29 20:41:54'),
(130, 13, 'master461371648561314.jpg', 1, '2022-03-29 20:41:54', '2022-03-29 20:41:54'),
(131, 13, 'master780241648561508.jpg', 0, '2022-03-29 20:45:08', '2022-03-29 20:45:08'),
(138, 15, 'master385591648562273.jpg', 0, '2022-03-29 20:57:53', '2022-03-29 21:00:06'),
(139, 15, 'master166201648562273.jpg', 1, '2022-03-29 20:57:53', '2022-03-29 21:00:06'),
(140, 15, 'master371281648562273.jpg', 1, '2022-03-29 20:57:53', '2022-03-29 20:57:53'),
(142, 16, 'master747811648562685.jpg', 0, '2022-03-29 21:04:45', '2022-03-29 21:05:34'),
(144, 17, 'master366271648562891.jpg', 1, '2022-03-29 21:08:11', '2022-03-29 21:11:01'),
(145, 17, 'master712851648562891.jpg', 1, '2022-03-29 21:08:11', '2022-03-29 21:08:11'),
(146, 17, 'master298881648562891.jpg', 1, '2022-03-29 21:08:11', '2022-03-29 21:08:11'),
(147, 17, 'master836051648562891.jpg', 1, '2022-03-29 21:08:11', '2022-03-29 21:08:11'),
(148, 17, 'master675931648562891.jpg', 1, '2022-03-29 21:08:11', '2022-03-29 21:08:11'),
(149, 17, 'master691711648562891.jpg', 0, '2022-03-29 21:08:11', '2022-03-29 21:11:01'),
(152, 19, 'master347151648980610.jpg', 0, '2022-04-03 17:10:10', '2022-04-03 17:17:33'),
(157, 20, 'master798601648981247.jpg', 0, '2022-04-03 17:20:47', '2022-04-03 17:25:46'),
(163, 22, 'master439001648982192.jpg', 0, '2022-04-03 17:36:32', '2022-04-03 17:39:19'),
(164, 22, 'master712171648982192.jpg', 1, '2022-04-03 17:36:32', '2022-04-03 17:39:19'),
(165, 22, 'master349831648982192.jpg', 1, '2022-04-03 17:36:32', '2022-04-03 17:36:32'),
(166, 23, 'master985161648983030.jpg', 1, '2022-04-03 17:50:30', '2022-04-03 17:54:48'),
(167, 23, 'master888961648983030.jpg', 0, '2022-04-03 17:50:30', '2022-04-03 17:54:48'),
(168, 23, 'master288941648983030.jpg', 1, '2022-04-03 17:50:30', '2022-04-03 17:50:30'),
(169, 23, 'master765301648983030.jpg', 1, '2022-04-03 17:50:30', '2022-04-03 17:50:30'),
(170, 23, 'master456331648983030.jpg', 1, '2022-04-03 17:50:30', '2022-04-03 17:50:30'),
(171, 23, 'master890111648983030.jpg', 1, '2022-04-03 17:50:30', '2022-04-03 17:50:30'),
(172, 24, 'master228021648984484.jpg', 0, '2022-04-03 18:14:44', '2022-04-03 19:38:48'),
(173, 24, 'master460081648984484.jpg', 1, '2022-04-03 18:14:44', '2022-04-03 19:38:48'),
(174, 25, 'master239801648988219.jpg', 1, '2022-04-03 19:16:59', '2022-04-03 19:30:03'),
(175, 25, 'master359341648988219.jpg', 0, '2022-04-03 19:16:59', '2022-04-03 19:30:03'),
(176, 25, 'master867931648988219.jpg', 1, '2022-04-03 19:16:59', '2022-04-03 19:16:59'),
(177, 24, 'master983981648989820.jpg', 0, '2022-04-03 19:43:40', '2022-04-03 19:43:40'),
(178, 26, 'master943061648989909.jpg', 1, '2022-04-03 19:45:09', '2022-04-03 19:45:31'),
(179, 26, 'master939271648989909.jpg', 1, '2022-04-03 19:45:09', '2022-04-03 19:45:09'),
(180, 26, 'master849301648989909.jpg', 0, '2022-04-03 19:45:09', '2022-04-03 19:45:31'),
(182, 27, 'master807151648990285.jpg', 1, '2022-04-03 19:51:25', '2022-04-03 19:51:25'),
(183, 27, 'master876771648990285.jpg', 0, '2022-04-03 19:51:25', '2022-04-03 19:55:31'),
(184, 27, 'master803151648990379.jpg', 1, '2022-04-03 19:52:59', '2022-04-03 19:55:31'),
(187, 28, 'master842021648990716.jpg', 1, '2022-04-03 19:58:36', '2022-04-03 19:58:58'),
(188, 28, 'master119141648990716.jpg', 0, '2022-04-03 19:58:36', '2022-04-03 19:58:58'),
(190, 28, 'master954011648990716.jpg', 1, '2022-04-03 19:58:36', '2022-04-03 19:58:36'),
(191, 29, 'master911751648991336.jpg', 1, '2022-04-03 20:08:56', '2022-04-03 20:09:36'),
(192, 29, 'master191981648991336.jpg', 0, '2022-04-03 20:08:56', '2022-04-03 20:09:36'),
(193, 29, 'master736711648991336.jpg', 1, '2022-04-03 20:08:56', '2022-04-03 20:08:56'),
(194, 8, 'master925491649701674.jpg', 0, '2022-04-12 01:27:54', '2022-04-12 01:27:54'),
(195, 16, 'master515111651288500.jpeg', 0, '2022-04-30 10:15:00', '2022-04-30 10:15:00'),
(196, 16, 'master644761651288500.jpeg', 1, '2022-04-30 10:15:00', '2022-04-30 10:15:00'),
(197, 16, 'master672701651288500.jpeg', 1, '2022-04-30 10:15:00', '2022-04-30 10:15:00'),
(198, 16, 'master872851651288601.jpeg', 0, '2022-04-30 10:16:41', '2022-04-30 10:16:41');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sitename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addresslink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slogan` text COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vision` text COLLATE utf8mb4_unicode_ci,
  `mission` text COLLATE utf8mb4_unicode_ci,
  `mapembed` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6903.296342985706!2d31.323036!3d30.104261!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1fa47ac3e883a584!2sBuildin%20contracting!5e0!3m2!1sen!2seg!4v1644589729721!5m2!1sen!2seg" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>',
  `career_title1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `career_title2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `career_title3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `career_description1` text COLLATE utf8mb4_unicode_ci,
  `career_description2` text COLLATE utf8mb4_unicode_ci,
  `career_description3` text COLLATE utf8mb4_unicode_ci,
  `career_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `since` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `sitename`, `phone`, `whatsapp`, `email`, `address`, `addresslink`, `logo`, `slogan`, `facebook`, `instagram`, `linkedin`, `created_at`, `updated_at`, `title`, `description`, `image`, `vision`, `mission`, `mapembed`, `career_title1`, `career_title2`, `career_title3`, `career_description1`, `career_description2`, `career_description3`, `career_image`, `since`) VALUES
(1, 'BUILDIN | A PASSION TO BUILD', '+201288886153', '+201288886153', 'contact@buildineg.com', '11B Abaza Square, Heliopolis, Cairo', 'https://www.google.com/maps?q=483F+P63+Buildin+contracting,+El-Bostan,+Heliopolis,+Cairo+Governorate&ftid=0x14581591d3e45bad:0x1fa47ac3e883a584&hl=en-EG&gl=eg&entry=gps&lucs=swa&shorturl=1', 'Image287711643747477.png', 'Egyptian Based General and Specialized Construction Contractor', 'https://www.facebook.com/buildineg', '#', 'https://www.linkedin.com/company/buildin-contracting/', NULL, '2022-04-09 19:37:55', 'BUILDIN CONTRACTING IS AN EGYPTIAN-BASED GENERAL AND SPECIALIZED CONSTRUCTION CONTRACTOR', '<p>was founded in 1999 when it started working under the name of (EL MASREYA FOR CONTRACTING &amp; DECOR ) delivers turnkey projects for diverse clients.</p>\r\n\r\n<p>At the beginning of 2016 was the restructuring of the company under the name BUILDIN CONTRACTING. Since 2016, BUILDIN is an integrated, multi-Discipline Civil Engineering and Architectural services, provider.</p>', 'Image150611644072386.jpg', 'To Be The Undisputed Leader In The Construction And Architecture Sector In Egypt And Africa.', '<p>We strive to be the best choice for our clients and partners by delivering excellence and creating value in every aspect of our operations choosing the simply luxurious life and ensuring long-term sustainable growth for all stakeholders While preserving the Environment.</p>', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6903.296342985706!2d31.323036!3d30.104261!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1fa47ac3e883a584!2sBuildin%20contracting!5e0!3m2!1sen!2seg!4v1644589729721!5m2!1sen!2seg\" width=\"100%\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\"></iframe>', 'Be Part of Something Ambitious', 'Be Part of Something Innovative', 'Be Part of Our Future', '<p>We&rsquo;re never content with now. We&rsquo;re constantly looking for new challenges and the right kind of people to help us meet those challenges. By combining the ambitions of our people with the ambitions of our business, we have become a group that&rsquo;s not only growing but one that&rsquo;s developing new markets, new initiatives and new opportunities.</p>', '<p>We&rsquo;re obsessed with getting things right and see problems as innovations we&rsquo;ve yet to discover.</p>\r\n\r\n<p>When we say we &ldquo;exceed expectations&rdquo; we mean we don&rsquo;t settle for &ldquo;that will do&rdquo;. We create value for our customers by striving to find solutions to their problems, no matter how extreme.</p>\r\n\r\n<p>We employ exceptional, talented, proactive people with a &lsquo;can-do&rsquo; attitude and a desire to be innovative. People who take time to understand and address the complex needs of our customers &ndash; and regularly surprise and delight them by giving more than they expected.</p>', '<p>We know that by employing talented people, nurturing their ambitions and giving them the opportunity to develop, we will all grow and prosper.</p>\r\n\r\n<p>We know who we are and where we want to be. If you want to be part of our journey, come with us and make it happen.</p>', 'Image157871649364779.jpg', '23'),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6903.296342985706!2d31.323036!3d30.104261!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1fa47ac3e883a584!2sBuildin%20contracting!5e0!3m2!1sen!2seg!4v1644589729721!5m2!1sen!2seg\" width=\"100%\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\"></iframe>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '23');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `logo`, `description`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Image849601649035065.jpg', '<h3>BUILDIN CONTRACTING IS AN EGYPTION BASED GENERAL AND SPECIALIZED CONSTRUCTION CONTRACTOR .</h3>', '#', '2022-02-01 20:50:39', '2022-04-04 08:17:46'),
(2, 'Image970721649036481.jpg', '<h3>TO BE THE UNDISPUTED LEADER IN THE CONSTRUCTION AND ARCHITECTURE .</h3>', '#', '2022-02-01 20:51:11', '2022-04-04 08:41:21'),
(3, 'Image579401649037458.jpg', '<h3>BUILDIN IS AN INTEGRATED, MULTI DISCIPLINE CIVIL ENGINEERING AND ARCHITECTURAL SERVICES PROVIDER&nbsp;</h3>', '#', '2022-02-01 20:51:50', '2022-04-04 08:57:38'),
(5, 'Image945211649032374.jpg', '<p>BuildIn is one stop shop anchoring contractor market leader .</p>', 'https://buildineg.com/singleService/23', '2022-04-04 07:32:54', '2022-04-04 07:32:54'),
(6, 'Image513961649033570.jpg', '<p>Our experienced carpenters and concrete finishers excel in forming and pouring concrete for different type of Buildings .</p>', 'https://buildineg.com/singleService/2', '2022-04-04 07:52:50', '2022-04-04 07:52:50'),
(7, 'Image999501649365463.jpg', '<p>We are Certified applicators for the Most leading fire-stop systems suppliers&nbsp;</p>', 'https://buildineg.com/singleService/10', '2022-04-08 04:04:23', '2022-04-08 04:04:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'admin@buildineg.com', NULL, '$2y$10$5TG1Yy.Tckl5viiYdwa5jOOygboRfE95PLLLmSLOQxogcM7JWbrOS', 'mM5ScBRrMs5cRAfDZsZd4ISeRwQONObboE0UUMzjdjOWQ6QLGiB1GSYBnaMF', '2022-01-31 22:00:00', '2022-01-31 22:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `whychooses`
--

CREATE TABLE `whychooses` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `whychooses`
--

INSERT INTO `whychooses` (`id`, `logo`, `title`, `created_at`, `updated_at`, `description`) VALUES
(1, 'Image123281643752492.png', 'SAFETY', '2022-02-01 19:54:52', '2022-02-24 18:59:53', 'As an industry provider of high-value technical solutions, we are firmly committed to maintaining a safe and healthy environment in each of our projects.'),
(2, 'Image378771643752534.png', 'HONEST & DEPENDABLE', '2022-02-01 19:55:34', '2022-02-24 19:01:16', 'Integrity and fairness are our basics. business ethics, Honesty is not the best policy,but the only policy.'),
(3, 'Image362771643752606.png', 'EXPERIENCE', '2022-02-01 19:56:46', '2022-02-01 19:56:46', ''),
(4, 'Image248821643752633.png', 'PROJECTS MANAGEMENT', '2022-02-01 19:57:13', '2022-02-01 19:57:13', ''),
(5, 'Image560531643752657.png', 'COST', '2022-02-01 19:57:37', '2022-02-01 19:57:37', ''),
(6, 'Image560801643752672.png', 'QUALITY', '2022-02-01 19:57:52', '2022-02-01 19:57:52', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `career_requests`
--
ALTER TABLE `career_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `career_requests_career_id_index` (`career_id`);

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contacts_service_id_index` (`service_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ourvalues`
--
ALTER TABLE `ourvalues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projects_service_id_index` (`service_id`);

--
-- Indexes for table `project_images`
--
ALTER TABLE `project_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_images_project_id_index` (`project_id`);

--
-- Indexes for table `responsibilities`
--
ALTER TABLE `responsibilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `responsibilitysliders`
--
ALTER TABLE `responsibilitysliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicecategories`
--
ALTER TABLE `servicecategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `services_service_category_id_index` (`service_category_id`);

--
-- Indexes for table `service_images`
--
ALTER TABLE `service_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_images_service_id_index` (`service_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `whychooses`
--
ALTER TABLE `whychooses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `career_requests`
--
ALTER TABLE `career_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `ourvalues`
--
ALTER TABLE `ourvalues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `project_images`
--
ALTER TABLE `project_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=225;

--
-- AUTO_INCREMENT for table `responsibilities`
--
ALTER TABLE `responsibilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `responsibilitysliders`
--
ALTER TABLE `responsibilitysliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `servicecategories`
--
ALTER TABLE `servicecategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `service_images`
--
ALTER TABLE `service_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `whychooses`
--
ALTER TABLE `whychooses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `career_requests`
--
ALTER TABLE `career_requests`
  ADD CONSTRAINT `career_requests_career_id_foreign` FOREIGN KEY (`career_id`) REFERENCES `careers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `project_images`
--
ALTER TABLE `project_images`
  ADD CONSTRAINT `project_images_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_service_category_id_foreign` FOREIGN KEY (`service_category_id`) REFERENCES `servicecategories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_images`
--
ALTER TABLE `service_images`
  ADD CONSTRAINT `service_images_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
